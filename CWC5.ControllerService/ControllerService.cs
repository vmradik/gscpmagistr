﻿using CWC5.Controller;
using Grpc.Core;
using GscpGrpc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using grpc = global::Grpc.Core;

namespace CWC5.ControllerService
{
	class ControllerService : GscpGrpc.ControllerService.ControllerServiceBase
	{
		string address_path = "./settings.json";

		Server server;

		bool running;
		bool local = false;

		public ControllerService()
		{

		}

		public void run()
		{
			Environment.SetEnvironmentVariable("NO_PROXY", "localhost");
			//port = 52001;
			//_process.Start_with_host(ip, port);
			JObject o = JObject.Parse(File.ReadAllText(address_path));
			if (!o.ContainsKey("host") || !o.ContainsKey("port"))
			{
				throw new Exception("invalid connection data");
			}
			string host = o["host"].ToString();
			int port = o["port"].Value<int>();
			var services = GscpGrpc.ControllerService.BindService(this);
			var ports = new ServerPort(host, port, ServerCredentials.Insecure);
			server = new Grpc.Core.Server()
			{
				Services = { services },
				Ports = { ports }
			};
			server.Start();

			Console.WriteLine($"Listening on {host}:{port}...");
			running = true;

			while (running)
			{
				Thread.Sleep(1000);
			}
			Console.WriteLine($"Exiting.");
		}

		class CoreConnection
		{
			public int port = 0;
			public string ip;
			public Channel channel;
			public LogicalCoreService.LogicalCoreServiceClient client = null;
			public int error_count = 0;
			public override bool Equals(object obj)
			{
				if (obj == null) return false;
				CoreConnection c = obj as CoreConnection;
				if (c == null) {
					return false;
				}
				else {
					return ip.Equals(c.ip) && port == c.port;
				}
			}
		}

		public class LogicalCoreRotation
		{
			public bool local = false;
			LinkedList<CoreConnection> cores = new LinkedList<CoreConnection>();
			LinkedListNode<CoreConnection> current_core = null;
			public LogicalCoreRotation(string config_path)
			{
				JObject o = JObject.Parse(File.ReadAllText(config_path));
				if (!o.ContainsKey("logical_cores"))
				{
					local = true;
				}
				else
				{
					JArray logical_cores = o["logical_cores"].Value<JArray>();
					if (!logical_cores.HasValues)
					{
						local = true;
					}
					else
					{
						foreach (JToken t in logical_cores)
						{
							JObject core = t.Value<JObject>();
							if (!core.ContainsKey("host") || !core.ContainsKey("port"))
							{
								throw new Exception("invalid connection data");
							}
							local = false;
							string c_host = core["host"].ToString();
							int c_port = core["port"].Value<int>();
							//port = 52001;
							var channel = new Channel(c_host, c_port, ChannelCredentials.Insecure);
							var client = new GscpGrpc.LogicalCoreService.LogicalCoreServiceClient(channel);
							CoreConnection c = new CoreConnection()
							{
								channel = channel,
								client = client,
								ip = c_host,
								port = c_port
							};
							cores.AddLast(c);
						}
					}
				}
			}

			CoreConnection current
			{
				get
				{
					if (current_core == null)
					{
						current_core = cores.First;
					}
					var ret = current_core.Value;
					return ret;
				}
			}

			public void rotate()
			{
				if (current_core == null)
				{
					current_core = cores.First;
				}
				if (current_core.Next == null)
				{
					current_core = cores.First;
				}
				else
				{
					current_core = current_core.Next;
				}
			}

			public ArenaResponse GetStartingArena(ArenaRequest request)
			{
				var c = current;
				ArenaResponse resp = null; ;
				bool remove = false;
				try
				{
					resp = c.client.GetStartingArena(request);
				} catch (Exception e)
				{
					++c.error_count;
					try
					{
						resp = c.client.GetStartingArena(request);
					}
					catch (Exception e2)
					{
						remove = true;
					}
				} finally
				{
					rotate();
					if (remove)
					{
						if (cores.Count < 2)
						{
							throw new Exception("No working LogicalCores left.");
						}
						cores.Remove(c);
					}
					if (resp == null)
					{
						resp = GetStartingArena(request);
					}
				}
				return resp;
			}

			public NewTickArena GetNextTick(TickRequest request)
			{
				var c = current;
				NewTickArena resp = null; ;
				bool remove = false;
				try
				{
					resp = c.client.GetNextTick(request);
				}
				catch (Exception e)
				{
					++c.error_count;
					try
					{
						resp = c.client.GetNextTick(request);
					}
					catch (Exception e2)
					{
						remove = true;
					}
				}
				finally
				{
					rotate();
					if (remove)
					{
						if (cores.Count < 2)
						{
							throw new Exception("No working LogicalCores left.");
						}
						cores.Remove(c);
					}
					if (resp == null)
					{
						resp = GetNextTick(request);
					}
				}
				return resp;
			}
		}

		public override global::System.Threading.Tasks.Task<global::GscpGrpc.ModelingResponse> DoModeling(global::GscpGrpc.ModelingRequest request, grpc::ServerCallContext context)
		{
			var game_options_obj = JsonConvert.DeserializeObject(request.DefaultOptions, new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Objects,
				SerializationBinder = new ControllerTypesBinder()
			});
			if (game_options_obj is CWC5.LogicalCore.Model.ModelingOptions game_options)
			{
				var bots_obj = JsonConvert.DeserializeObject<List<CWC5.Controller.BotInfo>>(request.Bots, new JsonSerializerSettings
				{
					TypeNameHandling = TypeNameHandling.Objects,
					SerializationBinder = new ControllerTypesBinder()
				});
				if (bots_obj is List<CWC5.Controller.BotInfo> bots)
				{
					Console.WriteLine($"Modeling new round");
					ModelingResult res;
					LogicalCoreRotation r = new LogicalCoreRotation(address_path);
					local = r.local;
					if (local)
					{
						res = call_logical_core_service(game_options, bots);
					}
					else
					{
						res = call_logical_core_service(r, game_options, bots);
					}
					var json = JsonConvert.SerializeObject(res, Formatting.Indented, new JsonSerializerSettings
					{
						TypeNameHandling = TypeNameHandling.Objects,
						SerializationBinder = new ControllerTypesBinder()
					});
					var resp = new ModelingResponse()
					{
						ModelingResult = json
					};
					return Task.FromResult(resp);
				}
				else
				{
					throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.InvalidArgument, "received invalid object. /n " + request.Bots));
				}
			}
			else
			{
				throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.InvalidArgument, "received invalid object. /n " + request.DefaultOptions));
			}
		}

		public ModelingResult call_logical_core_service(CWC5.LogicalCore.Model.ModelingOptions game_options, List<CWC5.Controller.BotInfo> bots)
		{
			var result = CWC5.Controller.Controller.DoModeling(CWC5.Controller.Controller.DefaultOptions, bots);
			return result;
		}


		public ModelingResult call_logical_core_service(LogicalCoreRotation r,
			CWC5.LogicalCore.Model.ModelingOptions game_options, List<CWC5.Controller.BotInfo> bots)
		{

			var result = CWC5.Controller.Controller.DoModeling(CWC5.Controller.Controller.DefaultOptions, bots,
				(CWC5.LogicalCore.Model.GameOptions opts) =>
				{
					var json = JsonConvert.SerializeObject(opts, Formatting.Indented, new JsonSerializerSettings
					{
						TypeNameHandling = TypeNameHandling.Objects,
						SerializationBinder = new CWC5.LogicalCore.Model.LogicalCoreTypesBinder()
					});
					var req = new ArenaRequest()
					{
						GameOptions = json
					};
					var res = r.GetStartingArena(req);

					var arena = JsonConvert.DeserializeObject(res.Arena, new JsonSerializerSettings
					{
						TypeNameHandling = TypeNameHandling.Objects,
						SerializationBinder = new CWC5.LogicalCore.Model.LogicalCoreTypesBinder()
					});
					if (arena is CWC5.LogicalCore.Model.Arena new_arena)
					{
						Console.WriteLine($"Arena generated.");
						return new_arena;
					}
					else
					{
						throw new Exception("received invalid object. /n " + res.Arena);
					}
				},
				(CWC5.LogicalCore.Model.Arena arena, List<CWC5.DTOModel.TurnResult?> turn_results) =>
				{
					var arena_json = JsonConvert.SerializeObject(arena, Formatting.Indented, new JsonSerializerSettings
					{
						TypeNameHandling = TypeNameHandling.Objects,
						SerializationBinder = new CWC5.LogicalCore.Model.LogicalCoreTypesBinder()
					});
					var bot_responses_json = JsonConvert.SerializeObject(turn_results, Formatting.Indented, new JsonSerializerSettings
					{
						TypeNameHandling = TypeNameHandling.Objects,
						SerializationBinder = new CWC5.LogicalCore.Model.LogicalCoreTypesBinder()
					});
					var req = new GscpGrpc.TickRequest()
					{
						Arena = arena_json,
						BotAnswers = bot_responses_json
					};

					var res = r.GetNextTick(req);

					var new_tick_arena = JsonConvert.DeserializeObject(res.Arena, new JsonSerializerSettings
					{
						TypeNameHandling = TypeNameHandling.Objects,
						SerializationBinder = new CWC5.LogicalCore.Model.LogicalCoreTypesBinder()
					});
					if (new_tick_arena is CWC5.LogicalCore.Model.Arena new_arena)
					{
						Console.WriteLine($"Processed tick {new_arena.Tick}.");
						return new_arena;
					}
					else
					{
						throw new Exception("received invalid object. /n " + res.Arena);
					}
				});
			return result;
		}
	}
}
