﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CWC5.DTOModel
{
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 1)]
    public struct GameOptions
    {
        #region New opts
        #region Objects
        #region Arena
        public Point Arena_Size;
        public Int32 Arena_FightDuration;
        #endregion

        #region EnergySpill
        public Double EnergySpill_Power;
        public Double EnergySpill_RadiusMin;
        public Double EnergySpill_RadiusMax;
        public Int32 EnergySpill_LifetimeMin;
        public Int32 EnergySpill_LifetimeMax;
        public Int32 EnergySpill_PauseMin;
        public Int32 EnergySpill_PauseMax;
        public Double EnergySpill_SpaceshipResistance;
        #endregion

        #region Spaceship
        public Double Spaceship_Radius;
        public Double Spaceship_InitEnergy;
        public Double Spaceship_MaxAcceleration;
        public Double Spaceship_MaxTurnSpeed;
        public Double Spaceship_Resistance;
        public Int32 Spaceship_PlasmaInitCooldown;

        public Int32 Spaceship_CargoHold;
        public Int32 Spaceship_GravimineInitCooldown;
        public Int32 Spaceship_GravimineCapacity;

        public Int32 Spaceship_MissileInitCooldown;
        public Int32 Spaceship_MissileCapacity;

        public Int32 Spaceship_GravishieldCapacity;
        public Int32 Spaceship_DashCapacity;
        #endregion

        #region PlasmaBullet
        public Double PlasmaBullet_Radius;
        public Double PlasmaBullet_EnergyPower;
        public Double PlasmaBullet_Speed;
        public Double PlasmaBullet_Reward;
        #endregion

        #region GravimineBullet
        public Double GravimineBullet_Radius;
        public Double GravimineBullet_EnergyPower;
        public Double GravimineBullet_PushPower;
        public Double GravimineBullet_StartSpeed;
        public Double GravimineBullet_MinSpeed;
        public Double GravimineBullet_Resistance;
        public Double GravimineBullet_Reward;
        #endregion
        #endregion

        #region MissileBullet
        public Double MissileBullet_Radius;
        public Double MissileBullet_EnergyPower;
        public Double MissileBullet_PushPower;
        public Double MissileBullet_StartSpeed;
        public Double MissileBullet_MinSpeed;
        public Double MissileBullet_Resistance;
        public Double MissileBullet_Reward;
        #endregion

        #region Bonuses
        #region GravimineBonus
        public Double GravimineBonus_Radius;
        #endregion

        #region MissileBonus
        public Double MissileBonus_Radius;
        #endregion

        #region BoosterBonus
        public Double BoosterBonus_Radius;
        public Double BoosterBonus_Multiplier;
        public Int32 BoosterBonus_Duration;

        //   public Double FireBoosterBonus_MaxAcceleration;
        //   public Double FireBoosterBonus_RiseAcceleration;


        #endregion

        #region FireBoosterBonus
        public Double FireBoosterBonus_Radius;
        public Int32 FireBoosterBonus_NewCooldown;
        public Int32 FireBoosterBonus_Duration;

        //   public Double FireBoosterBonus_MaxAcceleration;
        //   public Double FireBoosterBonus_RiseAcceleration;


        #endregion

        #region GravishieldBonus
        public Double GravishieldBonus_Radius;
        public Int32 GravishieldBonus_InitStrength;
        public Int32 GravishieldBonus_Duration;
        #endregion


        #region DashBonus
        public Double DashBonus_Radius;
        public Int32 DashBonus_Boost;
        #endregion

        #endregion

        #region Bot
        public Int32 Bot_MessagesMaxTotalLength;
        #endregion

        #endregion

    }
}
