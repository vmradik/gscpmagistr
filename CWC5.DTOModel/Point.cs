﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CWC5.DTOModel
{
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 1)]
    public struct Point
    {
        public Point(Double x, Double y)
        {
            X = x;
            Y = y;
        }

        public Double X;
        public Double Y;
    }
}
