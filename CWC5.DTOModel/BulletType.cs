﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CWC5.DTOModel
{
    public enum BulletType
    {
        None = 0,
        Plasma = 1,
        Gravimine = 2,
        Missile = 3
    }
}
