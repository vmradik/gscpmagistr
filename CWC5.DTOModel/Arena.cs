﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CWC5.DTOModel
{
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 1)]
    public struct Arena
    {
        public Int32 Tick;

        public EnergySpill[] EnergySpills;

        public Spaceship Me;

        public Spaceship[] Enemies;

        public GravimineBonus[] GravimineBonuses;
        public MissileBonus[] MissileBonuses;


        public BoosterBonus[] BoosterBonuses;
        public FireBoosterBonus[] FireBoosterBonuses;

        public DashBonus[] DashBonuses;
        public GravishieldBonus[] GravishieldBonuses;
        

        public PlasmaBullet[] PlasmaBullets;

        public GravimineBullet[] GravimineBullets;

        public MissileBullet[] MissileBullets;
    }
}
