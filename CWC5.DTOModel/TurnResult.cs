﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CWC5.DTOModel
{
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 1)]
    public struct TurnResult
    {
        public Point Acceleration;

        public BulletType Bullet;

        public Double CannonDirection;

        public Boolean ActivateDash;

        public Double DashDirection;

        public Int32 TargetID;

        public Boolean ActivateShield;

        public String[] Messages;
    }
}
