﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CWC5.DTOModel
{
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 1)]
    public struct GravimineBullet
    {
        public PhysicalObject Body;

        public Int32 OwnerId;
    }
}
