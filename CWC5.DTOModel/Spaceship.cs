﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CWC5.DTOModel
{
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 1)]
    public struct Spaceship
    {
        public PhysicalObject Body;

        public Int32 SpaceshipID;

        public Double Energy;

        public Double Direction;

        public Boolean IsInEnergySpill
		{
			get { return _IsInEnergySpill != 0; }
			set { _IsInEnergySpill = value ? (Byte)1 : (Byte)0; }
		}
		public Byte _IsInEnergySpill;

		public Int32 PlasmaCooldown;

        public Int32 GravimineCooldown;

        public Int32 GravimineCount;


        public Int32 BonusCount => GravimineCount + MissileCount + GravishieldCount + DashCount;


        public Int32 GravishieldCount;

        public Int32 GravishieldStrength;


        public Int32 DashCount;


        public Int32 MissileCooldown;

        public Int32 MissileCount;

        public Int32 GravishieldRemainingTime;

        public Int32 BoosterRemainingTime;

        public Int32 FireBoosterRemainingTime;
    }
}
