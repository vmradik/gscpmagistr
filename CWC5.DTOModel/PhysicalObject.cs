﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CWC5.DTOModel
{
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 1)]
    public struct PhysicalObject
    {
        public Double Radius;
         
        public Point Location;
        
        public Point Speed;
    }
}
