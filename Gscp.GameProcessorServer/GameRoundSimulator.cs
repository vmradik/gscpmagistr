﻿using Gscp.Grpc_Definitions;
using GscpGrpc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gscp.GameProcessorServer
{
	public class GameRoundSimulator
	{
		public GameRoundSimulator(Server.RoundRequest request, IStrategy[] strategies)
		{
			RequestId = request.RequestId;
			GameRoundId = request.GameRoundId;
			Strategies = strategies;
		}

		public Guid RequestId { get; }
		public Int32 GameRoundId { get; }
		public IStrategy[] Strategies { get; }


		public Server.RoundResponse Run(GscpGrpc.ControllerService.ControllerServiceClient client)
		{
			var response = new Server.RoundResponse
			{
				RequestId = RequestId,
				Result = new Server.RoundResult { RoundId = GameRoundId },
			};

			var bots = new List<CWC5.Controller.BotInfo>();
			foreach (var item in Strategies)
			{
				bots.Add(new CWC5.Controller.BotInfo
				{
					StrategyID = item.StrategyId,
					Name = item.Name,
					ExecutableFilePath = item.Path,
					SaveLogMessages = false,
					ExecutableType = CWC5.Controller.Languages.GetExecutableType(item.LanguageId),
				});
			}

			var def_options = JsonConvert.SerializeObject(CWC5.Controller.Controller.DefaultOptions, Formatting.Indented, new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Objects,
				SerializationBinder = new CWC5.Controller.ControllerTypesBinder()
			});
			var bot_json = JsonConvert.SerializeObject(bots, Formatting.Indented, new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Objects,
				SerializationBinder = new CWC5.Controller.ControllerTypesBinder()
			});
			var req = new ModelingRequest()
			{
				DefaultOptions = def_options,
				Bots = bot_json
			};

			var res = client.DoModeling(req, deadline: Grpc_tools.get_grpc_deadline());

			var result_obj = JsonConvert.DeserializeObject< CWC5.Controller.ModelingResult>(res.ModelingResult, new JsonSerializerSettings
			{
				TypeNameHandling = TypeNameHandling.Objects,
				SerializationBinder = new CWC5.Controller.ControllerTypesBinder()
			});
			if (result_obj is CWC5.Controller.ModelingResult result)
			{
				if (result.Exception != null)
				{
					response.IsSuccess = false;
				}
				else
				{
					response.IsSuccess = true;
					response.Log = result.FightHistory;
					response.Result.Result = new byte[0];

					List<Server.RoundPlayer> playerResults = new List<Server.RoundPlayer>(Strategies.Length);
					foreach (var item in Strategies)
					{
						var pres = result.Score.Points.Single(p => p.Key == item.StrategyId).Value;
						playerResults.Add(new Server.RoundPlayer
						{
							StrategyId = item.StrategyId,
							PersonalResult = BitConverter.GetBytes(pres)
						});
					}
					response.Result.Players = playerResults.ToArray();
				}

				return response;
			}
			else
			{
				throw new Exception("received invalid object. /n " + res.ModelingResult);
			}
		}

		public Server.RoundResponse Run()
		{
			var response = new Server.RoundResponse
			{
				RequestId = RequestId,
				Result = new Server.RoundResult { RoundId = GameRoundId },
			};

			var bots = new List<CWC5.Controller.BotInfo>();
			foreach (var item in Strategies)
			{
				bots.Add(new CWC5.Controller.BotInfo
				{
					StrategyID = item.StrategyId,
					Name = item.Name,
					ExecutableFilePath = item.Path,
					SaveLogMessages = false,
					ExecutableType = CWC5.Controller.Languages.GetExecutableType(item.LanguageId),
				});
			}


			var result = CWC5.Controller.Controller.DoModeling(CWC5.Controller.Controller.DefaultOptions, bots);

			if (result.Exception != null)
			{
				response.IsSuccess = false;
			}
			else
			{
				response.IsSuccess = true;
				response.Log = result.FightHistory;
				response.Result.Result = new byte[0];

				List<Server.RoundPlayer> playerResults = new List<Server.RoundPlayer>(Strategies.Length);
				foreach (var item in Strategies)
				{
					var pres = result.Score.Points.Single(p => p.Key == item.StrategyId).Value;
					playerResults.Add(new Server.RoundPlayer
					{
						StrategyId = item.StrategyId,
						PersonalResult = BitConverter.GetBytes(pres)
					});
				}
				response.Result.Players = playerResults.ToArray();
			}

			return response;
		}

		GscpGrpc.Point fromDto(CWC5.DTOModel.Point point)
		{
			return new GscpGrpc.Point()
			{
				X = point.X,
				Y = point.Y
			};
		}

		CWC5.DTOModel.Point toDto(GscpGrpc.Point point)
		{
			return new CWC5.DTOModel.Point()
			{
				X = point.X,
				Y = point.Y
			};
		}

		GscpGrpc.PhysicalObject fromDto(CWC5.DTOModel.PhysicalObject obj)
		{
			return new GscpGrpc.PhysicalObject()
			{
				Radius = obj.Radius,
				Location = fromDto(obj.Location),
				Speed = fromDto(obj.Speed)
			};
		}

		GscpGrpc.Spaceship fromDto(CWC5.DTOModel.Spaceship s)
		{
			return new GscpGrpc.Spaceship()
			{
				Body = fromDto(s.Body),
				SpaceshipID = s.SpaceshipID,
				Energy = s.Energy,
				Direction = s.Direction,
				IsInEnergySpill = s.IsInEnergySpill,
				PlasmaCooldown = s.PlasmaCooldown,
				GravimineCooldown = s.GravimineCooldown,
				GravimineCount = s.GravimineCount,
				GravishieldCount = s.GravishieldCount,
				GravishieldStrength = s.GravishieldStrength,
				DashCount = s.DashCount,
				MissileCooldown = s.MissileCooldown,
				MissileCount = s.MissileCount,
				GravishieldRemainingTime = s.GravishieldRemainingTime,
				BoosterRemainingTime = s.BoosterRemainingTime,
				FireBoosterRemainingTime = s.FireBoosterRemainingTime
			};
		}

		public GscpGrpc.Point fromLogicalCore(CWC5.LogicalCore.DPoint point)
		{
			var p = new GscpGrpc.Point()
			{
				X = point.X,
				Y = point.Y
			};
			return p;
		}

		public GscpGrpc.FullGameOptions fromLogicalCore(CWC5.LogicalCore.Model.GameOptions options)
		{
			var opts = new GscpGrpc.FullGameOptions()
			{
				ArenaFightDuration = options.Arena_FightDuration,
				ArenaSize = fromLogicalCore(options.Arena_Size),
				BoosterBonusDuration = options.BoosterBonus_Duration,
				BoosterBonusMultiplier = options.BoosterBonus_Multiplier,
				BoosterBonusRadius = options.BoosterBonus_Radius,
				BonusesOnScreenLimit = options.BonusesOnScreenLimit,
				BoosterBonusMaxAcceleration = options.BoosterBonus_MaxAcceleration,
				BoosterBonusProbability = options.BoosterBonusProbability,
				BoosterBonusRiseAcceleration = options.BoosterBonus_RiseAcceleration,
				BotMessagesMaxTotalLength = options.Bot_MessagesMaxTotalLength,
				CloudCount = options.CloudCount,
				DashBonusProbability = options.DashBonusProbability,
				DeformationCoefficient = options.DeformationCoefficient,
				DashBonusBoost = options.DashBonus_Boost,
				DashBonusRadius = options.DashBonus_Radius,
				EnergySpillBorder = options.EnergySpill_Border,
				EnergySpillChangeDirectMax = options.EnergySpill_ChangeDirectMax,
				EnergySpillChangeDirectMin = options.EnergySpill_ChangeDirectMin,
				EnergySpillInitRemainingTime = options.EnergySpillInitRemainingTime,
				EnergySpillLifetimeMax = options.EnergySpill_LifetimeMax,
				EnergySpillLifetimeMin = options.EnergySpill_LifetimeMin,
				EnergySpillPauseMax = options.EnergySpill_PauseMax,
				EnergySpillPauseMin = options.EnergySpill_PauseMin,
				EnergySpillPauseRemainingTime = options.EnergySpillPauseRemainingTime,
				EnergySpillPower = options.EnergySpill_Power,
				EnergySpillRadiusChangeStepMax = options.EnergySpill_RadiusChangeStepMax,
				EnergySpillRadiusChangeStepMin = options.EnergySpill_RadiusChangeStepMin,
				EnergySpillRadiusChangeTickMax = options.EnergySpill_RadiusChangeTickMax,
				EnergySpillRadiusChangeTickMin = options.EnergySpill_RadiusChangeTickMin,
				EnergySpillRadiusMax = options.EnergySpill_RadiusMax,
				EnergySpillRadiusMin = options.EnergySpill_RadiusMin,
				EnergySpillSpaceshipResistance = options.EnergySpill_SpaceshipResistance,
				FireboosterBonusDuration = options.FireboosterBonus_Duration,
				FireboosterBonusNewCooldown = options.FireboosterBonus_NewCooldown,
				FireboosterBonusProbability = options.FireboosterBonusProbability,
				FireboosterBonusRadius = options.FireboosterBonus_Radius,
				GravimineBonusProbability = options.GravimineBonusProbability,
				GravimineBonusRadius = options.GravimineBonus_Radius,
				GravimineBulletEnergyPower = options.GravimineBullet_EnergyPower,
				GravimineBulletMinSpeed = options.GravimineBullet_MinSpeed,
				GravimineBulletPrice = options.GravimineBullet_Price,
				GravimineBulletPushPower = options.GravimineBullet_PushPower,
				GravimineBulletRadius = options.GravimineBullet_Radius,
				GravimineBulletResistance = options.GravimineBullet_Resistance,
				GravimineBulletStartSpeed = options.GravimineBullet_StartSpeed,
				GravimineSplashRadius = options.GravimineSplashRadius,
				GravishieldBonusDuration = options.GravishieldBonus_Duration,
				GravishieldBonusInitStrength = options.GravishieldBonus_InitStrength,
				GravishieldBonusProbability = options.GravishieldBonusProbability,
				GravishieldBonusRadius = options.GravishieldBonus_Radius,
				MissileBonusProbability = options.MissileBonusProbability,
				MissileBonusRadius = options.MissileBonus_Radius,
				MissileBulletEnergyPower = options.MissileBullet_EnergyPower,
				MissileBulletMinSpeed = options.MissileBullet_MinSpeed,
				MissileBulletPrice = options.MissileBullet_Price,
				MissileBulletPushPower = options.MissileBullet_PushPower,
				MissileBulletRadius = options.MissileBullet_Radius,
				MissileBulletResistance = options.MissileBullet_Resistance,
				MissileBulletStartSpeed = options.MissileBullet_StartSpeed,
				PlasmaBulletEnergyPower = options.PlasmaBullet_EnergyPower,
				PlasmaBulletPrice = options.PlasmaBullet_Price,
				PlasmaBulletRadius = options.PlasmaBullet_Radius,
				PlasmaBulletSpeed = options.PlasmaBullet_Speed,
				PlayerResponsePeriod = options.PlayerResponsePeriod,
				PlayersCount = options.PlayersCount,
				SpaceshipCargoHold = options.Spaceship_CargoHold,
				SpaceshipDashCapacity = options.Spaceship_DashCapacity,
				SpaceshipGravimineCapacity = options.Spaceship_GravimineCapacity,
				SpaceshipGravimineInitCooldown = options.Spaceship_GravimineInitCooldown,
				SpaceshipGravishieldCapacity = options.Spaceship_GravishieldCapacity,
				SpaceshipInitEnergy = options.Spaceship_InitEnergy,
				SpaceshipMaxAcceleration = options.Spaceship_MaxAcceleration,
				SpaceshipMaxTurnSpeed = options.Spaceship_MaxTurnSpeed.Radians,
				SpaceshipMissileCapacity = options.Spaceship_MissileCapacity,
				SpaceshipMissileInitCooldown = options.Spaceship_MissileInitCooldown,
				SpaceshipPlasmaInitCooldown = options.Spaceship_PlasmaInitCooldown,
				SpaceshipRadius = options.Spaceship_Radius,
				SpaceshipResistance = options.Spaceship_Resistance,
				ZoneCreate = options.zoneCreate
			};
			opts.PlayerRespawnPoints.AddRange(options.PlayerRespawnPoints.Select(p => fromLogicalCore(p)));
			return opts;
		}

		public CWC5.LogicalCore.DPoint toLogicalCore(GscpGrpc.Point point)
		{
			var p = new CWC5.LogicalCore.DPoint(point.X, point.Y);
			return p;
		}

		public CWC5.LogicalCore.Model.GameOptions toLogicalCore(GscpGrpc.FullGameOptions options)
		{
			var opts = new CWC5.LogicalCore.Model.GameOptions()
			{
				Arena_FightDuration = options.ArenaFightDuration,
				Arena_Size = toLogicalCore(options.ArenaSize),
				BonusesOnScreenLimit = options.BonusesOnScreenLimit,
				BoosterBonusProbability = options.BoosterBonusProbability,
				BoosterBonus_Duration = options.BoosterBonusDuration,
				BoosterBonus_MaxAcceleration = options.BoosterBonusMaxAcceleration,
				BoosterBonus_Multiplier = options.BoosterBonusMultiplier,
				BoosterBonus_Radius = options.BoosterBonusRadius,
				BoosterBonus_RiseAcceleration = options.BoosterBonusRiseAcceleration,
				Bot_MessagesMaxTotalLength = options.BotMessagesMaxTotalLength,
				CloudCount = options.CloudCount,
				DashBonusProbability = options.DashBonusProbability,
				DashBonus_Boost = options.DashBonusBoost,
				DashBonus_Radius = options.DashBonusRadius,
				DeformationCoefficient = options.DeformationCoefficient,
				EnergySpillInitRemainingTime = options.EnergySpillInitRemainingTime,
				EnergySpillPauseRemainingTime = options.EnergySpillPauseRemainingTime,
				EnergySpill_Border = options.EnergySpillBorder,
				EnergySpill_ChangeDirectMax = options.EnergySpillChangeDirectMax,
				EnergySpill_ChangeDirectMin = options.EnergySpillChangeDirectMin,
				EnergySpill_LifetimeMax = options.EnergySpillLifetimeMax,
				EnergySpill_LifetimeMin = options.EnergySpillLifetimeMin,
				EnergySpill_PauseMax = options.EnergySpillPauseMax,
				EnergySpill_PauseMin = options.EnergySpillPauseMin,
				EnergySpill_Power = options.EnergySpillPower,
				EnergySpill_RadiusChangeStepMax = options.EnergySpillRadiusChangeStepMax,
				EnergySpill_RadiusChangeStepMin = options.EnergySpillRadiusChangeStepMin,
				EnergySpill_RadiusChangeTickMax = options.EnergySpillRadiusChangeTickMax,
				EnergySpill_RadiusChangeTickMin = options.EnergySpillRadiusChangeTickMin,
				EnergySpill_RadiusMax = options.EnergySpillRadiusMax,
				EnergySpill_RadiusMin = options.EnergySpillRadiusMin,
				EnergySpill_SpaceshipResistance = options.SpaceshipResistance,
				FireboosterBonusProbability = options.FireboosterBonusProbability,
				FireboosterBonus_Duration = options.FireboosterBonusDuration,
				FireboosterBonus_NewCooldown = options.FireboosterBonusNewCooldown,
				FireboosterBonus_Radius = options.FireboosterBonusRadius,
				GravimineBonusProbability = options.GravimineBonusProbability,
				GravimineBonus_Radius = options.GravimineBonusRadius,
				GravimineBullet_EnergyPower = options.GravimineBulletEnergyPower,
				GravimineBullet_MinSpeed = options.GravimineBulletMinSpeed,
				GravimineBullet_Price = options.GravimineBulletPrice,
				GravimineBullet_PushPower = options.GravimineBulletPushPower,
				GravimineBullet_Radius = options.GravimineBulletRadius,
				GravimineBullet_Resistance = options.GravimineBulletResistance,
				GravimineBullet_StartSpeed = options.GravimineBulletStartSpeed,
				GravimineSplashRadius = options.GravimineSplashRadius,
				GravishieldBonusProbability = options.GravishieldBonusProbability,
				GravishieldBonus_Duration = options.GravishieldBonusDuration,
				GravishieldBonus_InitStrength = options.GravishieldBonusInitStrength,
				GravishieldBonus_Radius = options.GravishieldBonusRadius,
				MissileBonusProbability = options.MissileBonusProbability,
				MissileBonus_Radius = options.MissileBonusRadius,
				MissileBullet_EnergyPower = options.MissileBulletEnergyPower,
				MissileBullet_MinSpeed = options.MissileBulletMinSpeed,
				MissileBullet_Price = options.MissileBulletPrice,
				MissileBullet_PushPower = options.MissileBulletPushPower,
				MissileBullet_Radius = options.MissileBulletRadius,
				MissileBullet_Resistance = options.MissileBulletResistance,
				MissileBullet_StartSpeed = options.MissileBulletStartSpeed,
				PlasmaBullet_EnergyPower = options.PlasmaBulletEnergyPower,
				PlasmaBullet_Price = options.PlasmaBulletPrice,
				PlasmaBullet_Radius = options.PlasmaBulletRadius,
				PlasmaBullet_Speed = options.PlasmaBulletSpeed,
				PlayerRespawnPoints = options.PlayerRespawnPoints.Select(p => toLogicalCore(p)).ToArray(),
				PlayerResponsePeriod = options.PlayerResponsePeriod,
				PlayersCount = options.PlayersCount,
				Spaceship_CargoHold = options.SpaceshipCargoHold,
				Spaceship_PlasmaInitCooldown = options.SpaceshipPlasmaInitCooldown,
				Spaceship_DashCapacity = options.SpaceshipDashCapacity,
				Spaceship_GravimineCapacity = options.SpaceshipGravimineCapacity,
				Spaceship_GravimineInitCooldown = options.SpaceshipGravimineInitCooldown,
				Spaceship_GravishieldCapacity = options.SpaceshipGravishieldCapacity,
				Spaceship_InitEnergy = options.SpaceshipInitEnergy,
				Spaceship_MaxAcceleration = options.SpaceshipMaxAcceleration,
				Spaceship_MaxTurnSpeed = new CWC5.LogicalCore.Angle(options.SpaceshipMaxTurnSpeed),
				Spaceship_MissileCapacity = options.SpaceshipMissileCapacity,
				Spaceship_MissileInitCooldown = options.SpaceshipMissileInitCooldown,
				Spaceship_Radius = options.SpaceshipRadius,
				Spaceship_Resistance = options.SpaceshipResistance,
				zoneCreate = options.ZoneCreate
			};
			return opts;
		}

		/*
		public GscpGrpc.LogicalCoreArena fromLogicalCore(CWC5.LogicalCore.Model.Arena arena)
		{
			var a = new GscpGrpc.LogicalCoreArena()
			{
				Tick = arena.Tick,
				GameOptions = fromLogicalCore(arena.Options)
			};
			a.EnergySpills.AddRange(arena.Bonuses.OfType<CWC5.LogicalCore.Model.EnergySpill>().Select(spill => new GscpGrpc.EnergySpill()
			{
				RemainingTime = spill.RemainingTime,
				Body = fromDto(spill.Body),
			}));

			a.Spaceships.AddRange(arena.Bonuses.OfType<CWC5.DTOModel.Spaceship>().Select(spill => new GscpGrpc.Spaceship()
			{
				RemainingTime = spill.RemainingTime,
				Body = fromDto(spill.Body),
			}));
			repeated Spaceship Spaceships = 4;
			repeated GravimineBonus GravimineBonuses = 5;
			repeated MissileBonus MissileBonuses = 6;
			repeated BoosterBonus BoosterBonuses = 7;
			repeated FireBoosterBonus FireBoosterBonuses = 8;
			repeated DashBonus DashBonuses = 9;
			repeated GravishieldBonus GravishieldBonuses = 10;
			repeated PlasmaBullet PlasmaBullets = 11;
			repeated GravimineBullet GravimineBullets = 12;
			repeated MissileBullet MissileBullets = 13;
			repeated FightEvent events = 14;

			a.Enemies.AddRange(arena.Enemies.Select(enemy => fromDto(enemy)));
			a.GravimineBonuses.AddRange(arena.GravimineBonuses.Select(gb => new GscpGrpc.GravimineBonus()
			{
				Body = fromDto(gb.Body),
			}));
			a.MissileBonuses.AddRange(arena.MissileBonuses.Select(gb => new GscpGrpc.MissileBonus()
			{
				Body = fromDto(gb.Body),
			}));
			a.BoosterBonuses.AddRange(arena.BoosterBonuses.Select(gb => new GscpGrpc.BoosterBonus()
			{
				Body = fromDto(gb.Body),
			}));
			a.FireBoosterBonuses.AddRange(arena.FireBoosterBonuses.Select(gb => new GscpGrpc.FireBoosterBonus()
			{
				Body = fromDto(gb.Body),
			}));
			a.GravishieldBonuses.AddRange(arena.GravishieldBonuses.Select(gb => new GscpGrpc.GravishieldBonus()
			{
				Body = fromDto(gb.Body),
			}));
			a.PlasmaBullets.AddRange(arena.PlasmaBullets.Select(gb => new GscpGrpc.PlasmaBullet()
			{
				Body = fromDto(gb.Body),
				OwnerId = gb.OwnerId
			}));
			a.GravimineBullets.AddRange(arena.GravimineBullets.Select(gb => new GscpGrpc.GravimineBullet()
			{
				Body = fromDto(gb.Body),
				OwnerId = gb.OwnerId
			}));
			a.MissileBullets.AddRange(arena.MissileBullets.Select(gb => new GscpGrpc.MissileBullet()
			{
				Body = fromDto(gb.Body),
				OwnerId = gb.OwnerId,
				TargetId = gb.TargetId
			}));

			return a;
		}
		*/
	}
}
