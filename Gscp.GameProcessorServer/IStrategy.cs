﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gscp.GameProcessorServer
{
	public interface IStrategy
	{
		Int32 StrategyId { get; }
		String LanguageId { get; }
		String Name { get; }
		String Path { get; }

	}
}
