﻿using Grpc.Core;
using Gscp.Network;
using GscpGrpc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace Gscp.GameProcessorServer
{

	public class GameProcessorWorker : IDisposable
    {

        public void Run()
        {
            CreateGameProcessorServiceHost("localhost", 50010, 50011, "gameprocessor", "gameprocessorpass");
        }

        private void CreateGameProcessorServiceHost(String base_address, int server_port, int game_processor_port, string user, string password)
        {
            Dispose();

			_console.ReportInfo("Connecting...");
			var channel = new Channel(base_address, server_port, ChannelCredentials.Insecure);
			var client = new GameProcessorEndpoint.GameProcessorEndpointClient(channel);
			if (channel.State == ChannelState.Idle)
			{
				_console.ReportInfo("Connected to the server.");
			}
			else
			{
				_console.ReportInfo("Cannot connect!");
				return;
			}
			_processor = new GameProcessor(_console, client);
			var services = GscpGrpc.GameProcessor.BindService(_processor);
			var ports = new ServerPort(base_address, game_processor_port, ServerCredentials.Insecure);
			var server = new Grpc.Core.Server()
			{
				Services = { services },
				Ports = { ports }
			};
			server.Start();
			_console.ReportInfo("Listener started.");
		}

        private readonly IConsole _console;
		public GameProcessorWorker(IConsole console)
		{
			_console = console;
		}

		private GameProcessor _processor;
		

		public void Dispose()
		{
		}
	}
}
