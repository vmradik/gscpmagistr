﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gscp.GameProcessorServer
{
	public class RatingCalculator
	{
		public RatingCalculator(Server.RatingCalculationRequest request)
		{
			RequestId = request.RequestId;
			_ratings = request.InitialRatings.ToDictionary(r => r.StrategyId, r => r.Value);
			_events = request.Events;
		}

		public Guid RequestId { get; }

		private Dictionary<Int32, Int64> _ratings;
		private void SetNewRating(Int32 id, Int64 value)
		{
			if (!_ratings.ContainsKey(id))
			{
				_ratings.Add(id, value);
			}
			else
			{
				_ratings[id] = value;
			}
		}

		private Server.RatedEventBase[] _events;

		public Server.RatingCalculationResponse Calculate()
		{
			List<Server.RatingsResult> results = new List<Server.RatingsResult>(_events.Length);
			foreach (var ev in _events)
			{
				Server.RatingsResult result = null;
				if (ev is Server.StrategyUpdate)
				{
					result = Process((Server.StrategyUpdate)ev);
				}
				else if (ev is Server.RoundResult)
				{
					result = Process((Server.RoundResult)ev);
				}

				if (result != null)
				{
					foreach (var item in result.Ratings)
					{
						SetNewRating(item.StrategyId, item.Value);
					}
				}
				results.Add(result);
			}

			return new Server.RatingCalculationResponse
			{
				RequestId = RequestId,
				RatingResults = results.ToArray(),
			};
		}


		private const Int64 _defaultRating = 1000; 

		private Server.RatingsResult Process(Server.StrategyUpdate stratUpdate)
		{
			Server.StrategyRating result = new Server.StrategyRating { StrategyId = stratUpdate.NewStrategyId };

			if (stratUpdate.OldStrategyId == null)
			{
				result.Value = _defaultRating;
			}
			else
			{
				result.Value = _ratings[stratUpdate.OldStrategyId.Value];
			}

			return new Server.RatingsResult { Ratings = new Server.StrategyRating[] { result } };
		}

		private class Participant
		{
			public Participant(Server.RoundPlayer player, Int64 oldRating)
			{
				StrategyId = player.StrategyId;
				Points = BitConverter.ToDouble(player.PersonalResult, 0);
				OldRatingPresize = oldRating;
				Addition = 0;
			}

			public Int32 StrategyId { get; }
			public Int64 OldRatingPresize { get; }
			public Double OldRating { get { return (Double)OldRatingPresize; } }
			public Double Points { get; }

			public Int32 Rank { get; set; }

			public void AddResult(Participant other, Double k)
			{
				Double den = 1 + Math.Pow(10, (other.OldRating - OldRating)/400);
				Double p = 1 / den;
				Double pts = other.Rank < Rank ? 0 : other.Rank > Rank ? 1 : 0.5;
				Addition += k * (pts - p);
			}

			public Double Addition { get; set; }

			public Server.StrategyRating CreateResult()
			{
				return new Server.StrategyRating
				{
					StrategyId = StrategyId,
					Value = (Int64)Math.Round(OldRating + Addition),
				};
			}
		}

		private Server.RatingsResult OrderResult(Server.RoundPlayer[] players, IEnumerable<Participant> participants)
		{
			List<Server.StrategyRating> result = new List<Server.StrategyRating>();
			foreach (var player in players)
			{
				result.Add(participants.First(p => p.StrategyId == player.StrategyId).CreateResult());
			}
			return new Server.RatingsResult
			{
				Ratings = result.ToArray()
			};
		}


		private const Double PointsEpsilon = 1e-3;
		private const Double TotalK = 32;
		private Server.RatingsResult Process(Server.RoundResult round)
		{
			List<Participant> participants = round.Players
				.Select(p => new Participant(p, _ratings[p.StrategyId]))
				.ToList();

			if (participants.Count == 0)
			{
				return new Server.RatingsResult { Ratings = new Server.StrategyRating[0] };
			}
			else if (participants.Count == 1)
			{
				var p = participants.Single();
				return new Server.RatingsResult { Ratings = new Server.StrategyRating[] { p.CreateResult() } };
			}

			participants.Sort((p1, p2) => -1 * p1.Points.CompareTo(p2.Points));
			for (int i = 0; i < participants.Count; i++)
			{
				participants[i].Rank = i + 1;
				if (i > 0 && Math.Abs(participants[i].Points - participants[i-1].Points) < PointsEpsilon)
				{
					participants[i].Rank = participants[i - 1].Rank;
				}
			}

			Double k = TotalK / (participants.Count - 1);
			for (int i = 0; i < participants.Count; i++)
			{
				for (int j = 0; j < participants.Count; j++)
				{
					if (i == j) continue;
					participants[i].AddResult(participants[j], k);
				}
			}

			return OrderResult(round.Players, participants);
		}
	}
}
