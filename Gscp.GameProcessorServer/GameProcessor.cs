﻿using Grpc.Core;
using Gscp.Grpc_Definitions;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Gscp.GameProcessorServer
{
    using grpc = global::Grpc.Core;
    public class GameProcessor : GscpGrpc.GameProcessor.GameProcessorBase
    {
        Logger Log = LogManager.GetLogger(nameof(GameProcessor));

        private readonly IConsole _console;
        //private readonly Server.IGameProcessorService _server;

        // nomdusk Список библиотек, используемых стратегиями на С#
        private string[] Required_C_Sharp_Libs = { "CWC5.DotNet.dll", "CWC5.DTOModel.dll", "Google.Protobuf.dll", "Grpc.Core.Api.dll",
            "Grpc.Core.dll", "grpc_csharp_ext.x64.dll", "grpc_csharp_ext.x86.dll", "Gscp.Grpc_Definitions.dll",
            "System.Buffers.dll", "System.Memory.dll", "System.Runtime.CompilerServices.Unsafe.dll"};
        private string executable_path;

        GscpGrpc.GameProcessorEndpoint.GameProcessorEndpointClient server_connection;

        public GameProcessor(IConsole console, GscpGrpc.GameProcessorEndpoint.GameProcessorEndpointClient server)
        {
            _console = console;
            server_connection = server;
            // nomdusk Восстановим библиотеки, необходимые для стратегий на шарпах. 
            // Эти библиотеки должны быть в одной папке с геймпроцессором (нужные проекты добавлены в зависимости проекта геймпроцессора)
            executable_path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            var Included_C_Sharp_Libs = Required_C_Sharp_Libs.Select(file => $"{executable_path}\\strategy_files\\{file}").ToArray();
            if (Included_C_Sharp_Libs.Any(lib => !File.Exists($"{lib}")))
            {
                // nomdusk ой, какой-то из либ нет в папке геймпроцессора...
                var lib = Included_C_Sharp_Libs.FirstOrDefault(l => !File.Exists($"{l}"));
                throw new Exception($"Library \"{lib}\" is required for c# projects but not included in game processor root folder \"{executable_path}\\strategy_files\"");
            }
            // nomdusk Проверим папку, в которй запускаются стратегии, и скопируем туда недостающие либы
            Directory.CreateDirectory(StrategiesFolder);
            DirectoryInfo d = new DirectoryInfo($"{executable_path}\\strategy_files\\");
            FileInfo[] Files = d.GetFiles();
            foreach (FileInfo file in Files)
            {
                if (!File.Exists($"{StrategiesFolder}\\{file.Name}"))
                {
                    try
                    {
                        File.Copy($"{file.FullName}", $"{StrategiesFolder}\\{file.Name}");
                    } catch (Exception e)
                    {
                        Log.Error(e, $"Cannot copy file {file.Name}");
                    }
                }
            }
            LoadCache();
            SendReady();
        }

        public override async global::System.Threading.Tasks.Task StartSimulation(grpc::IAsyncStreamReader<global::GscpGrpc.RoundRequest> requestStream, grpc::IServerStreamWriter<global::GscpGrpc.RoundResponse> responseStream, grpc::ServerCallContext context)
        {
            _console.SetCurrentRequestPackage($"Game round simulations package");
            while (await requestStream.MoveNext(Grpc_tools.get_grpc_deadline_token()))
            {
                try
                {
                    if (context.CancellationToken.IsCancellationRequested) { break; }
                    var r = requestStream.Current;
                    Server.RoundRequest request = Grpc_conversion.from_grpc(r);

                    var req_result = await ProcessRequests(request);

                    GscpGrpc.RoundResponse resp = Grpc_conversion.to_grpc(req_result);
                    await responseStream.WriteAsync(resp);
                    _console.AddToRoundsSimulated(1);

                } catch (Exception e)
                {
                    Log.Error(e, "StartSimulation raised exception");
                }
            }
            _console.SetCurrentRequestPackage(null);
            SendReady();
        }

        public override async global::System.Threading.Tasks.Task StartRatingCalculations(grpc::IAsyncStreamReader<global::GscpGrpc.RatingCalculationRequest> requestStream, grpc::IServerStreamWriter<global::GscpGrpc.RatingCalculationResponse> responseStream, grpc::ServerCallContext context)
        {
            _console.SetCurrentRequestPackage($"Rating calculation package");
            //var task = requestStream.MoveNext();
            //task.Wait(Grpc_tools.get_grpc_timespan());
            while (await requestStream.MoveNext(Grpc_tools.get_grpc_deadline_token()))
            {
                try
                {
                    if (context.CancellationToken.IsCancellationRequested) {
                        break;
                    }
                    var r = requestStream.Current;
                    Server.RatingCalculationRequest request = Grpc_conversion.from_grpc(r);

                    var calc_resp = ProcessRequests(request);

                    var response = Grpc_conversion.to_grpc(calc_resp);
                    await responseStream.WriteAsync(response);
                }
                catch (Exception e)
                {
                    Log.Error(e, "StartRatingCalculations raised exception");
                }
                //task = requestStream.MoveNext();
                //task.Wait(Grpc_tools.get_grpc_timespan());
            }
            _console.SetCurrentRequestPackage(null);
            SendReady();
        }

        public void SendReady()
        {
            Google.Protobuf.WellKnownTypes.Int32Value val = new Google.Protobuf.WellKnownTypes.Int32Value();
            val.Value = _console.MaxRequestSimulationsCount;
            server_connection.RequestSimulations(val, deadline: Grpc_tools.get_grpc_deadline());
            //_server.RequestSimulations(_console.MaxRequestSimulationsCount);
        }

        public Server.RatingCalculationResponse ProcessRequests(Server.RatingCalculationRequest item)
        {
            _console.SetCurrentRequest(item.RequestId, $"Rating calculation request [{item.Events.Length}]");
            var calc = new RatingCalculator(item);
            var ret = calc.Calculate();
            _console.AddToRatingRequests(1);
            return ret;
        }

        private static readonly String _strategiesRegistryName = "strat_registry.xml";
        private XDocument _strategiesRegistry;

        private Dictionary<Int32, CachedStrategy> _cachedStrategies;
        private static readonly String _cacheFolder = "CachedStrategies";
        private static readonly String _stratPrefix = "strat_";

        private static String StrategiesFolder
        {
            get
            {
                return Path.Combine(Environment.CurrentDirectory, _cacheFolder);
            }
        }

        private static String CreateStrategyPath(Int32 id)
        {
            return Path.Combine(StrategiesFolder, _stratPrefix + id + "_" + Guid.NewGuid());
        }

        private class CachedStrategy : IStrategy
        {
            public static readonly String ElementName = "Strategy";

            public CachedStrategy(XElement element)
            {
                Element = element;
            }

            public CachedStrategy(Server.Strategy strategy)
            {
                Element = new XElement(ElementName,
                    new XAttribute(nameof(StrategyId), strategy.StrategyId),
                    new XAttribute(nameof(LanguageId), strategy.LanguageId),
                    new XAttribute(nameof(Name), strategy.Name),
                    new XAttribute(nameof(Path), CreateStrategyPath(strategy.StrategyId))
                    );
            }

            public XElement Element { get; }

            public string LanguageId
            {
                get { return Element.Attribute(nameof(LanguageId)).Value; }
            }

            public string Name
            {
                get { return Element.Attribute(nameof(Name)).Value; }
            }

            public string Path
            {
                get { return Element.Attribute(nameof(Path)).Value; }
            }

            public int StrategyId
            {
                get { return Int32.Parse(Element.Attribute(nameof(StrategyId)).Value); }
            }
        }

		private void LoadCache()
		{
			Directory.CreateDirectory(StrategiesFolder);
			if (!File.Exists(_strategiesRegistryName))
			{
				_strategiesRegistry = new XDocument(
					new XElement("Root")
					);
				_strategiesRegistry.Save(_strategiesRegistryName);
				_cachedStrategies = new Dictionary<int, CachedStrategy>();
			}
			else
			{
				_strategiesRegistry = XDocument.Load(_strategiesRegistryName);
				_cachedStrategies = _strategiesRegistry.Root.Elements(CachedStrategy.ElementName)
					.Select(e => new CachedStrategy(e))
					.ToDictionary(s => s.StrategyId, s => s);
			}
		}

		private void CommitCache()
		{
			_strategiesRegistry.Save(_strategiesRegistryName);
		}

		private void AddStrategyToCache(Server.Strategy strategy)
		{
			var s = new CachedStrategy(strategy);
			var path = s.Path;
			File.WriteAllBytes(path, strategy.Binary);
			_strategiesRegistry.Root.Add(s.Element);
			_cachedStrategies.Add(s.StrategyId, s);
		}

		public async Task<Server.RoundResponse> ProcessRequests(Server.RoundRequest request)
		{
            var strategyIds = request.Players.Select(p => p.StrategyId).ToList();
			
			_console.SetCurrentRequestPackage($"{strategyIds.Count} distinct strategies found", true);

			var strategiesToLoad = strategyIds.Where(s => !_cachedStrategies.ContainsKey(s)).ToArray();

			_console.SetCurrentRequestPackage($"{strategiesToLoad.Length} strategies loading from server", true);

			if (strategiesToLoad.Any())
			{
                List<Server.Strategy> strategies = new List<Server.Strategy>();
                using (var stream = server_connection.RequestStrategies(deadline: Grpc_tools.get_grpc_deadline()))
                {
                    foreach (var s in strategiesToLoad)
                    {
                        Google.Protobuf.WellKnownTypes.Int32Value val = new Google.Protobuf.WellKnownTypes.Int32Value();
                        val.Value = s;
                        await stream.RequestStream.WriteAsync(val);
                        //if(stream.ResponseStream.MoveNext().Wait(Grpc_tools.get_grpc_timespan()))
                    }
                    await stream.RequestStream.CompleteAsync();
                    while (await stream.ResponseStream.MoveNext(Grpc_tools.get_grpc_deadline_token()))
                    {
                        var strat = stream.ResponseStream.Current;
                        strategies.Add(Grpc_conversion.from_grpc(strat));
                    }
                }
				foreach (var strat in strategies)
				{
					AddStrategyToCache(strat);
				}
				CommitCache();
			}

			_console.SetCurrentRequestPackage($"{strategiesToLoad.Length} strategies loaded and cached", true);


            try
            {
                Channel channel;
                GscpGrpc.ControllerService.ControllerServiceClient client = null;
                Environment.SetEnvironmentVariable("NO_PROXY", "localhost");
                string address_path = "./settings.json";
                JObject o = JObject.Parse(File.ReadAllText(address_path));

                bool local;

                if (!o.ContainsKey("controllers"))
                {
                    local = true;
                }
                else
                {
                    JArray controllers = o["controllers"].Value<JArray>();
                    if (!controllers.HasValues)
                    {
                        local = true;
                    }
                    else
                    {
                        JObject contr = controllers[0].Value<JObject>();
                        if (!contr.ContainsKey("host") || !contr.ContainsKey("port"))
                        {
                            throw new Exception("invalid connection data");
                        }
                        local = false;
                        string c_host = contr["host"].ToString();
                        int c_port = contr["port"].Value<int>();
                        //port = 52001;
                        channel = new Channel(c_host, c_port, ChannelCredentials.Insecure);
                        client = new GscpGrpc.ControllerService.ControllerServiceClient(channel);
                    }
                }

                Server.RoundResponse result;
                if (local)
                {
                    _console.SetCurrentRequest(request.RequestId, $"Round #{request.GameRoundId} simulation with {request.Players.Length} players");
                    var sim = new GameRoundSimulator(request, request.Players.Select(p => (IStrategy)_cachedStrategies[p.StrategyId]).ToArray());
                    result = sim.Run();
                    _console.SetCurrentRequest(request.RequestId, $"Simulation completed", true);
                }
                else
                {
                    _console.SetCurrentRequest(request.RequestId, $"Round #{request.GameRoundId} simulation with {request.Players.Length} players");
                    var sim = new GameRoundSimulator(request, request.Players.Select(p => (IStrategy)_cachedStrategies[p.StrategyId]).ToArray());
                    result = sim.Run(client);
                    _console.SetCurrentRequest(request.RequestId, $"Simulation completed", true);
                }

                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error while requesting game round smulation");
                return null;
            }
		}

	}
}
