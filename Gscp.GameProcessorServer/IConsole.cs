﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gscp.GameProcessorServer
{
	public interface IConsole
	{
		Int32 MaxRequestSimulationsCount { get; } 
		void ReportInfo(string format, params object[] args);
		void ReportError(string format, params object[] args);

		void AddToRoundsSimulated(Int32 count);
		void AddToRatingRequests(Int32 count);

		void SetCurrentRequestPackage(String description, Boolean append = false);
		void SetCurrentRequest(Guid id, String description, Boolean append = false);
	}
}
