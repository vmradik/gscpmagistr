﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gscp.GameProcessorServer
{
    public static class Grpc_conversion
    {
        public static Server.RatingCalculationRequest from_grpc(GscpGrpc.RatingCalculationRequest r)
        {
            Server.RatingCalculationRequest request = new Server.RatingCalculationRequest()
            {
                RequestId = Guid.Parse(r.RequestId),
                InitialRatings = r.InitialRatings.Select(p => new Server.StrategyRating()
                {
                    StrategyId = p.StrategyId,
                    Value = p.Value
                }).ToArray(),
                Events = r.ResultEvents.Select(p => (Server.RatedEventBase)(new Server.RoundResult()
                {
                    RoundId = p.RoundId,
                    Result = p.Result.ToByteArray(),
                    Players = p.Players.Select(rpl => new Server.RoundPlayer()
                    {
                        StrategyId = rpl.StrategyId,
                        PersonalResult = rpl.PersonalResult.ToByteArray()
                    }).ToArray()
                })).Concat(r.UpdateEvents.Select(ue => new Server.StrategyUpdate()
                {
                    NewStrategyId = ue.NewStrategyId,
                    OldStrategyId = ue.OldStrategyId
                })).ToArray()
            };
            return request;
        }

        public static GscpGrpc.RatingCalculationResponse to_grpc(Server.RatingCalculationResponse r)
        {
            var response = new GscpGrpc.RatingCalculationResponse()
            {
                RequestId = r.RequestId.ToString()
            };
            response.RatingResults.AddRange(r.RatingResults.Select(rr => {
                var ret = new GscpGrpc.RatingResult();
                ret.Ratings.AddRange(rr.Ratings.Select(rt => new GscpGrpc.StrategyRating()
                {
                    StrategyId = rt.StrategyId,
                    Value = rt.Value
                }));
                return ret;
            }));
            return response;
        }

        public static Server.Strategy from_grpc(GscpGrpc.Strategy s)
        {
            return new Server.Strategy()
            {
                StrategyId = s.StrategyId,
                Name = s.Name,
                LanguageId = s.LanguageId,
                Binary = s.Binary.ToByteArray()
            };
        }

        public static GscpGrpc.RoundResponse to_grpc(Server.RoundResponse res)
        {
            GscpGrpc.RoundResult rr = new GscpGrpc.RoundResult()
            {
                RoundId = res.Result.RoundId,
                Result = Google.Protobuf.ByteString.CopyFrom(res.Result.Result ?? new byte[0])
            };
            if (res.Result.Players?.Any() ?? false)
            {
                rr.Players.AddRange(res.Result.Players.Select(pl => new GscpGrpc.RoundPlayer()
                {
                    StrategyId = pl.StrategyId,
                    PersonalResult = Google.Protobuf.ByteString.CopyFrom(pl.PersonalResult ?? new byte[0])
                }));
            }
            GscpGrpc.RoundResponse resp = new GscpGrpc.RoundResponse()
            {
                IsSuccess = res.IsSuccess,
                RequestId = res.RequestId.ToString(),
                Result = rr,
                Log = Google.Protobuf.ByteString.CopyFrom(res.Log ?? new byte[0])
            };
            return resp;
        }

        public static Server.RoundRequest from_grpc(GscpGrpc.RoundRequest r)
        {
            Server.RoundRequest request = new Server.RoundRequest()
            {
                GameRoundId = r.GameRoundId,
                RequestId = Guid.Parse(r.RequestId),
                Players = r.Players.Select(p => new Server.RoundPlayer()
                {
                    StrategyId = p.StrategyId,
                    PersonalResult = p.PersonalResult.ToByteArray()
                }).ToArray()
            };
            return request;
        }
    }
}
