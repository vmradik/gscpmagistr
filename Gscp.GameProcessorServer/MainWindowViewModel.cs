﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Gscp.GameProcessorServer
{
	public class MainWindowViewModel : BindableBase, IConsole
	{
		public MainWindowViewModel()
		{
			_dispatcher = Dispatcher.CurrentDispatcher;
			_maxRequestSimulationsCount = 2;
			_worker = new GameProcessorWorker(this);
			_worker.Run();
		}

		private GameProcessorWorker _worker;
		private Dispatcher _dispatcher;

		private void InvokeInUI(Action action)
		{
			_dispatcher.BeginInvoke(action);
		}


		private String _errors;
		public String Errors
		{
			get { return _errors; }
			set { SetProperty(ref _errors, value); }
		}

		private void AddMessage(String type, string format, params object[] args)
		{
			var message = $"{DateTime.Now.ToString("dd.MM.yyyy HH.mm.ss.fff")} {type} {String.Format(format, args)}\n\n";
			Errors = message + Errors;
		}

		void IConsole.ReportInfo(string format, params object[] args)
		{
			InvokeInUI(() => AddMessage("INF", format, args));
		}

		void IConsole.ReportError(string format, params object[] args)
		{
			InvokeInUI(() => AddMessage("ERR", format, args));
		}


		private String _currentAction;
		public String CurrentAction
		{
			get { return _currentAction; }
			set { SetProperty(ref _currentAction, value); }
		}

		private String _packageDesc = String.Empty;
		private String _requestDesc = String.Empty;
		private void UpdateCurrentAction()
		{
			CurrentAction = $"PACKAGE:\n{_packageDesc}\nREQUEST:\n{_requestDesc}";
		}

		void IConsole.SetCurrentRequestPackage(string description, Boolean append)
		{
			InvokeInUI(() =>
			{
				if (append)
				{
					_packageDesc += '\n';
					_packageDesc += description ?? String.Empty;
				}
				else
				{
					_requestDesc = String.Empty;
					_packageDesc = description ?? String.Empty;
				}
				UpdateCurrentAction();
			});
		}

		void IConsole.SetCurrentRequest(Guid id, string description, Boolean append)
		{
			InvokeInUI(() =>
			{
				if (append)
				{
					_requestDesc += '\n';
					_requestDesc += description ?? String.Empty;
				}
				else
				{
					_requestDesc = $"Id: {id}\n{description ?? String.Empty}";
				}
				UpdateCurrentAction();
			});
		}



		private Int32 _maxRequestSimulationsCount;
		public Int32 MaxRequestSimulationsCount
		{
			get { return _maxRequestSimulationsCount; }
			set { SetProperty(ref _maxRequestSimulationsCount, value); }
		}


		private Int32 _roundsSimulated = 0;
		public Int32 RoundsSimulated
		{
			get { return _roundsSimulated; }
			set { SetProperty(ref _roundsSimulated, value); }
		}

		void IConsole.AddToRoundsSimulated(int count)
		{
			InvokeInUI(() => RoundsSimulated += count);
		}


		private Int32 _ratingRequests = 0;
		public Int32 RatingRequests
		{
			get { return _ratingRequests; }
			set { SetProperty(ref _ratingRequests, value); }
		}

		void IConsole.AddToRatingRequests(int count)
		{
			InvokeInUI(() => RatingRequests += count);
		}

		


	}
}
