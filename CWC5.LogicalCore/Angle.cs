﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore
{
    [Serializable]
    public class Angle
    {
        public static readonly Double TwoPi = 2 * Math.PI;
        public static Angle Zero
        {
            get { return new Angle(0.0); }
        }

        public Angle(DPoint vector)
        {
            Vector = vector;
        }  
        
        public Angle(Double angle)
        {
            Radians = angle;
        }

        public Angle()
            : this(Zero.Radians)
        {

        }

        protected Double _radians;
        public Double Radians 
        {
            get { return _radians; }
            protected set { _radians = value; }
        }

        public DPoint Vector
        {
            get { return new DPoint(Math.Cos(Radians), Math.Sin(Radians)); }
            protected set { Radians = Math.Atan2(value.Y, value.X); }
        }

        /// <summary>
        /// Returns inverse angle relate to zero angle
        /// </summary>
        /// <returns>New angle</returns>
        public Angle Invert()
        {
            return new Angle(-Radians);
        }

        /// <summary>
        /// Summ radians values
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public Angle Add(Angle angle)
        {
            return new Angle(Radians + angle.Radians);
        }

        public Angle Abs()
        {
            return new Angle(Math.Abs(Radians));
        }

        public Angle360 GetAngle360()
        {
            return new Angle360(Radians);
        }

        public Angle GetAngle()
        {
            return new Angle(Radians);
        }

    }
}
