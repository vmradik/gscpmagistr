﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore.FightStory
{
    [Serializable]
    public class FightStoryContainer
    {
        public FightStoryContainer(Int32 framesCount, List<BotInfo> botsInfo)
        {
            _frames = new List<Frame>();
            for (int i = 0; i <= framesCount; i++)
            {
                _frames.Add(null);
            }

            _botsInfo = botsInfo;
        }

        public void AddFrame(Frame frame)
        {
            _frames[frame.Tick] = frame;
        }

        private List<Frame> _frames;
        public Frame[] Frames
        {
            get { return _frames.ToArray(); }
        }
        
        private List<BotInfo> _botsInfo;
        public BotInfo[] BotsInfo
        {
            get { return _botsInfo.ToArray(); }
        }

        public List<KeyValuePair<Int32, Double>> GetFinalPoints()
        {
            return _frames
                .Last()
                .Players
                .Select(p => new KeyValuePair<Int32, Double>(_botsInfo[p.PlayerID].StrategyID, p.Points))
                .ToList();
        }        
    }
}
