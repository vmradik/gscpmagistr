﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.FightStory
{
    [Serializable]
    public class Frame : ArenaObjectContainer
    {
        public Frame(Arena arena)
        {
            Tick = arena.Tick;
            ArenaSize = arena.Size;
            CloneContent(arena);
        }

        public DPoint ArenaSize
        {
            get;
            set;
        }
        
        public List<FightEvent> Events
        {
            get;
            set;
        }

        public List<String> Messages
        {
            get;
            set;
        }

        public Int32 Tick
        {
            get;
            set;
        }

    }
}
