﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CWC5.LogicalCore.FightStory
{
    public class FightStoryStream
    {
        public FightStoryStream(Stream stream)
        {
            _stream = stream;
        }

        private Stream _stream;

        public FightStoryContainer ReadFightStory()
        {
            GZipStream cs = new GZipStream(_stream, CompressionMode.Decompress);
            IFormatter ser = new BinaryFormatter();
            return (FightStoryContainer)ser.Deserialize(cs);
            //return (FightStoryContainer)ser.Deserialize(_stream);
        }

        public Boolean WriteFightStory(FightStoryContainer story)
        {
            using (GZipStream cs = new GZipStream(_stream, CompressionMode.Compress))
            {
                var ms = new MemoryStream();
                IFormatter ser = new BinaryFormatter();
                ser.Serialize(cs, story);
            }
           // ser.Serialize(_stream, story);
            return true;
        }
        
    }
}
