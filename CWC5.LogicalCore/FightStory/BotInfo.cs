﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore.FightStory
{
    [Serializable]
    public class BotInfo
    {
        public Int32 StrategyID { get; set; }
        public Int32 UserID { get; set; }
        public String Name { get; set; }
    }
}
