﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore
{
    [Serializable]
    public class Angle360 : Angle
    {
        public static new Angle360 Zero
        {
            get { return Angle.Zero.GetAngle360(); }
        }

        public Angle360(DPoint vector)
        {
            Vector = vector.Clone();
        }  
        
        public Angle360(Double angle)
        {
            Radians = angle;
        }

        public Angle360()
            : this(Zero.Radians)
        {

        }

        public new Double Radians
        {
            get { return _radians; }
            protected set { _radians = value - TwoPi * Math.Floor(value / TwoPi); }
        }


        public new Angle360 Invert()
        {
            return new Angle360(-Radians);
        }

        public new Angle360 Add(Angle angle)
        {
            return new Angle360(angle.Add(this).Radians);
        }
        
        public Angle360 Add(Angle360 angle)
        {
            return Add((Angle)angle);
        }

        public Angle360 Substract(Angle360 angle)
        {
            return Add(((Angle)angle).Invert());
        }



    }
}
