﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWC5.LogicalCore.Model
{
    public class ModelingOptions : GameOptions
    {
        public ModelingOptions()
            : base()
        {

        }

        public TimeSpan TimeToBotResponse { get; set; }

        public GameOptions GetGameOptions()
        {
            return (GameOptions)base.MemberwiseClone();
        }

        public ModelingOptions Clone()
        {
            return (ModelingOptions)this.MemberwiseClone();
        }
    }
}
