﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public class Gravimine : Bullet
    {
        public Gravimine(Double power, Double splashRadius, Double price = 0)
        {
            Price = price;
            SplashRadius = splashRadius;
            Power = power;
        }

        public Double SplashRadius
        {
            get;
            set;
        }

        public Double Power { get; set; }
        public Double Price
        {
            get;
            set;
        }
    }
}
