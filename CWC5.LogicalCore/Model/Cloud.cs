﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public class Cloud : ArenaObject
    {      

        public Cloud(Double GravimineProbability, Double MissileProbability, Double GravishieldProbability, Double BoosterProbability, Double FireBoosterProbability, Double DashProbability)
        {
            _splashProb = GravimineProbability;
            _missileProb = MissileProbability;

            _shieldProb = GravishieldProbability;
            _dashProb = DashProbability;

            _boosterProb = BoosterProbability;
            _fireBoosterProb = FireBoosterProbability;
        }

        [NonSerialized]
        private Double _missileProb;
        private Double _splashProb;
        private Double _shieldProb;
        private Double _boosterProb;
        private Double _fireBoosterProb;
        private Double _dashProb;

        /* public Boolean CanGetEnergySpill()
         {
             return MagicRandom.NextDouble()
                 <= (1.0 - 1 / Math.Pow(Points + 1, 1.0/Math.Log(_const+1, 100.0/99.0)));
         }*/
        public Boolean CanGetEnergySpill(int TimePause)
        {
            if (TimePause == 0) return true;
            return false;
        }

        public Boolean CanGetMissileBonus()
        {
            return MagicRandom.NextDouble() <= _missileProb;
        }
        
        public Boolean CanGetGravimineBonus()
        {
            return MagicRandom.NextDouble() <= _splashProb;
        }



        public Boolean CanGetGravishieldBonus()
        {
            return MagicRandom.NextDouble() <= _shieldProb;
        }

        public Boolean CanGetDashBonus()
        {
            return MagicRandom.NextDouble() <= _dashProb;
        }


        public Boolean CanGetBoosterBonus()
        {
            return MagicRandom.NextDouble() <= _boosterProb;
        }
        public Boolean CanGetFireBoosterBonus()
        {
            return MagicRandom.NextDouble() <= _fireBoosterProb;
        }


        public Double Points { get; set; }

    }
}
