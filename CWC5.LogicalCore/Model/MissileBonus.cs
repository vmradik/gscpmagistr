﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public class MissileBonus : Bonus
    {
        public MissileBonus(DPoint dest)
        {
            destination = dest;
        }
    }
}
