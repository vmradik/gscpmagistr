﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public class DashBonus : Bonus
    {
        public DashBonus(DPoint dest)
        {
            destination = dest;
        }
    }
}
