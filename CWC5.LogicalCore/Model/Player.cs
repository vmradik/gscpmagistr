﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public class Player : PhysicalObject
    {
        public Player(Int32 id)
        {
            PlayerID = id;
           // _enginePowerCoefficient = enginePowerCoefficient;
        }
        
        public Int32 PlayerID
        {
            get;
            set;
        }

        public Boolean IsAlive
        {
            get;
            set;
        }

        /// <summary>
        /// Получает или задает направление ствола пушки игрока
        /// </summary>
        public Angle360 Direction
        {
            get;
            set;
        }

        private Double _points;

        public Double Points
        {
            get { return _points; }
            set { _points = value; if (_points < 0.0) _points = 0.0; }
        }

        public Int32 BonusCount => GravimineCount + MissileCount + GravishieldCount + DashCount;

        public Int32 PlasmaCooldown { get; set; }

        public Int32 GravimineCount { get; set; }

        public Int32 GravimineCooldown { get; set; }

        public Int32 MissileCount { get; set; }

        public Int32 MissileCooldown { get; set; }


        public Int32 GravishieldCount { get; set; }

        public Int32 GravishieldStrength { get; set; }

        public Int32 GravishieldRemainingtime { get; set; }



        public Int32 DashCount { get; set; }





        public Int32 FireBoosterRemainingTime { get; set; }

        public Int32 BoosterRemainingTime { get; set; }

		public Boolean IsGravishieldActive
		{
			get
			{
				return GravishieldRemainingtime != 0 && GravishieldStrength != 0;
			}
			set
			{
				GravishieldRemainingtime = 0;
				GravishieldStrength = 0;
			}
		}
        
        public Boolean InZone { get; set; }

        //[NonSerialized]
        //private Double _enginePowerCoefficient;

        [NonSerialized]
        private DPoint _lastAcceleration;
        internal DPoint LastAcceleration
        {
            get { return _lastAcceleration; }
            set { _lastAcceleration = value; }
        }
        
        public void DropPlasma(Int32 cooldown)
        {
            PlasmaCooldown = cooldown;
        }

        public void DropGravimine()
        {
            GravimineCount--;
        }
        public void DropMissile()
        {
            MissileCount--;
        }





    }
}
