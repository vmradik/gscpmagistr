﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWC5.LogicalCore.Model
{
    public class KnownTypesBinder : ISerializationBinder
    {
        public IList<Type> KnownTypes { get; set; }

        public KnownTypesBinder()
        {
            KnownTypes = new List<Type>();
        }

        public Type BindToType(string assemblyName, string typeName)
        {
            return KnownTypes.SingleOrDefault(t => t.Name == typeName);
        }

        public void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;
            typeName = serializedType.Name;
        }
    }

    public class LogicalCoreTypesBinder : KnownTypesBinder
    {
        public LogicalCoreTypesBinder() : base()
        {
            KnownTypes.Add(typeof(Base.ArenaObject));
            KnownTypes.Add(typeof(Base.ArenaObjectContainer));
            KnownTypes.Add(typeof(Base.Bonus));
            KnownTypes.Add(typeof(Base.Bullet));
            KnownTypes.Add(typeof(Base.PhysicalObject));
            KnownTypes.Add(typeof(Arena));
            KnownTypes.Add(typeof(BoosterBonus));
            KnownTypes.Add(typeof(Cloud));
            KnownTypes.Add(typeof(DashBonus));
            KnownTypes.Add(typeof(EnergySpill));
            KnownTypes.Add(typeof(FightEvent));
            KnownTypes.Add(typeof(FightEventType));
            KnownTypes.Add(typeof(FireboosterBonus));
            KnownTypes.Add(typeof(GameOptions));
            KnownTypes.Add(typeof(ModelingOptions));
            KnownTypes.Add(typeof(Gravimine));
            KnownTypes.Add(typeof(GravimineBonus));
            KnownTypes.Add(typeof(GravishieldBonus));
            KnownTypes.Add(typeof(MissileBonus));
            KnownTypes.Add(typeof(Plasma));
            KnownTypes.Add(typeof(Player));
            KnownTypes.Add(typeof(Angle));
            KnownTypes.Add(typeof(Angle360));
            KnownTypes.Add(typeof(DPoint));
            KnownTypes.Add(typeof(DTOModel.Point));
            KnownTypes.Add(typeof(DTOModel.BulletType));
            KnownTypes.Add(typeof(Missile));
            KnownTypes.Add(typeof(DTOModel.TurnResult));
            KnownTypes.Add(typeof(DTOModel.TurnResult?));
        }
    }
}
