﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public enum FightEventType
    {
        FirePlasma,
        FireGravimine,
        FireMissile,
        BonusAppearance,
        BonusCaught,
        PlasmaExploded,
        GravimineExploded,
        MissileExploded  
    }
}
