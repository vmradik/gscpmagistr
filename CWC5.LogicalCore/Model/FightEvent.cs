﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public class FightEvent
    {
        public DPoint Location
        {
            get;
            set;
        }

        public FightEventType Type
        {
            get;
            set;
        }
                  
    }
}
