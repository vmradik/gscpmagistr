﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public class EnergySpill : Bonus
    {
        public EnergySpill(Double points, Int32 remainingTime, DPoint dest)
        {
            destination = dest;
            Points = points;
            RemainingTime = remainingTime;
        }

        public static double changeRadiusStep(GameOptions Options) {
            return (Options.EnergySpill_RadiusChangeStepMin +
                    MagicRandom.NextDouble() * (Options.EnergySpill_RadiusChangeStepMax-Options.EnergySpill_RadiusChangeStepMin));
        }

        public static int changeRadiusTick(GameOptions Options)
        {
            double min = (Options.EnergySpill_RadiusChangeTickMin * 1.5);
            double max = Math.Max(min, Options.EnergySpill_RadiusChangeTickMax);
            return (int)(min + MagicRandom.NextDouble()*(max - min));
         }


        public Double Points
        { get; set; }
        public Double ChangeDirectTick { get; set; }
        public Int32 RemainingTime { get; set; }

        public Double ChangeRadiusTick { get; set; }
        public Double ChangeRadiusStep { get; set; }
    }
}
