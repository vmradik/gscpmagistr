﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore.Model.Base
{
    /// <summary>
    /// Базовый класс для всех снарядов
    /// </summary>
    [Serializable]
    public class Bullet : PhysicalObject
    {
        /// <summary>
        /// PlayerID бота 
        /// </summary>
        public Int32 Owner { get; set; }
    }
}
