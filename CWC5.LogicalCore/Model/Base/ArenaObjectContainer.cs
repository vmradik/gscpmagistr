﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore.Model.Base
{
    /// <summary>
    /// Экземпляр данного класса содержит объекты, находящиеся на арене
    /// </summary>
    [Serializable]
    public abstract class ArenaObjectContainer
    {
        /// <summary>
        /// Копирует ссылки на объекты другого ArenaObjectContainer в данный объект
        /// </summary>
        /// <param name="container">Исходный контейнер</param>
        protected void SetContent(ArenaObjectContainer container)
        {
            _objects = container._objects.ToList();
        }

        /// <summary>
        /// Клонирует объекты другого контейнера.
        /// </summary>
        /// <param name="container">Исходный контейнер</param>
        protected void CloneContent(ArenaObjectContainer container)
        {
            _objects = container._objects.Select(o => (ArenaObject)o.Clone()).ToList();
        }

        /// <summary>
        /// Все объекты арены
        /// </summary>
        public List<ArenaObject> _objects;

        /// <summary>
        /// Получает игроков арены
        /// </summary>
        public Player[] Players
        {
            get { return _objects.OfType<Player>().ToArray(); }
        }

        /// <summary>
        /// Получает снаряды арены
        /// </summary>
        public Bullet[] Bullets
        {
            get { return _objects.OfType<Bullet>().ToArray(); }
        }

        /// <summary>
        /// Получает бонусы арены
        /// </summary>
        public Bonus[] Bonuses
        {
            get { return _objects.OfType<Bonus>().ToArray(); }
        }
        
        /// <summary>
        /// Получает количество бонусов на арене
        /// </summary>
        public int BonusCount
        {
            get { return _objects.OfType<Bonus>().Count() - _objects.OfType<EnergySpill>().Count(); }
        }

        /// <summary>
        /// Получает количество бонусов на арене
        /// </summary>
        public int SpillCount
        {
            get { return _objects.OfType<EnergySpill>().Count(); }
        }

        /// <summary>
        /// Получает облака арены
        /// </summary>
        public Cloud[] Clouds
        {
            get { return _objects.OfType<Cloud>().ToArray(); }
        }

        public PhysicalObject[] PhysicalObjects
        {
            get { return _objects.OfType<PhysicalObject>().ToArray(); }
        }
    }
}
