﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore.Model.Base
{
    /// <summary>
    /// Базовый класс для всех объектов, находящихся на арене
    /// </summary>
    [Serializable]
    public class ArenaObject : ICloneable
    {
        public ArenaObject()
        {
            ArenaObjectGuid = Guid.NewGuid();
        }

        [NonSerialized]
        private Int32 _appearenceTick;

        public Int32 AppearenceTick
        {
            get { return _appearenceTick; }
            set { _appearenceTick = value; }
        }


        public Guid ArenaObjectGuid
        {
            get;
            set;
        }
                

        #region ICloneable Members

        /// <summary>
        /// Выполняет поверхностное клонирование объекта.
        /// В конкретной модели все классы, наследованные от ArenaObject
        /// содержат только свойства-значения, так что клонирование можно считатть глубоким
        /// </summary>
        /// <returns>Клонированный объект</returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
