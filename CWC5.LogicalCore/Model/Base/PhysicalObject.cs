﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore.Model.Base
{
    /// <summary>
    /// Базовый класс для физического объекта на арене
    /// </summary>
    [Serializable]
    public class PhysicalObject : ArenaObject
    {
        /// <summary>
        /// Получает или задает радиус объекта
        /// </summary>
        public Double Radius
        {
            get;
            set;
        }

        /// <summary>
        /// Получает или задает позицию объекта на арене
        /// </summary>
        public DPoint Location
        {
            get;
            set;
        }
        
        /// <summary>
        /// Получает или задает скорость объекта
        /// </summary>
        public DPoint Speed
        {
            get;
            set;
        }

        /// <summary>
        /// Получает или задает коэффициент сопротивления среды для объекта
        /// </summary>
        public Double ResistanceCoefficient
        {
            get;
            set;
        }

    }
}
