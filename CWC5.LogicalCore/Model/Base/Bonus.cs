﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore.Model.Base
{
    /// <summary>
    /// Базовый класс для всех бонусов
    /// </summary>
    [Serializable]
    public class Bonus : PhysicalObject
    {
        public DPoint destination;
        public DPoint startLocation;
        public static double yOnStraight(double x, double x1, double x2, double y1, double y2)
        {
            return (x * (y2 - y1) + x2 * y1 - x1 * y2) / (x2 - x1);
            // return (y2 - y1) * (x - x1) / (x2 - x1) + y1;
        }

        public static double distance(DPoint begin, DPoint end)
        {
            return Math.Sqrt(Math.Pow((end.X - begin.X), 2) + Math.Pow((end.Y - begin.Y), 2));
        }

        public static Double randAngl()
        {
            return (Math.PI/4 + MagicRandom.NextDouble() * (Math.PI * 2-Math.PI/2- Math.PI / 4));
        }
        public static DPoint turnLeft(double angl, DPoint v)
        {
            return new DPoint(v.X*Math.Cos(angl) - v.Y*Math.Sin(angl), v.X * Math.Sin(angl) + v.Y * Math.Cos(angl));
        }
        public static DPoint genericDest(DPoint size, DPoint loc)
        {
            Double angl=0;

                angl = randAngl(); DPoint vct = new DPoint(0, size.Y);
                if (loc.X < size.X / 2)
                {
                    if (loc.Y < size.Y / 2)
                    {
                   //     angl += 3 * Math.PI / 2;
                        vct = new DPoint(0, 0);

                    }
                }
                else
                {
                    if (loc.Y < size.Y / 2)
                    {
                     //   angl += Math.PI;
                        vct = new DPoint(size.X, 0);
                    }
                    else
                    {
                       // angl += Math.PI / 2;
                        vct = new DPoint(size.X, size.Y);
                    }
                }
                vct=vct.Substract(loc);
                if (vct.X == 0 && vct.Y == 0)
                    vct = new DPoint(-size.X, -size.Y);


            /*
                       double xMin, xMax;
                       double yMax, yMin;
                       if (loc.X < size.X / 2)
                       {
                           xMin = loc.X;
                           xMax = size.X;
                       }
                       else
                       {
                           xMin = 0;
                           xMax = loc.X;
                       }

                       if (loc.Y < size.Y / 2)
                       {
                           yMin = loc.Y;
                           yMax = size.Y;
                       }
                       else
                       {
                           yMin = 0;
                           yMax = loc.Y;
                       }

                          DPoint rez = new DPoint(
                                      xMin + MagicRandom.NextDouble() * (xMax),
                                      yMin + MagicRandom.NextDouble() * (yMax)
                                   );
                                   */

            DPoint rez = turnLeft(angl,vct);
            //  rez.Add(new DPoint(((rez.X >= Arena.Size.X) ? 2000 : -2000), ((rez.Y >= Arena.Size.Y) ? 2000 : -2000)));
            return rez;
        }


        public static double yOnStraight(double x, DPoint loc, DPoint dst)
        {
            //return (x * (dst.Y - loc.Y) + destination.X * loc.Y - loc.X * dst.Y) / (destination.X - loc.X);
            return (dst.Y - loc.Y) * (x - loc.X) / (dst.X - loc.X) + loc.Y;
        }

        public static double xOnStraight(double y, DPoint loc, DPoint dst)
        {
            //return (x * (dst.Y - loc.Y) + destination.X * loc.Y - loc.X * dst.Y) / (destination.X - loc.X);
            return (dst.X - loc.X) * (y - loc.Y) / (dst.Y - loc.Y) + loc.X;
        }


        public static DPoint choiseDirect(DPoint loc, DPoint size, DPoint dst, DPoint dst2, int tick)
        {

            // dst2 = new DPoint(0, yOnStraight(0, loc, dst));
            DPoint point1 = new DPoint(size.X + 1, yOnStraight(size.X + 1, loc, dst));
            DPoint point2 = new DPoint(size.X - 1, yOnStraight(size.X - 1, loc, dst));


            double length1 = distance(point1, dst2);
            double length2 = distance(point2, dst2);

            DPoint vct;
            if (length1 < length2)
                vct = point1.Substract(loc);
            else
                vct = point2.Substract(loc);
            vct = vct.Normalize();

            point1 = loc.Add(vct);
            length1 = distance(point1, dst2);
            length2 = distance(loc, dst2);
            if (length1 < length2)
                return vct;
            else
                return vct.Invert();






        }

        public static double inWindow(double x, DPoint loc, DPoint dst, DPoint size)
        {
            int y = (int)yOnStraight(x, loc, dst);
            if (y > 0 && y < size.Y)
                return 1;
            else if (y == 0 || y == size.Y)
                return 0;
            else return -1;
        }

        public static double dihot(double a, double b, DPoint loc, DPoint dst, DPoint size)
        {
            double c, rez;
            while (b - a > 0.01)
            {
                c = (a + b) / 2;
                rez = inWindow(c, loc, dst, size);
                if (rez > 0)
                    b = c;
                else if (rez == 0)
                    return c;
                else
                    a = c;
            };
            return -65535;
        }
        public static DPoint moveBonusToPoint(DPoint dst, DPoint loc, DPoint size, int tick)
        {



            /*
                        tick = tick;
                        double rez1 = dihot(loc.X - 1, size.X + 1, loc, dst, size);
                        double rez2 = dihot(-1, loc.X + 1, loc, dst, size);
                        if (rez1 != -65535)
                            rez1 = distance(loc, new DPoint(rez1, yOnStraight(rez1, loc, dst)));
                        if (rez2 != -65535)
                            rez2 = distance(loc, new DPoint(rez2, yOnStraight(rez2, loc, dst)));
                        double rez;
                        if (rez1 > rez2)
                        {
                            rez = rez1;
                            if (rez1 == -65535)
                                return new DPoint(0, 0);

                        }
                        else
                        {
                            rez = rez2;
                            if (rez2 == -65535)
                                return new DPoint(0, 0);
                        }

                        DPoint rezPoint = new DPoint(rez, yOnStraight(rez, loc, dst));
                        return (loc.Substract(rezPoint)).Normalize();
                        //return (choiseDirect(loc, size, dst, rezPoint, tick));
                        //return new DPoint(0, 0);
            */
            // return (dst.Substract(loc)).Normalize();
            return dst.Normalize();
        }
    }

}
