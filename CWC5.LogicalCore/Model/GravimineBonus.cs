﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public class GravimineBonus : Bonus
    {
        public GravimineBonus(DPoint dest)
        {
            destination = dest;
        }
    }
}
