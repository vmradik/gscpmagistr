﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.FightStory;
using CWC5.LogicalCore.Model.Base;
using Newtonsoft.Json;

namespace CWC5.LogicalCore.Model
{
    public class Arena : ArenaObjectContainer
    {
        public Arena()
        {

        }

        public Arena(GameOptions options)
        {
            Options = options;

            _objects = new List<ArenaObject>();

            events = new List<FightEvent>();
        }


        public Arena(Frame frame, GameOptions options)
        {
            Options = options;

            _objects = new List<ArenaObject>();

            events = new List<FightEvent>();

            Tick = frame.Tick;

            CloneContent(frame);
        }

        public List<FightEvent> events;

        public GameOptions Options
        {
            get;
            set;
        }

        [JsonIgnore]
        public DPoint Size
        {
            get { return Options.Arena_Size; }
        }



        public Int32 Tick
        { get; set; }

        public IEnumerable<TObject> GetLastTickObjects<TObject>() where TObject: ArenaObject
        {
            return _objects.OfType<TObject>().Where(o => o.AppearenceTick == Tick);
        }

        public IEnumerable<TObject> GetNewTickObjects<TObject>(int tick) where TObject : ArenaObject
        {
            return _objects.OfType<TObject>().Where(o => o.AppearenceTick == tick);
        }


        internal void CreateObject(ArenaObject obj)
        {
            _objects.Add(obj);
        }

        internal void RemoveObject(ArenaObject obj)
        {
            _objects.Remove(obj);
        }

        public Player GetPlayer(Int32 id)
        {
            return Players.Single(p => p.PlayerID == id);
        }


    }
}
