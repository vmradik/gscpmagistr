﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model.Base;
using CWC5.LogicalCore.Model;

namespace CWC5.LogicalCore.Model
{
    public class GameOptions
    {

        #region Arena

        public DPoint Arena_Size { get; set; }
        public Int32 Arena_FightDuration { get; set; }

        #endregion

        #region Players

        public Double Spaceship_Radius { get; set; }
        public Double Spaceship_InitEnergy { get; set; }
        public Double Spaceship_MaxAcceleration { get; set; }
        public Angle Spaceship_MaxTurnSpeed { get; set; }
        public Double Spaceship_Resistance { get; set; }
        public Int32 Spaceship_PlasmaInitCooldown { get; set; }



        public Int32 Spaceship_CargoHold { get; set; }
        public Int32 Spaceship_GravimineInitCooldown { get; set; }
        public Int32 Spaceship_GravimineCapacity { get; set; }

        public Int32 Spaceship_MissileInitCooldown { get; set; }
        public Int32 Spaceship_MissileCapacity { get; set; }

        public Int32 Spaceship_GravishieldCapacity { get; set; }
        public Int32 Spaceship_DashCapacity { get; set; }

        #endregion

        #region Bonuses

        public Int32 BonusesOnScreenLimit { get; set; }

        public Double EnergySpill_Power { get; set; }

        public Double EnergySpill_Border { get; set; }
        public Double EnergySpill_ChangeDirectMin { get; set; }
        public Double EnergySpill_ChangeDirectMax { get; set; }



        public Double EnergySpill_RadiusMin { get; set; }
        public Double EnergySpill_RadiusMax { get; set; }

        public Double EnergySpill_RadiusChangeStepMin { get; set; }
        public Double EnergySpill_RadiusChangeStepMax { get; set; }
        public Double EnergySpill_RadiusChangeTickMin { get; set; }
        public Double EnergySpill_RadiusChangeTickMax { get; set; }

        public Int32 EnergySpill_LifetimeMin { get; set; }
        public Int32 EnergySpill_LifetimeMax { get; set; }
        public Int32 EnergySpill_PauseMin { get; set; }
        public Int32 EnergySpill_PauseMax { get; set; }
        public Double EnergySpill_SpaceshipResistance { get; set; }


        public Double MissileBonus_Radius { get; set; }
        public Double GravimineBonus_Radius { get; set; }

        public Double BoosterBonus_Radius { get; set; }
        public Double BoosterBonus_Multiplier { get; set; }
        public Int32 BoosterBonus_Duration { get; set; }

        public Double BoosterBonus_MaxAcceleration { get; set; }
        public Double BoosterBonus_RiseAcceleration { get; set; }

        public Double FireboosterBonus_Radius { get; set; }
        public Int32 FireboosterBonus_NewCooldown { get; set; }
        public Int32 FireboosterBonus_Duration { get; set; }

        //public Double FireboosterBonus_MaxAcceleration { get; set; }
        //public Double FireboosterBonus_RiseAcceleration { get; set; }


        public Double GravishieldBonus_Radius { get; set; }
        public Int32 GravishieldBonus_InitStrength { get; set; }
        public Int32 GravishieldBonus_Duration { get; set; }

        public Double DashBonus_Radius { get; set; }
        public Int32 DashBonus_Boost { get; set; }


        #endregion

        #region Bullets

        /// <summary>
        /// Радиус простого снаряда
        /// </summary>
        public Double PlasmaBullet_Radius { get; set; }
        public Double PlasmaBullet_EnergyPower { get; set; }
        public Double PlasmaBullet_Speed { get; set; }
        public Double PlasmaBullet_Price { get; set; }

        public Double MissileBullet_Radius { get; set; }
        public Double MissileBullet_EnergyPower { get; set; }
        public Double MissileBullet_PushPower { get; set; }
        public Double MissileBullet_StartSpeed { get; set; }
        public Double MissileBullet_MinSpeed { get; set; }
        public Double MissileBullet_Resistance { get; set; }
        public Double MissileBullet_Price { get; set; }

        public Double GravimineBullet_Radius { get; set; }
        public Double GravimineBullet_EnergyPower { get; set; }
        public Double GravimineBullet_PushPower { get; set; }
        public Double GravimineBullet_StartSpeed { get; set; }
        public Double GravimineBullet_MinSpeed { get; set; }
        public Double GravimineBullet_Resistance { get; set; }
        public Double GravimineBullet_Price { get; set; }

        /// <summary>
        /// Радиус сплеша
        /// </summary>
        public Double GravimineSplashRadius { get; set; }
        #endregion

        #region Bot

        /// <summary>
        /// Максимальная суммарная длина всех сообщений бота за 1 бой
        /// </summary>
        public Int32 Bot_MessagesMaxTotalLength { get; set; }

        #endregion
        
        /// <summary>
        /// Точки, в которых появляются игроки
        /// </summary>
        public DPoint[] PlayerRespawnPoints { get; set; }

        public Double GravimineBonusProbability { get; set; }
        public Double MissileBonusProbability { get; set; }
        
        public Double GravishieldBonusProbability { get; set; }
        public Double DashBonusProbability { get; set; }

        public Double BoosterBonusProbability { get; set; }
        public Double FireboosterBonusProbability { get; set; }


        /// <summary>
        /// Количество облаков
        /// </summary>
        public Int32 CloudCount { get; set; }

        /// <summary>
        /// Количество игроков
        /// </summary>
        public Int32 PlayersCount { get; set; }

        /// <summary>
        /// Коэффициент деформации
        /// </summary>
        public Double DeformationCoefficient { get; set; }

        /// <summary>
        /// allways 1
        /// </summary>
        public Int32 PlayerResponsePeriod { get; set; }



        /// <summary>
        ///Do not set
        /// </summary>
        public Int32 EnergySpillInitRemainingTime { get; set; }
        /// <summary>
        ///Do not set
        /// </summary>
        public Int32 EnergySpillPauseRemainingTime { get; set; }
        /// <summary>
        ///Do not set
        /// </summary>
        public bool zoneCreate;
    }
}
