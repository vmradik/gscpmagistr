﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public class Plasma : Bullet
    {
        public Plasma(Double power, Double price = 0)
        {
            Price = price;
            Power = power;
        }


        public Double Price
        {
            get;
            set;
        }

        public Double Power
        {
            get;
            set;
        }

    }
}
