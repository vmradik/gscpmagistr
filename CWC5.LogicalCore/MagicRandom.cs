﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore
{
    public static class MagicRandom
    {
        static MagicRandom()
        {
            _random = new Random((Int32)DateTime.Now.Ticks);
        }
        private static Random _random;

        public static Double NextDouble()
        {
            return _random.NextDouble();
        }

        public static Int32 Next(Int32 min, Int32 max)
        {
            return _random.Next(min, max);
        }


    }
}
