﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Controller
{
    internal class ObjectCreator : ControllerBase
    {
        public ObjectCreator(Arena arena)
            : base(arena)
        {

        }

        public void CreatePlayer(Int32 id)
        {
            Angle360 a = new Angle360(new DPoint(1, 1));
            if (id == 0)
            {
                a = new Angle360(new DPoint(-1, -1));
            }
            else if (id == 1)
            {
                a = new Angle360(new DPoint(1, 1));
            }
            else if (id == 2)
            {
                a = new Angle360(new DPoint(-1, 1));
            }
            else if (id == 3)
            {
                a = new Angle360(new DPoint(1, -1));
            }


            arena.CreateObject(new Player(id)
            {
                AppearenceTick = arena.Tick,
                IsAlive = true,
                Location = arena.Options.PlayerRespawnPoints[id],
                Direction = a,
                ResistanceCoefficient = arena.Options.Spaceship_Resistance,
                Radius = arena.Options.Spaceship_Radius,
                Speed = DPoint.Zero,
                PlasmaCooldown = 0,
                Points = arena.Options.Spaceship_InitEnergy
            });
        }

        public void CreateCloud()
        {
            arena.CreateObject(new Cloud(
                arena.Options.GravimineBonusProbability,
                arena.Options.MissileBonusProbability,
                arena.Options.GravishieldBonusProbability,
                arena.Options.DashBonusProbability,
                arena.Options.BoosterBonusProbability,
                arena.Options.FireboosterBonusProbability)
            {
                AppearenceTick = arena.Tick,
                Points = 0
            });

        }

        public void CreateEnergySpill(Double _Raduis, Double points, DPoint location, DPoint speed, DPoint dest, double stepChangeRadius, int changeRadiusTick)
        {
            arena.CreateObject(new EnergySpill(points, arena.Options.EnergySpillInitRemainingTime, dest)
            {
                AppearenceTick = arena.Tick,
                // Direction = Angle360.Zero,
                Radius = _Raduis,
                ChangeDirectTick = MagicRandom.Next((int)arena.Options.EnergySpill_ChangeDirectMin, (int)arena.Options.EnergySpill_ChangeDirectMax),

                Location = location.Clone(),
                startLocation = location.Clone(),
                Speed = speed.Clone(),
                ChangeRadiusTick = changeRadiusTick,
                ChangeRadiusStep = stepChangeRadius
            });
        }

        public void CreateMissileBonus(DPoint location, DPoint speed, DPoint dest)
        {
            arena.CreateObject(new MissileBonus(dest)
            {
                AppearenceTick = arena.Tick,
                //           Direction = Angle360.Zero,
                Speed = speed.Clone(),
                Location = location.Clone(),
                startLocation = location.Clone(),
                Radius = arena.Options.MissileBonus_Radius
            });
        }

        public void CreateGravimineBonus(DPoint location, DPoint speed, DPoint dest)
        {
            arena.CreateObject(new GravimineBonus(dest)
            {
                AppearenceTick = arena.Tick,
                //           Direction = Angle360.Zero,
                Speed = speed.Clone(),
                Location = location.Clone(),
                startLocation = location.Clone(),
                Radius = arena.Options.GravimineBonus_Radius
            });
        }
        public void CreateGravishieldBonus(DPoint location, DPoint speed, DPoint dest)
        {
            arena.CreateObject(new GravishieldBonus(arena.Options.GravishieldBonus_InitStrength, dest)
            {
                AppearenceTick = arena.Tick,
                //           Direction = Angle360.Zero,
                Speed = speed.Clone(),
                Location = location.Clone(),
                startLocation = location.Clone(),
                Radius = arena.Options.GravishieldBonus_Radius
            });
        }


        public void CreateDashBonus(DPoint location, DPoint speed, DPoint dest)
        {
            arena.CreateObject(new DashBonus(dest)
            {
                AppearenceTick = arena.Tick,
                //           Direction = Angle360.Zero,
                Speed = speed.Clone(),
                Location = location.Clone(),
                startLocation = location.Clone(),
                Radius = arena.Options.DashBonus_Radius
            });
        }


        public void CreateBoosterBonus(DPoint location, DPoint speed, DPoint dest)
        {
            arena.CreateObject(new BoosterBonus(dest)
            {
                AppearenceTick = arena.Tick,
                //           Direction = Angle360.Zero,
                Speed = speed.Clone(),
                Location = location.Clone(),
                startLocation = location.Clone(),
                Radius = arena.Options.BoosterBonus_Radius
            });
        }

        

        public void CreateFireBoosterBonus(DPoint location, DPoint speed, DPoint dest)
        {
            arena.CreateObject(new FireboosterBonus(dest)
            {
                AppearenceTick = arena.Tick,
                //           Direction = Angle360.Zero,
                Speed = speed.Clone(),
                Location = location.Clone(),
                startLocation = location.Clone(),
                Radius = arena.Options.FireboosterBonus_Radius
            });
        }




        public void CooldownToInit(Player owner)
        {
            owner.PlasmaCooldown = arena.Options.Spaceship_PlasmaInitCooldown;
            owner.GravimineCooldown = arena.Options.Spaceship_GravimineInitCooldown;
            owner.MissileCooldown = arena.Options.Spaceship_MissileInitCooldown;
        }
        public void CreatePlasma(Player owner)
        {
            CooldownToInit(owner);
            arena.CreateObject(new Plasma(arena.Options.PlasmaBullet_EnergyPower, arena.Options.PlasmaBullet_Price)
            {
                AppearenceTick = arena.Tick,
                Owner = owner.PlayerID,
                //        Direction = Angle360.Zero,
                Radius = arena.Options.PlasmaBullet_Radius,
                Location = owner.Location.Add(owner.Direction.Vector.Multiply(owner.Radius)),
                Speed = owner.Direction.Vector.Multiply(arena.Options.PlasmaBullet_Speed)
            });
        }

        public void CreateGravimine(Player owner)
        {
            CooldownToInit(owner);
            arena.CreateObject(new Gravimine(arena.Options.GravimineBullet_EnergyPower, arena.Options.GravimineSplashRadius, arena.Options.GravimineBullet_Price)
            {
                AppearenceTick = arena.Tick,
                Radius = arena.Options.GravimineBullet_Radius,
                Speed = owner.Direction.Vector.Multiply(arena.Options.GravimineBullet_StartSpeed),
                Location = owner.Location.Add(owner.Direction.Vector.Multiply(owner.Radius)),
                ResistanceCoefficient = arena.Options.GravimineBullet_Resistance,
                Owner = owner.PlayerID,
                //         Direction = Angle360.Zero
            });
        }

        public void CreateMissile(Player owner, int targetId)
        {
            CooldownToInit(owner);
            arena.CreateObject(new Missile(arena.Options.MissileBullet_EnergyPower, targetId, arena.Options.MissileBullet_Price)
            {
                AppearenceTick = arena.Tick,
                Radius = arena.Options.MissileBullet_Radius,
                Speed = owner.Direction.Vector.Multiply(arena.Options.MissileBullet_StartSpeed),
                Location = owner.Location.Add(owner.Direction.Vector.Multiply(owner.Radius)),
                ResistanceCoefficient = arena.Options.MissileBullet_Resistance,
                Owner = owner.PlayerID,
                TargetId = targetId,
                Direction = new Angle360(owner.Direction.Radians)
            });
        }

        /* Функционал в PlayerDriver
        public void ShieldActive(Player player)
        {
            
        }*/


    }
}
