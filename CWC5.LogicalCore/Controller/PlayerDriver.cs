﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model;

namespace CWC5.LogicalCore.Controller
{
    internal class PlayerDriver : ControllerBase
    {
        public PlayerDriver(Arena arena)
            : base(arena)
        { }

        public void Die(Player player)
        {
            player.LastAcceleration = DPoint.Zero;
            player.IsAlive = false;
        }

        public void SetAcceleration(Player player, DPoint acceleration)
        {
            DPoint realAcceleration = acceleration.Clone();
            Double maxAcceleration;
            if (player.BoosterRemainingTime > 0)
            {
                maxAcceleration = arena.Options.Spaceship_MaxAcceleration * arena.Options.BoosterBonus_Multiplier;
            //    realAcceleration = realAcceleration.Multiply(Arena.Options.FireBoosterBonus_RiseAcceleration);
            }
            else
                maxAcceleration = arena.Options.Spaceship_MaxAcceleration;
            if (acceleration.GetLength() > maxAcceleration)
            {
                realAcceleration = realAcceleration.Normalize().Multiply(maxAcceleration);
            }

            player.LastAcceleration = realAcceleration;
        }

        public void Accelerate(Player player)
        {
            var acc = player.LastAcceleration;
            player.LastAcceleration = null;

            if (acc == null) return;
            player.Speed = player.Speed.Add(acc);
            //player.Points -= player.GetPointsForAcceleration(acc.GetLength());
            //new Sky(Arena).ReleasePoints(player.GetPointsForAcceleration(acc.GetLength()));
            // return acc;
        }

        public void ChangeDirection(Player player, Angle360 newDirection)
        {
            Angle nd = newDirection.Radians > player.Direction.Radians ?
                newDirection
                : newDirection.GetAngle().Add(new Angle(2 * Math.PI));

            Angle ccw = nd.Add(player.Direction.GetAngle().Invert());
            Angle cw = player.Direction.GetAngle().Add(new Angle(Angle.TwoPi)).Add(nd.Invert());

            Angle ang = ccw.Radians < cw.Radians ? ccw : cw.Invert();

            if (ang.Abs().Radians > arena.Options.Spaceship_MaxTurnSpeed.Abs().Radians)
            {
                player.Direction = player.Direction.Add(
                    new Angle(ang.Radians / Math.Abs(ang.Radians)
                        * arena.Options.Spaceship_MaxTurnSpeed.Abs().Radians));
            }
            else
            {
                player.Direction = player.Direction.Add(ang);
            }
        }




        public Boolean FireBullet(Player player, Type bulletType, int targetId = -1)
        {
            if (bulletType == null)
                return false;

            var creator = new ObjectCreator(arena);
            if (bulletType == typeof(Plasma))
            {
                if (player.PlasmaCooldown == 0)
                {
                    creator.CreatePlasma(player);
                    // /*old
                    // if (player.FireBoosterRemainingTime > 0)
                    // {
                    //     player.DropPlasma(Arena.Options.FireBoosterBonus_NewCooldown);
                    // }
                    // else
                    // { */
                    //     player.DropPlasma(Arena.Options.Spaceship_PlasmaInitCooldown);
                    //// }
                    //new 15.04.2018
                     if (player.FireBoosterRemainingTime > 0)
                     {
                         player.DropPlasma(arena.Options.FireboosterBonus_NewCooldown);
                     }
                     else
                     {
                        player.DropPlasma(arena.Options.Spaceship_PlasmaInitCooldown);
                     }
                    return true;
                }
            }
            else if (bulletType == typeof(Gravimine))
            {
                if (player.GravimineCooldown == 0)
                {
                    if (player.GravimineCount > 0)
                    {
                        creator.CreateGravimine(player);
                        player.DropGravimine();
                        return true;
                    }
                }
            }
            else if (bulletType == typeof(Missile))
            {
                if (player.MissileCooldown == 0)
                {
                    if (player.MissileCount>0) 
                    {

                        var target = arena.Players.Any(p => p.PlayerID == targetId && p.PlayerID != player.PlayerID) ? targetId : -1;
                        creator.CreateMissile(player, target);
                        player.DropMissile();
                        return true;
                    }
                }
            }
            return false;
        }

        public Boolean ActivateShield(Player player)
        {
            if ((player.GravishieldCount > 0)&&(player.GravishieldRemainingtime==0))
            {
                player.GravishieldCount--;
                player.GravishieldRemainingtime = arena.Options.GravishieldBonus_Duration;
                player.GravishieldStrength = arena.Options.GravishieldBonus_InitStrength;
                return true;
            }
            return false;
        }
        public Boolean ActivateDash(Player player, Double direction)
        {
            if (player.DashCount > 0)
            {
                player.DashCount--;
                //player.Speed  //TODO Dash
                player.Speed = player.Speed.Add(new Angle360(direction).Vector.Add(arena.Options.DashBonus_Boost));
                return true;
            }
            return false;
        }

    }
}
