﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model;

namespace CWC5.LogicalCore.Controller
{
    [Obsolete("Pass Arena object as parameter instead", false)]
    internal abstract class ControllerBase
    {
        public ControllerBase(Arena _arena)
        {
            arena = _arena;
        }

        protected Arena arena
        {
            get;
            set;
        }


    }
}
