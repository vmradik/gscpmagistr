﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model;
using CWC5.LogicalCore.FightStory;

namespace CWC5.LogicalCore.Controller
{
    public class GameController
    {
        public GameController()
        {
        }

        public static Arena GetEmptyArena(GameOptions _options)
        {
            Arena arena = new Arena(_options);
            FightStarter.StartFight(arena);
            return arena;
        }

        public static Arena GetNextFrame(Arena arena, List<DTOModel.TurnResult?> bot_answers)
        {

            ///выполняем запросы тика tick
            for (int playerId = 0; playerId < arena.Options.PlayersCount; playerId++)
            {
                BotQueryHandler.ExecuteBotAnswer(arena, bot_answers[playerId], playerId);
            }

            ///вополняем переход к тику (tick + 1)
            arena = new Ticker().GoToNextTick(arena);
            //current_frame.Events = arena.events;
            ///сохраняем состояние тика tick

            if (arena.Tick == arena.Options.Arena_FightDuration - 1)
            {
                FightStarter.FinishFight(arena);
            }

            return arena;
        }

    }
}
