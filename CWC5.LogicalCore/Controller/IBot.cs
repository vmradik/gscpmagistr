﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore.Controller
{
    public interface IBot
    {
        Boolean Init(DTOModel.GameOptions options);
        DTOModel.TurnResult? GetTurnResult(DTOModel.Arena arena);
    }
}
