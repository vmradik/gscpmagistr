﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model;

namespace CWC5.LogicalCore.Controller
{
    public static class BotQueryHandler
    {
        /*
        public static readonly DTOModel.MoveResult DefaultMoveResult = new DTOModel.MoveResult()
        {
            Acceleration = new DTOModel.Point() { X = 0.0, Y = 0.0 },
            CannonDirection = Double.NaN,
            Bullet = DTOModel.BulletType.None,
            Messages = null
        };
        */
        public static DTOModel.Arena CreateBotQuery(Arena arena, Int32 playerID)
        { 
            return new DTO.DTOMapper().GetArena(arena, playerID);
        }

        public static void ExecuteBotAnswer(Arena arena, DTOModel.TurnResult? result, Int32 playerID)
        {
            Player player = arena.GetPlayer(playerID);
            PlayerDriver driver = new PlayerDriver(arena);

            if (!result.HasValue)
            {
                driver.Die(player);
                return;
            }

            var answer = result.Value;
            var bulletType = answer.Bullet == DTOModel.BulletType.Plasma
                    ? typeof(Plasma)
                    : answer.Bullet == DTOModel.BulletType.Gravimine
                        ? typeof(Gravimine)
                        : answer.Bullet == DTOModel.BulletType.Missile
                            ? typeof(Missile)
                            : null;
            /*if(bulletType == typeof(Gravimine))
            {
                var asdasd = 0;
            }*/
            if (bulletType == typeof(Missile))
            {
                if (driver.FireBullet(player, bulletType, answer.TargetID))
                    answer.Bullet = DTOModel.BulletType.None;
            } else
            {
                if (driver.FireBullet(player, bulletType))
                    answer.Bullet = DTOModel.BulletType.None;
            }

            
            if (answer.ActivateShield)
            {
                driver.ActivateShield(player);
            }
            if (answer.ActivateDash)
            {
                driver.ActivateDash(player, answer.DashDirection);
            }
            if (!Double.IsNaN(answer.Acceleration.X) && !Double.IsNaN(answer.Acceleration.Y) &&
                !Double.IsInfinity(answer.Acceleration.X) && !Double.IsInfinity(answer.Acceleration.Y)) 
                driver.SetAcceleration(player, new DPoint(answer.Acceleration.X, answer.Acceleration.Y));
            if (!Double.IsNaN(answer.CannonDirection) && !Double.IsInfinity(answer.CannonDirection))
                driver.ChangeDirection(player, new Angle360(answer.CannonDirection));
        }
    }
}
