﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model;
using CWC5.LogicalCore.FightStory;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Controller
{

    internal class Ticker
    {
        public static double Distance(DPoint o1, DPoint o2)
        {
            return Distance(o1.X, o1.Y, o2.X, o2.Y);
        }

        public static double Distance(double beginX, double beginY, double endX, double endY)
        {
            return Math.Sqrt(Math.Pow((endX - beginX), 2) + Math.Pow((endY - beginY), 2));
        }

        private static Int32 DecrementWithZero(Int32 count)
        {
            return Math.Max(0, count - 1);
        }


        public double yOnStraight(double x, double x1, double x2, double y1, double y2)
        {
            return (x * (y2 - y1) + x2 * y1 - x1 * y2)/(x2-x1);
           // return (y2 - y1) * (x - x1) / (x2 - x1) + y1;
        }
        public DPoint moveBonusToPoint(Arena arena, Bonus item,DPoint destination) {
            
            return item.Location = new DPoint(item.Location.X+((item.startLocation.X>=arena.Size.X/2|| item.startLocation.Y >= arena.Size.Y / 2) ?-0.5:0.5),
                                              yOnStraight(item.Location.X+1, item.Location.X, destination.X, item.Location.Y, destination.Y)
                                        );
        }
        public void controlDeleteBonus(Arena arena, PhysicalObject item)
        {
            if (item.Location.X   <= item.Radius || item.Location.X >= arena.Size.X - item.Radius || item.Location.Y <= item.Radius || item.Location.Y >= arena.Size.Y - item.Radius)
                arena.RemoveObject(item);
        }

        DPoint GetMissileSpeedVector(Arena arena, Missile b, DPoint e = null)
        {
            DPoint eIfTargetIsNull = b.Speed.Add(b.Speed.Multiply(-arena.Options.GravimineBullet_Resistance));
            DPoint rez;
            if (e != null)
            {
                rez = b.Speed.Add(e.Substract(b.Location)).Normalize()
                    .Multiply(
                    b.Speed.GetLength());
                rez = rez.Add(rez.Multiply(-arena.Options.MissileBullet_Resistance));
            }
            else rez = eIfTargetIsNull;
            return rez;
        }

        DPoint GetMissileEnemy(Arena arena, Missile b)
        {

            DPoint e = null;
            if (b.TargetId == -1)
            {
                return arena.Players.OrderBy(p => Distance(p.Location, b.Location)).FirstOrDefault(p => p.PlayerID != b.Owner)?.Location ?? b.Location.Add(b.Speed);
            }
            else { e = arena.Players.FirstOrDefault(p => p.PlayerID == b.TargetId).Location; }
            return e;
        }

        

		public Arena GoToNextTick(Arena arena)
		{
            arena.events = new List<FightEvent>();

            MoveObjects(arena);

			new Sky(arena).DropBonuses();
            arena.events.AddRange(arena.GetLastTickObjects<Bonus>()
				.Select(b => new FightEvent()
				{
					Type = FightEventType.BonusAppearance,
					Location = b.Location.Clone()
				}));
            arena.events.AddRange(arena.GetLastTickObjects<Bullet>()
				.Select(b => new FightEvent()
				{
					Type = b is Plasma 
                            ? FightEventType.FirePlasma
						    : b is Gravimine
                                ? FightEventType.FireGravimine
						        : b is Missile
                                    ? FightEventType.FireMissile
                                    : 0,
					Location = b.Location
				}));
			///* флаг банки 
			foreach (var item in arena.Players)
			{
				item.InZone = false;
			}
			//*/

			DoBonusCollisions(arena);
			DoBulletCollisions(arena);
			DoArenaCollisions(arena);
			DoPlayerCollisions(arena);

			foreach (var item in arena.Players)
			{
                item.PlasmaCooldown = DecrementWithZero(item.PlasmaCooldown);
                item.GravimineCooldown = DecrementWithZero(item.GravimineCooldown);
                item.MissileCooldown = DecrementWithZero(item.MissileCooldown);
                item.GravishieldRemainingtime = DecrementWithZero(item.GravishieldRemainingtime);
                item.BoosterRemainingTime = DecrementWithZero(item.BoosterRemainingTime);
                item.FireBoosterRemainingTime = DecrementWithZero(item.FireBoosterRemainingTime);
                if (!item.IsGravishieldActive)
				{
					item.IsGravishieldActive = false;
				}                
			}




			arena.Tick++;
			return arena;
		}
		private double inZoneResistPlayerModifier(Arena arena, Player obj)
		{
			return obj.InZone ? arena.Options.EnergySpill_SpaceshipResistance : obj.ResistanceCoefficient;
		}

		private void MoveObjects(Arena arena)
		{
			foreach (var item in arena.PhysicalObjects)
			{
                if (item is Gravimine && item.Speed.GetLength() < arena.Options.GravimineBullet_MinSpeed && item.Speed != DPoint.Zero)
                {
                    item.Speed = DPoint.Zero;
                }
                else if (item is Missile && item.Speed.GetLength() < arena.Options.MissileBullet_MinSpeed)
                {
                    ExplodeBulletEvent(arena, (Bullet)item);
                    arena.RemoveObject(item);
                }
                else
                {
                    if (item is Player)
                    {
                        item.Speed = item.Speed.Add(item.Speed.Multiply(-inZoneResistPlayerModifier(arena, (Player)item)));

                        new PlayerDriver(arena).Accelerate((Player)item);
                    }
                    else if (item is Gravimine)
                    {
                        item.Speed = item.Speed.Add(item.Speed.Multiply(-arena.Options.GravimineBullet_Resistance));
                    }
                    else if (item is Missile)
                    {   //todo

                        var b = ((Missile)item);
                        //item.Speed = item.Speed.Add(item.Speed.Multiply(-Arena.Options.GravimineBullet_Resistance));
                        item.Speed = GetMissileSpeedVector(arena, b, GetMissileEnemy(arena, b));
                        b.Direction = new Angle360(b.Speed);
                    }
                    else if(item is EnergySpill)
                    {
                        EnergySpill pb = (EnergySpill)item;
                        --pb.RemainingTime;
                        --pb.ChangeDirectTick;
                        --pb.ChangeRadiusTick;


                        if (pb.RemainingTime == 0)
                        {
                            arena.RemoveObject(item);
                            arena.Options.zoneCreate = false;
                            arena.Options.EnergySpillPauseRemainingTime = MagicRandom.Next(arena.Options.EnergySpill_PauseMin, arena.Options.EnergySpill_PauseMax);
                        }
                        else
                        {
                            
                            //if (pb.ChangeRadiusTick > 0)
                           // {
                                if (!(pb.Radius <= arena.Options.EnergySpill_RadiusMax
                                     && pb.Radius >= arena.Options.EnergySpill_RadiusMin))
                                {
                                 /*   pb.ChangeRadiusStep *= -1; ;
                                    pb.Radius += 2*pb.ChangeRadiusStep;
                                    */
                                }
                                else
                                    pb.Radius += -Math.Abs(pb.ChangeRadiusStep);

                           // }
                            /*
                            else
                            {
                                pb.ChangeRadiusStep = EnergySpill.changeRadiusStep(Arena.Options);
                                pb.ChangeRadiusTick = EnergySpill.changeRadiusTick(Arena.Options);
                            }
                            
                            */
                            if (pb.ChangeDirectTick <= 0)
                            {
                                pb.ChangeDirectTick = MagicRandom.Next((int)arena.Options.EnergySpill_ChangeDirectMin, (int)arena.Options.EnergySpill_ChangeDirectMax);
                                pb.startLocation = pb.Location;
                                pb.destination = Bonus.genericDest(arena.Size,pb.Location);
                                pb.Speed = Bonus.moveBonusToPoint(pb.destination, pb.Location, arena.Size, arena.Tick);
                            }
                            
                        }
                    }


                    item.Location = item.Location.Add(item.Speed);
                }
			}
		}

		#region Do collisions

		private Boolean HasCollision(PhysicalObject o1, PhysicalObject o2)
		{
			return CollisionDegree(o1, o2) >= 0;
		}

		private Boolean ZonePlayerHasCollision(PhysicalObject o1, PhysicalObject o2)
		{
			return ZonePlayerCollisionDegree(o1, o2) >= 0;
		}

		private Double CollisionDegree(PhysicalObject o1, PhysicalObject o2)
		{
			return o1.Radius + o2.Radius - o1.Location.Substract(o2.Location).GetLength();
		}
		
		private Double ZonePlayerCollisionDegree(PhysicalObject o1, PhysicalObject o2)
		{
			
			return Math.Abs(o1.Radius - o2.Radius) - o1.Location.Substract(o2.Location).GetLength();
		}
		

		private void DoBonusCollisions(Arena arena)
		{
			Dictionary<Bonus, Player> bp = new Dictionary<Bonus, Player>();
			Dictionary<Player, Bonus> bpEnergySpill = new Dictionary<Player, Bonus>();
			var inZoneCount = 0;
			// double EnergySpillPoints=0.0;
			foreach (var bonus in arena.Bonuses)
			{
				if (bonus is EnergySpill)
				{
					foreach (var player in arena.Players)
					{
						if (ZonePlayerHasCollision(bonus, player))
						{
							// player.Points += ((EnergySpill)bonus).Points;
							// EnergySpillPoints = ((EnergySpill)bonus).Points; // писец на костылях
							player.InZone = true;
							inZoneCount++;
							bpEnergySpill.Add(player, bonus);
						}
					}
				}
				

			 // КТО ЭТО СДЕЛАЛ И ЗАЧЕМ ТАК???
				else
				{
					var player = arena.Players.First(p => !(bonus is EnergySpill) && CollisionDegree(p, bonus) == arena.Players.Max(pp => CollisionDegree(pp, bonus)));
					if (HasCollision(bonus, player))
					{
						bp.Add(bonus, player);
					}
				}
			}

			//EnergySpillPoints = (inZoneCount != 0) ? Arena.Options.EnergySpill_Power / inZoneCount : EnergySpillPoints;
			double EnergySpillPoints = (inZoneCount > 0) ? arena.Options.EnergySpill_Power / inZoneCount : arena.Options.EnergySpill_Power;
            
            foreach (var item in bpEnergySpill)
			{
                if (item.Key.InZone) item.Key.Points += EnergySpillPoints;
            }


			foreach (var item in bp)
			{
				GiveBonus(arena, item.Value, item.Key);
			}
			// ХЗ ЗАЧЕМ
			// вставка шлака, не показывать - малые косяки порождают большие
		}

		private void GiveBonus(Arena arena, Player player, Bonus bonus)
		{
			Boolean picked = true;
            if (player.BonusCount < arena.Options.Spaceship_CargoHold)
            {
                if (bonus is GravimineBonus)
                {

                    if (player.GravimineCount < arena.Options.Spaceship_GravimineCapacity)
                    {
                        player.GravimineCount++;
                    }
                    else picked = false;

                    // player.GravimineCount++;
                }
                else if (bonus is MissileBonus)
                {

                    if (player.MissileCount < arena.Options.Spaceship_MissileCapacity)
                    {
                        player.MissileCount++;
                    }
                    else picked = false;

                    // player.GravimineCount++;
                }
                else if (bonus is GravishieldBonus)
                {
                    if (player.GravishieldCount < arena.Options.Spaceship_GravishieldCapacity)
                    {
                        ++player.GravishieldCount;
                    }
                    else picked = false;
                }

                else if (bonus is DashBonus)
                {
                    if (player.DashCount < arena.Options.Spaceship_DashCapacity)
                    {
                        ++player.DashCount;
                    }
                    else picked = false;
                }

                else if (bonus is BoosterBonus)
                {
                    /* now this is speed booster */


                    player.BoosterRemainingTime = arena.Options.BoosterBonus_Duration;
                    /*OLD
                    player.FireBoosterRemainingTime = Arena.Options.FireBoosterBonus_Duration;
                    if (player.PlasmaCooldown > Arena.Options.FireBoosterBonus_NewCooldown)
                    {
                        player.PlasmaCooldown = Arena.Options.FireBoosterBonus_NewCooldown;
                    }
                    */

                }

                else if (bonus is FireboosterBonus)
                {
                    player.FireBoosterRemainingTime = arena.Options.FireboosterBonus_Duration;
                    if (player.PlasmaCooldown > arena.Options.FireboosterBonus_NewCooldown)
                    {
                        player.PlasmaCooldown = arena.Options.FireboosterBonus_NewCooldown;
                    }

                }
                else picked = false; // thomething GiveBonus Error
                if (picked)
                {
                    arena.events.Add(new FightEvent() { Type = FightEventType.BonusCaught, Location = bonus.Location.Clone() });
                    arena.RemoveObject(bonus);
                }
            }
		}










		private bool BulletVsBulletCollisions(Arena arena, Bullet a, Bullet b)
		{   //todo nomdusk remove arena object removal to caller
			if (a != b && HasCollision(a, b))
			{
                if ((a is Plasma) && (b is Missile))
                {
                    arena.RemoveObject(a);
                    arena.RemoveObject(b);
                    return false;
                }
                else if ((a is Missile) && (b is Plasma))
                {
                    arena.RemoveObject(a);
                    arena.RemoveObject(b);
                    return false;
                }

                else if ((b is Missile) && (a is Missile))
                {
                    var aTemp = a;
                    KnockObj(arena, a, b);
                    KnockObj(arena, b, aTemp);
                    aTemp = null;
                    return false;
                }
                else if ((a is Gravimine) && (b is Missile))
                {
                    arena.RemoveObject(a);
                    arena.RemoveObject(b);
                    return false;
                }
                else if ((a is Missile) && (b is Gravimine))
                {
                    arena.RemoveObject(a);
                    arena.RemoveObject(b);
                    return false;
                }

                else if ((b is Missile) && (a is Missile))
                {
                    var aTemp = a;
                    KnockObj(arena, a, b);
                    KnockObj(arena, b, aTemp);
                    aTemp = null;
                    return false;
                }
                else if ((a is Plasma) && (b is Gravimine))
				{
					arena.RemoveObject(a);
					arena.RemoveObject(b);
					return false;
				}
				else if ((a is Gravimine) && (b is Plasma))
				{
					arena.RemoveObject(a);
					arena.RemoveObject(b);
					return false;
				}

				else if ((b is Gravimine) && (a is Gravimine))
				{
					var aTemp = a;
					KnockObj(arena, a, b);
					KnockObj(arena, b, aTemp);
					aTemp=null;
					return false;
				}
				else return true;
			}
			return false;
		}

		public struct bulletAndPlayer
		{
			public Bullet bullet;
			public PhysicalObject obj;
			public Double CollisionDegree;
			public bulletAndPlayer(Bullet b, PhysicalObject p,Double CD)
			{
				bullet = b;
				obj = p;
				CollisionDegree = CD;
			}
		}
		private void DoBulletCollisions(Arena arena)
		{
			Dictionary<Bullet, bulletAndPlayer> bulletsToExplode = new Dictionary<Bullet, bulletAndPlayer>();
			/*
			List<Bullet> bulletsCollisions = Arena.Bullets
				.Where(b => Arena.Players.Any(p => p.PlayerID != b.Owner && HasCollision(b, p))
				 //        || Arena.Bullets.Any(bb => BulletVsBulletCollisions(b, bb))
				||  Arena.Bullets.Any(bb => b != bb && HasCollision(b, bb))
				)
				.ToList();*/
			// foreach (var item in Arena.PhysicalObjects)
			foreach (var b in arena.Bullets)
			{
				foreach(var bb in arena.Bullets)
				{
					if ((b!=bb)&&(CollisionDegree(b, bb) >= 0)&& ((!bulletsToExplode.ContainsKey((Bullet)b)) || (CollisionDegree(b, bb) > bulletsToExplode[(Bullet)b].CollisionDegree)))
					{
							
							bulletsToExplode[(Bullet)b] = new bulletAndPlayer(b,bb,CollisionDegree(b, bb));
					}
				}
				foreach (var bb in arena.Players)
				{
					if ((b.Owner != bb.PlayerID) &&(CollisionDegree(b, bb) >= 0)&& ((!bulletsToExplode.ContainsKey((Bullet)b)) || (CollisionDegree(b, bb) > bulletsToExplode[(Bullet)b].CollisionDegree)))
					{
						bulletsToExplode[(Bullet)b] = new bulletAndPlayer(b,bb, CollisionDegree(b, bb));
					}
				}
			}
			foreach(var item in bulletsToExplode)
			{
				ExplodeBullet(arena, item.Value);
			}

		}

		private void DoArenaCollisions(Arena arena)
		{
			foreach (var item in arena.PhysicalObjects)
			{
				Double x = item.Location.X;
				Double y = item.Location.Y;
				Double r = item.Radius;
				if (item is Plasma)
				{
					if (x + r <= 0 || x - r >= arena.Size.X
					 || y + r <= 0 || y - r >= arena.Size.Y)
						KnockWithArena(arena, item);
				}
                else if(item is Bonus )
                {
                    if(item is EnergySpill)
                    {
                      //  if (x - 2*r <= 0 || x + 2*r >= Arena.Size.X
                       //     || y - 2*r <= 0 || y + 2*r >= Arena.Size.Y)
                            KnockWithArena(arena, item, arena.Options.EnergySpill_RadiusMax*1.5, 0);
                    }
                    else if (x + r <= 0 || x - r >= arena.Size.X
                     || y + r <= 0 || y - r >= arena.Size.Y)
                        arena.RemoveObject(item);
                }
				else
				{
					if (x - r <= 0 || x + r >= arena.Size.X
					 || y - r <= 0 || y + r >= arena.Size.Y)
						KnockWithArena(arena, item);
				}

			}
		}

		private void DoPlayerCollisions(Arena arena)
		{
			List<Player> playersCopies = arena.Players.Select(p => (Player)p.Clone()).ToList();
			foreach (var pc in playersCopies)
			{
				foreach (var p in arena.Players)
				{
					if (pc.PlayerID == p.PlayerID || !HasCollision(pc, p))
						continue;

					KnockPlayers(arena, pc, p);
				}
			}
		}

		#endregion


		private void KnockPlayers(Arena arena, Player pc, Player p)
		{
			DPoint radiusVector = p.Location.Substract(pc.Location);
			DPoint add = radiusVector.Normalize().Multiply(Math.Pow(CollisionDegree(pc, p), 1.0 / 2.0) * arena.Options.DeformationCoefficient);
			p.Speed = p.Speed.Add(add);
		}



        private void KnockWithArena(Arena arena, PhysicalObject obj, Double border, Double add)
        {
            Double left = obj.Location.X - obj.Radius;
            Double right = obj.Location.X + obj.Radius;
            Double down = obj.Location.Y - obj.Radius;
            Double up = obj.Location.Y + obj.Radius;

            if (obj is Plasma)
            {
                arena.RemoveObject(obj);
                return;
            }
            bool collision=false;
            if (left <= border)
            {
                obj.Speed = new DPoint(Math.Abs(obj.Speed.X), obj.Speed.Y);
                obj.Location = new DPoint(obj.Radius+border+add, obj.Location.Y);
                collision = true;
            }
            else if (right >= arena.Size.X-border)
            {
                obj.Speed = new DPoint(-Math.Abs(obj.Speed.X), obj.Speed.Y);
                obj.Location = new DPoint(arena.Size.X - obj.Radius-border-add, obj.Location.Y);
                collision = true;
            }
            if (up >= arena.Size.Y-border)
            {
                obj.Speed = new DPoint(obj.Speed.X, -obj.Speed.Y);
                obj.Location = new DPoint(obj.Location.X, arena.Size.Y - obj.Radius - border-add);
                collision = true;
            }
            else if (down <= border)
            {
                obj.Speed = new DPoint(obj.Speed.X, Math.Abs(obj.Speed.Y));
                obj.Location = new DPoint(obj.Location.X, obj.Radius + border+add);
                collision = true;
            }
            if (obj is EnergySpill && collision)
                ((EnergySpill)obj).ChangeDirectTick += arena.Options.EnergySpill_ChangeDirectMin;
  
        }


        private void KnockWithArena(Arena arena, PhysicalObject obj)
        {
            KnockWithArena(arena, obj, 0, 0);
        }



        private void KnockObj(Arena arena, Bullet pc, Bullet p)
		{
			DPoint radiusVector = p.Location.Substract(pc.Location);
			DPoint add = radiusVector.Normalize().Multiply(Math.Pow(CollisionDegree(pc, p), 1.0 / 2.0) * arena.Options.DeformationCoefficient);
			p.Speed = p.Speed.Add(add);
		}



		private void ExplodeBullet(Arena arena, bulletAndPlayer bap)
		{
			Player owner = arena.GetPlayer(bap.bullet.Owner);
            arena.events.Add(new FightEvent()
			{
				Type = bap.bullet is Plasma ? FightEventType.PlasmaExploded
					  : bap.bullet is Gravimine ? FightEventType.GravimineExploded
					  : bap.bullet is Missile ? FightEventType.MissileExploded
                      : 0,
				Location = bap.bullet.Location
			});

			arena.RemoveObject(bap.bullet);
			if (!(bap.obj is Player)) return;
			var player = (Player)bap.obj;


			if (player.GravishieldStrength > 0)
			{
				--player.GravishieldStrength;
				return;
			}

			if (bap.bullet is Plasma)
			{
				if (!player.InZone)
				{                   
					Double points = Math.Min(player.Points, ((Plasma)bap.bullet).Power);
					player.Points -= points;
                    (arena.Players.Where(p => p.PlayerID == bap.bullet.Owner).FirstOrDefault()).Points += ((Plasma)bap.bullet).Price;

                }
			}
			else if (bap.bullet is Gravimine)
			{
				if (!player.InZone)
				{
					Double points = Math.Min(player.Points, ((Gravimine)bap.bullet).Power);
					player.Points -= points;

                    (arena.Players.Where(p => p.PlayerID == bap.bullet.Owner).FirstOrDefault()).Points += ((Gravimine)bap.bullet).Price;
                }
				
				player.Speed = player.Speed.Add(player.Location.Substract(bap.bullet.Location)
					.Normalize()
					.Multiply(arena.Options.GravimineBullet_PushPower));
			}
            else if (bap.bullet is Missile)
            {   //todo
                if (!player.InZone)
                {
                    Double points = Math.Min(player.Points, ((Missile)bap.bullet).Power);
                    player.Points -= points;

                    (arena.Players.Where(p => p.PlayerID == bap.bullet.Owner).FirstOrDefault()).Points += ((Missile)bap.bullet).Price;
                }

                player.Speed = new DPoint(0, 0);

                //player.Speed = player.Speed.Add(player.Location.Substract(bap.bullet.Location)
                //    .Normalize()
                //    .Multiply(Arena.Options.MissileBullet_PushPower));
            }
        }

		private void ExplodeBulletEvent(Arena arena, Bullet bullet)
		{
            arena.events.Add(new FightEvent()
			{
				Type = bullet is Plasma ? FightEventType.PlasmaExploded
		                : bullet is Gravimine ? FightEventType.GravimineExploded
                        : bullet is Missile ? FightEventType.MissileExploded
                        : 0,
                Location = bullet.Location
			});
		}

	}
}
