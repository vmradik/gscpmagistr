﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model;

namespace CWC5.LogicalCore.Controller
{
    internal static class FightStarter
    {

        public static void StartFight(Arena arena)
        {
            ObjectCreator creator = new ObjectCreator(arena);
            arena.Options.EnergySpillPauseRemainingTime = MagicRandom.Next(arena.Options.EnergySpill_PauseMin, arena.Options.EnergySpill_PauseMax);
            for (int i = 0; i < arena.Options.PlayersCount; i++)
            {
                creator.CreatePlayer(i);
            }

            for (int i = 0; i < arena.Options.CloudCount; i++)
            {
                creator.CreateCloud();
            }
            arena.Tick = 0;
        }

        public static void FinishFight(Arena arena)
        {
            Double free = arena.Clouds.Sum(c => c.Points) + arena.Bonuses.OfType<EnergySpill>().Sum(b => b.Points);
            free /= arena.Players.Count();

            foreach (var item in arena.Players)
            {
                item.Points += free;
            }
        }

    }
}
