﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model;
using CWC5.LogicalCore.Model.Base;
using System.Collections;
namespace CWC5.LogicalCore.Controller
{
    internal class Sky : ControllerBase
    {
        public Sky(Arena arena)
            : base(arena)
        {

        }

        public void ReleasePoints(Double points)
        {
            arena.Clouds[MagicRandom.Next(0, arena.Clouds.Length)].Points += points;
        }


        public void DropBonuses()
        {
            List<Cloud> toPointsDrop = new List<Cloud>();
            List<Cloud> toGravimineDrop = new List<Cloud>();

            List<Cloud> toGravishieldDrop = new List<Cloud>();
            List<Cloud> toDashDrop = new List<Cloud>();

            List<Cloud> toBoosterDrop = new List<Cloud>();
            List<Cloud> toFireBoosterDrop = new List<Cloud>();
            List<Cloud> toMissileDrop = new List<Cloud>();

            foreach (var item in arena.Clouds)
            {
                if (item.CanGetEnergySpill(arena.Options.EnergySpillPauseRemainingTime))
                {
                    toPointsDrop.Add(item);
                }
                else arena.Options.EnergySpillPauseRemainingTime--;
                if (!canSpawnMoreBonuses())
                {
                    continue;
                }
                if (item.CanGetGravimineBonus())
                {
                    toGravimineDrop.Add(item);
                }


                if (item.CanGetGravishieldBonus())
                {
                    toGravishieldDrop.Add(item);
                }

                if (item.CanGetDashBonus())
                {
                    toDashDrop.Add(item);
                }



                if (item.CanGetBoosterBonus())
                {
                    toBoosterDrop.Add(item);
                }

                if (item.CanGetFireBoosterBonus())
                {
                    toFireBoosterDrop.Add(item);
                }
                if (item.CanGetMissileBonus())
                {
                    toMissileDrop.Add(item);
                }
            }

            foreach (var item in toPointsDrop)
            {

                DropEnergySpill(item);
            }
            foreach (var item in toGravimineDrop)
            {
                DropGravimineBonus(item);
            }


            foreach (var item in toGravishieldDrop)
            {
                DropGravishieldBonus(item);
                // DropGravimineBonus(item);
            }

            foreach (var item in toDashDrop)
            {
                DropDashBonus(item);
            }



            foreach (var item in toBoosterDrop)
            {
                DropBoosterBonus(item);
            }
            foreach (var item in toFireBoosterDrop)
            {
                DropFireBoosterBonus(item);
            }
            foreach (var item in toMissileDrop)
            {
                DropMissileBonus(item);
            }
        }
        private double ScaleCoefficient = 1.5; // SpriteBase.ScaleCoefficient

        private bool canSpawnMoreBonuses()
        {
            int count = arena.BonusCount;
            int limit = arena.Options.BonusesOnScreenLimit;
            /*if (count == 0) { return true; }
            int diff = limit - count;
            if (diff <= 0) { diff = 1; }
            double prob = (double)diff / (double)limit;
            if (Arena.SpillCount != 0)
            {
                prob *= 3;
            }
            return (MagicRandom.NextDouble() < prob);
            */
            return count < limit;
        }

        private double pricePathToEnergySpeel(DPoint zoneLocation)
        {
            var x1 = zoneLocation.X; var y1 = zoneLocation.Y;

            double[] arr = new double[arena.Players.Length];
            var i = 0;

            foreach (var player in arena.Players)
            {
                var x2 = player.Location.X; var y2 = player.Location.Y;
                arr[i] = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
                i++;
            }
            Array.Sort(arr);
            double sum = 0;
            for (var k = 0; k < i; k++)
            {
                for (int k2 = k + 1; k2 < i; k2++)
                {
                    arr[k2] -= arr[k];
                    sum += arr[k2];
                }
            }
            //Console.WriteLine(sum);
            return sum;
        }
        private DPoint optimalEnergySpeelLocation()
        {
            int iter = 1000;

            double bestIter = 0;
            double bestPrice = Double.MaxValue;
            double iterPrice;

            DPoint bestLocation = new DPoint(0, 0);
            for (int i = 0; i < iter; i++)
            {
                Double r = arena.Options.EnergySpill_RadiusMax * ScaleCoefficient;
                DPoint zoneLocation = new DPoint(               // location
                            r + MagicRandom.NextDouble() * (arena.Size.X - 2 * r),
                            r + MagicRandom.NextDouble() * (arena.Size.Y - 2 * r)
                        );
                iterPrice = pricePathToEnergySpeel(zoneLocation);
                if (bestPrice > iterPrice)
                {
                    bestIter = i;
                    bestLocation = zoneLocation;
                    bestPrice = iterPrice;
                }

            }

            return bestLocation;
        }


        private void DropEnergySpill(Cloud cloud)
        {
            if (!arena.Options.zoneCreate)
            {

                arena.Options.EnergySpillInitRemainingTime = MagicRandom.Next(arena.Options.EnergySpill_LifetimeMin, arena.Options.EnergySpill_LifetimeMax);

                arena.Options.zoneCreate = true;
                Double r = arena.Options.EnergySpill_RadiusMax * ScaleCoefficient;
                var creator = new ObjectCreator(arena);
                var optLocation = optimalEnergySpeelLocation();
                var dest = Bonus.genericDest(arena.Size, optLocation);
                var radius = MagicRandom.Next((int)arena.Options.EnergySpill_RadiusMin, (int)arena.Options.EnergySpill_RadiusMax);
                creator.CreateEnergySpill(
                    radius,
                    cloud.Points,			// points
                    optLocation,
                    //  Arena.Size.Y + r),
                    Bonus.moveBonusToPoint(dest, optLocation, arena.Size, arena.Tick),
                    // new DPoint(0.0, 0.0),    // speed Object
                    dest,
                    EnergySpill.changeRadiusStep(arena.Options),
                    EnergySpill.changeRadiusTick(arena.Options)
                );
                cloud.Points = 0.0;
            }
        }








        private void DropGravimineBonus(Cloud cloud)
        {
            Double r = arena.Options.GravimineBonus_Radius;
            var creator = new ObjectCreator(arena);

            var Location = new DPoint(
                    r + MagicRandom.NextDouble() * (arena.Size.X - 2 * r),
                    r + MagicRandom.NextDouble() * (arena.Size.Y - 2 * r));
            var dest = Bonus.genericDest(arena.Size, Location);
            creator.CreateGravimineBonus(
                    Location,
                    Bonus.moveBonusToPoint(dest, Location, arena.Size, arena.Tick),
                    dest
                    );

        }
        

        private void DropMissileBonus(Cloud cloud)
        {
            Double r = arena.Options.MissileBonus_Radius;
            var creator = new ObjectCreator(arena);

            var Location = new DPoint(
                    r + MagicRandom.NextDouble() * (arena.Size.X - 2 * r),
                    r + MagicRandom.NextDouble() * (arena.Size.Y - 2 * r));
            var dest = Bonus.genericDest(arena.Size, Location);
            creator.CreateMissileBonus(
                    Location,
                    Bonus.moveBonusToPoint(dest, Location, arena.Size, arena.Tick),
                    dest
                    );

        }

        private void DropFireBoosterBonus(Cloud cloud)
        {
            Double r = arena.Options.FireboosterBonus_Radius;
            var creator = new ObjectCreator(arena);
            var Location = new DPoint(
                    r + MagicRandom.NextDouble() * (arena.Size.X - 2 * r),
                    r + MagicRandom.NextDouble() * (arena.Size.Y - 2 * r));
            var dest = Bonus.genericDest(arena.Size, Location);
            creator.CreateFireBoosterBonus(
                    Location,
                    Bonus.moveBonusToPoint(dest, Location, arena.Size, arena.Tick),
                    dest
                    );
        }

        

        private void DropBoosterBonus(Cloud cloud)
        {
            Double r = arena.Options.BoosterBonus_Radius;
            var creator = new ObjectCreator(arena);
            var Location = new DPoint(
                    r + MagicRandom.NextDouble() * (arena.Size.X - 2 * r),
                    r + MagicRandom.NextDouble() * (arena.Size.Y - 2 * r));
            var dest = Bonus.genericDest(arena.Size, Location);
            creator.CreateBoosterBonus(
                    Location,
                    Bonus.moveBonusToPoint(dest, Location, arena.Size, arena.Tick),
                    dest
                    );
        }

        private void DropGravishieldBonus(Cloud cloud)
        {
            Double r = arena.Options.GravishieldBonus_Radius;
            var creator = new ObjectCreator(arena);
            var Location = new DPoint(
                    r + MagicRandom.NextDouble() * (arena.Size.X - 2 * r),
                    r + MagicRandom.NextDouble() * (arena.Size.Y - 2 * r));
            var dest = Bonus.genericDest(arena.Size, Location);
            creator.CreateGravishieldBonus(
                Location,
                    Bonus.moveBonusToPoint(dest, Location, arena.Size, arena.Tick),
                    dest
                );
        }

        private void DropDashBonus(Cloud cloud)
        {
            Double r = arena.Options.DashBonus_Radius;
            var creator = new ObjectCreator(arena);
            var Location = new DPoint(
                    r + MagicRandom.NextDouble() * (arena.Size.X - 2 * r),
                    r + MagicRandom.NextDouble() * (arena.Size.Y - 2 * r));
            var dest = Bonus.genericDest(arena.Size, Location);
            creator.CreateDashBonus(
                    Location,
                    Bonus.moveBonusToPoint(dest, Location, arena.Size, arena.Tick),
                    dest
                    );
        }


        /*


public void DropBonuses()
{
if (MagicRandom.NextDouble() <= Arena.Options.EnergySpillProbabilityConstant)
{
    DropEnergySpill();
}
if (MagicRandom.NextDouble() <= Arena.Options.FireBoosterBonusProbability)
{
    DropFireBoosterBonus(null);
}
if (MagicRandom.NextDouble() <= Arena.Options.GravimineBonusProbability)
{
    DropGravimineBonus(null);
}
if (MagicRandom.NextDouble() <= Arena.Options.GravishieldBonusProbability)
{
    DropGravishieldBonus(null);
}
}

private void DropEnergySpill()
{
Double r = Arena.Options.EnergySpillRadius;
var creator = new ObjectCreator(Arena);
creator.CreateEnergySpill(
    200,
    new DPoint(
        r + MagicRandom.NextDouble() * (Arena.Size.X - 2 * r),
        r + MagicRandom.NextDouble() * (Arena.Size.Y - 2 * r)),
        //  Arena.Size.Y + r),
        new DPoint(0.0, 0.0));
}


*/

    }
}
