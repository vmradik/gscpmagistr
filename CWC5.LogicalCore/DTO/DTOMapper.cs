﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.DTO
{
	public class DTOMapper
	{ 
		public CWC5.DTOModel.Arena GetArena(Arena arena, Int32 myID)
		{
            var GravimineBullets = arena.Bullets.OfType<Gravimine>().Select(b => GetGravimine(b)).ToArray();
            var MissileBullets = arena.Bullets.OfType<Missile>().Select(b => GetMissile(b)).ToArray();
            /*if(GravimineBullets.Length+ MissileBullets.Length > 0)
            {
                if (MissileBullets.Length>0)
                {
                    var a = 0;//todo
                }
            }*/
            return new DTOModel.Arena()
			{
				Tick = arena.Tick,
				EnergySpills = arena.Bonuses.OfType<EnergySpill>().Select(b => GetEnergySpill(b)).ToArray(),

				Me = GetPlayer(arena.Players.Single(p => p.PlayerID == myID)),
				Enemies = arena.Players.Where(p => p.PlayerID != myID).Select(p => GetPlayer(p)).ToArray(),

                MissileBonuses = arena.Bonuses.OfType<MissileBonus>().Select(b => GetMissileBonus(b)).ToArray(),
                GravimineBonuses = arena.Bonuses.OfType<GravimineBonus>().Select(b => GetGravimineBonus(b)).ToArray(),

                FireBoosterBonuses = arena.Bonuses.OfType<FireboosterBonus>().Select(b => GetFireBoosterCell(b)).ToArray(),
                BoosterBonuses = arena.Bonuses.OfType<BoosterBonus>().Select(b => GetBoosterCell(b)).ToArray(),

				GravishieldBonuses = arena.Bonuses.OfType<GravishieldBonus>().Select(b => GetGravishieldBox(b)).ToArray(),
                DashBonuses = arena.Bonuses.OfType<DashBonus>().Select(b => GetDashBox(b)).ToArray(),

                PlasmaBullets = arena.Bullets.OfType<Plasma>().Select(b => GetPlasma(b)).ToArray(),
				GravimineBullets = GravimineBullets,
                MissileBullets = MissileBullets,
            };
		}


		public CWC5.DTOModel.Spaceship GetPlayer(Player player)
		{
			return new DTOModel.Spaceship()
			{
				Body = GetPhisicalObject(player),
				PlasmaCooldown = player.PlasmaCooldown,
				Direction = player.Direction.Radians,
				Energy = player.Points,
				SpaceshipID = player.PlayerID,

                MissileCount = player.MissileCount,
                MissileCooldown = player.MissileCooldown,

                GravimineCount = player.GravimineCount,
                GravimineCooldown = player.GravimineCooldown,

                GravishieldCount = player.GravishieldCount,
				GravishieldStrength = player.GravishieldStrength,
                GravishieldRemainingTime = player.GravishieldRemainingtime,

                DashCount = player.DashCount,

                FireBoosterRemainingTime = player.FireBoosterRemainingTime,
                BoosterRemainingTime = player.BoosterRemainingTime,
				IsInEnergySpill = player.InZone,
			};
		}

		public CWC5.DTOModel.Point GetPoint(DPoint point)
		{
			return new DTOModel.Point()
			{
				X = point.X,
				Y = point.Y
			};
		}
		
		public CWC5.DTOModel.GameOptions GetOptions(GameOptions options)
		{
			return new DTOModel.GameOptions()
			{

				#region New opts
				Arena_Size = GetPoint(options.Arena_Size),
				Arena_FightDuration = options.Arena_FightDuration,

                EnergySpill_Power = options.EnergySpill_Power,
				EnergySpill_RadiusMin = options.EnergySpill_RadiusMin,
				EnergySpill_RadiusMax = options.EnergySpill_RadiusMax,
				EnergySpill_LifetimeMin = options.EnergySpill_LifetimeMin,
				EnergySpill_LifetimeMax = options.EnergySpill_LifetimeMax,
				EnergySpill_PauseMin = options.EnergySpill_PauseMin,
				EnergySpill_PauseMax = options.EnergySpill_PauseMax,
				EnergySpill_SpaceshipResistance = options.EnergySpill_SpaceshipResistance,
				
				Spaceship_Radius = options.Spaceship_Radius,
				Spaceship_InitEnergy = options.Spaceship_InitEnergy,
				Spaceship_MaxAcceleration = options.Spaceship_MaxAcceleration,
				Spaceship_MaxTurnSpeed = options.Spaceship_MaxTurnSpeed.Radians,
				Spaceship_Resistance = options.Spaceship_Resistance,
				Spaceship_PlasmaInitCooldown = options.Spaceship_PlasmaInitCooldown,

                Spaceship_CargoHold = options.Spaceship_CargoHold,
                Spaceship_GravimineInitCooldown = options.Spaceship_GravimineInitCooldown,
				Spaceship_GravimineCapacity = options.Spaceship_GravimineCapacity,
                
                Spaceship_MissileInitCooldown = options.Spaceship_MissileInitCooldown,
                Spaceship_MissileCapacity = options.Spaceship_MissileCapacity,

                Spaceship_GravishieldCapacity = options.Spaceship_GravishieldCapacity,
                Spaceship_DashCapacity = options.Spaceship_DashCapacity,

                PlasmaBullet_Radius = options.PlasmaBullet_Radius,
				PlasmaBullet_EnergyPower = options.PlasmaBullet_EnergyPower,
				PlasmaBullet_Speed = options.PlasmaBullet_Speed,
                PlasmaBullet_Reward = options.PlasmaBullet_Price,


                GravimineBullet_Radius = options.GravimineBullet_Radius,
				GravimineBullet_EnergyPower = options.GravimineBullet_EnergyPower,
				GravimineBullet_PushPower = options.GravimineBullet_PushPower,
				GravimineBullet_MinSpeed = options.GravimineBullet_MinSpeed,
				GravimineBullet_Resistance = options.GravimineBullet_Resistance,
				GravimineBullet_StartSpeed = options.GravimineBullet_StartSpeed,
                GravimineBullet_Reward = options.GravimineBullet_Price,


                MissileBullet_Radius = options.MissileBullet_Radius,
                MissileBullet_EnergyPower = options.MissileBullet_EnergyPower,
                MissileBullet_PushPower = options.MissileBullet_PushPower,
                MissileBullet_MinSpeed = options.MissileBullet_MinSpeed,
                MissileBullet_Resistance = options.MissileBullet_Resistance,
                MissileBullet_StartSpeed = options.MissileBullet_StartSpeed,
                MissileBullet_Reward = options.MissileBullet_Price,

                GravimineBonus_Radius = options.GravimineBonus_Radius,

                MissileBonus_Radius = options.MissileBonus_Radius,

				BoosterBonus_Radius = options.BoosterBonus_Radius,
				BoosterBonus_Multiplier = options.BoosterBonus_Multiplier,
				BoosterBonus_Duration = options.BoosterBonus_Duration,


                FireBoosterBonus_Radius = options.FireboosterBonus_Radius,
                FireBoosterBonus_NewCooldown = options.FireboosterBonus_NewCooldown,
                FireBoosterBonus_Duration = options.FireboosterBonus_Duration,


                GravishieldBonus_Radius = options.GravishieldBonus_Radius,
				GravishieldBonus_InitStrength = options.GravishieldBonus_InitStrength,
				GravishieldBonus_Duration = options.GravishieldBonus_Duration,


                DashBonus_Radius = options.DashBonus_Radius,
                DashBonus_Boost = options.DashBonus_Boost,


                Bot_MessagesMaxTotalLength = options.Bot_MessagesMaxTotalLength,
				#endregion
			};
		}

		
		public CWC5.DTOModel.PhysicalObject GetPhisicalObject(PhysicalObject obj)
		{
			return new DTOModel.PhysicalObject()
			{
				Location = GetPoint(obj.Location),
				Radius = obj.Radius,
				Speed = GetPoint(obj.Speed)
			};
		}
		
		public CWC5.DTOModel.PlasmaBullet GetPlasma(Plasma bullet)
		{
			return new DTOModel.PlasmaBullet()
			{
				Body = GetPhisicalObject(bullet),
				OwnerId = bullet.Owner,
			};
		}

        public CWC5.DTOModel.MissileBullet GetMissile(Missile bullet)
        {   //todo
            return new DTOModel.MissileBullet()
            {
                Body = GetPhisicalObject(bullet),
                OwnerId = bullet.Owner,
                TargetId = bullet.TargetId
            };
        }

        public CWC5.DTOModel.GravimineBullet GetGravimine(Gravimine bullet)
		{
			return new DTOModel.GravimineBullet()
			{
				Body = GetPhisicalObject(bullet),
				OwnerId = bullet.Owner,
			};
		}

		public CWC5.DTOModel.EnergySpill GetEnergySpill(EnergySpill bonus)
		{
			return new DTOModel.EnergySpill()
			{
				Body = GetPhisicalObject(bonus),
				RemainingTime = bonus.RemainingTime,
			};        
		}

		public CWC5.DTOModel.GravimineBonus GetGravimineBonus(GravimineBonus bonus)
		{
			return new DTOModel.GravimineBonus()
			{
				Body = GetPhisicalObject(bonus)
			};
		}

        public CWC5.DTOModel.MissileBonus GetMissileBonus(MissileBonus bonus)
        {
            return new DTOModel.MissileBonus()
            {
                Body = GetPhisicalObject(bonus)
            };
        }


        public CWC5.DTOModel.GravishieldBonus GetGravishieldBox(GravishieldBonus bonus)
		{
			return new DTOModel.GravishieldBonus()
			{
				Body = GetPhisicalObject(bonus),
			};
		}
        public CWC5.DTOModel.DashBonus GetDashBox(DashBonus bonus)
        {
            return new DTOModel.DashBonus()
            {
                Body = GetPhisicalObject(bonus),
            };
        }




        public CWC5.DTOModel.BoosterBonus GetBoosterCell(BoosterBonus bonus)
		{
			return new DTOModel.BoosterBonus()
			{
				Body = GetPhisicalObject(bonus),
			};
		}
        public CWC5.DTOModel.FireBoosterBonus GetFireBoosterCell(FireboosterBonus bonus)
        {
            return new DTOModel.FireBoosterBonus()
            {
                Body = GetPhisicalObject(bonus),
            };
        }
    }
}
