﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.LogicalCore
{
    [Serializable]
    public class DPoint
    {
        public static DPoint Zero
        {
            get { return new DPoint(0.0, 0.0); }
        }

        public Double X { get; set; }
        public Double Y { get; set; }

        public DPoint()
        {

        }

        public DPoint(Double x, Double y) : this()
        {
            X = x; Y = y;
        }

        public DPoint Add(DPoint p)
        {
            return new DPoint(X + p.X, Y + p.Y);
        }

        public DPoint Add(int length)
        {
            var n = Normalize();
            return new DPoint(X + n.X * length, Y + n.Y * length);
        }

        public DPoint Substract(DPoint p)
        {
            return new DPoint(X - p.X, Y - p.Y);
        }

        public DPoint Multiply(Double k)
        {
            return new DPoint(X * k, Y * k);
        }

        public DPoint Invert()
        {
            return new DPoint(-X, -Y);
        }

        public Double GetLength()
        {
            return Math.Sqrt(X * X + Y * Y);
        }
        
        public DPoint Normalize()
        {
            Double mod = GetLength();
            if (Math.Abs(mod) < Double.Epsilon)
                return new DPoint(0, 0);
            return new DPoint(X / mod, Y / mod);
        }

        public DPoint Clone()
        { return (DPoint)this.MemberwiseClone(); }
        
    }
}
