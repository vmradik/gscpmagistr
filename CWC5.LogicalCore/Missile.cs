﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Model.Base;

namespace CWC5.LogicalCore.Model
{
    [Serializable]
    public class Missile : Bullet
    {
        public Missile(Double power, int targetId, Double price = 0)
        {
            Price = price;
            TargetId = targetId;
            Power = power;
        }

        public int TargetId
        {
            get;
            set;
        }

        public Double Power { get; set; }

        /// <summary>
        /// Получает или задает направление
        /// </summary>
        public Angle360 Direction
        {
            get;
            set;
        }
        public Double Price
        {
            get;
            set;
        }

    }
}
