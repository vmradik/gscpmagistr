﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Gscp.Grpc_Definitions
{
    public class Grpc_tools
	{

		public static TimeSpan get_grpc_timespan()
		{
			return new TimeSpan(0, 5, 0);
			//return DateTime.UtcNow.Add(_receiveTimeout);
		}
		public static DateTime get_grpc_deadline()
		{
			return DateTime.UtcNow.Add(get_grpc_timespan());
			//return DateTime.UtcNow.AddMinutes(5);
			//return DateTime.UtcNow.Add(_receiveTimeout);
		}

		public static CancellationToken get_grpc_deadline_token()
		{
			return new CancellationTokenSource(Grpc_tools.get_grpc_timespan()).Token;
		}
	}

}
