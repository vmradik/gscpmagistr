﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gscp.Compiler
{
    internal class JSONCompiler : CompilerBase
    {
        public JSONCompiler()
        {
            CompiledFileName = "bot.json";
            SourceFileName = "MyBot.json";
            CompilerFolder = "CWC5.Template.JSON";

        }
        public override string CompilerFolder{ get; set; }

        public override string SourceFileName { get; set; }
        public override string CompiledFileName { get; set; }

        protected override string CompilerFileName
        {
            get { return "json.bat"; }
        }

        protected override string ResultFileName
        {
            get { return "result.txt"; }
        }



        protected override int EncodingCode
        {
            get { return 866; } // TODO maybe need to fix
        }
    }
}
