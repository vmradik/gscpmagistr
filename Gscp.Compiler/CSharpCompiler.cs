﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gscp.Compiler
{
    internal class CSharpCompiler : CompilerBase
    {
        public CSharpCompiler()
        {
            CompiledFileName = "bot.exe";
            SourceFileName = "MyBot.cs";
            CompilerFolder = "CWC5.Template.CSharp";

        }
        public override string CompilerFolder{ get; set; }

        public override string SourceFileName { get; set; }
        public override string CompiledFileName { get; set; }

        protected override string CompilerFileName
        {
            get { return "cs.bat"; }
        }

        protected override string ResultFileName
        {
            get { return "result.txt"; }
        }



        protected override int EncodingCode
        {
            get { return 866; }
        }
    }
}
