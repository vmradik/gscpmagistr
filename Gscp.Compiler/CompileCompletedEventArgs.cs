﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gscp.Compiler
{
    public class CompilationCompletedEventArgs : EventArgs
    {
        public CompilationCompletedEventArgs(CompileResult result)
        {
            Result = result;
        }

        public CompileResult Result { get; set; }
    }
}
