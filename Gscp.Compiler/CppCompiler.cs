﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gscp.Compiler
{
    internal class CppCompiler : CompilerBase
    {



        public CppCompiler()
        {
            CompiledFileName = "bot.exe";
            SourceFileName = "MyBot.h";
            CompilerFolder = "CWC5.Template.Cpp";
        }

        public override string CompilerFolder { get; set; }

        public override string SourceFileName { get; set; }
        public override string CompiledFileName { get; set; }



        protected override string CompilerFileName
        {
            get { return "cpp.bat"; }
        }

        protected override string ResultFileName
        {
            get { return "result.txt"; }
        }



        protected override int EncodingCode
        {
            get { return 866; }
        }
    }
}
