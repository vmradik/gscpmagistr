﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gscp.Compiler
{
    internal class JavaCompiler : CompilerBase
    {
        
        public JavaCompiler()
        {
            CompiledFileName = "bot.jar";
            SourceFileName = @"src\Cwc\MyBot.java";
            CompilerFolder = "CWC5.Template.Java";

        }



        public override string CompilerFolder { get; set; }
        public override string SourceFileName { get; set; }
        public override string CompiledFileName { get; set; }



        protected override string CompilerFileName
        {
            get { return "java.bat"; }
        }
        protected override string ResultFileName
        {
            get { return "result.txt"; }
        }
        protected override int EncodingCode
        {
            get { return 866; }
        }
        
    }
}
