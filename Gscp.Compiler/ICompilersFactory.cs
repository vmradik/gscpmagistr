﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Gscp.Compiler
{
	public class ICompilersFactory
	{
		static ICompilersFactory()
		{
			_compilers = new Dictionary<LanguageType, CompilerBase>();
			_compilers[LanguageType.CSharp] = new CSharpCompiler();
			_compilers[LanguageType.Cpp] = new CppCompiler();
			_compilers[LanguageType.Java] = new JavaCompiler();			
			_compilers[LanguageType.JSON] = new JSONCompiler();
		}


		private Uri _compilersFolder;
		private Uri _batFolder;
		private string _projectName;
		public ICompilersFactory(Uri compilersFolder, Uri batFolder)
		{
			_compilersFolder = compilersFolder;
			_batFolder = batFolder;
		}
		public ICompilersFactory(Uri compilersFolder, Uri batFolder, string projectName)
		{
			_compilersFolder = compilersFolder;
			_batFolder = batFolder;
			_projectName = projectName;
		}

		public CompilerBase IProjectCompiler(LanguageType type)
		{
			if (_projectName != null) _compilers[type].CompilerFolder = _projectName;
			return _compilers[type].Clone(_compilersFolder, _batFolder);
		}
		public CompilerBase IProjectCompiler(int intType)
		{
			LanguageType type = ToLang(intType);
            return IProjectCompiler(type);

        }
        public CompilerBase IProjectCompiler(string strType)
        {
            LanguageType type = ToLang(strType);
            return IProjectCompiler(type);

        }


        private static Dictionary<LanguageType, CompilerBase> _compilers;

		public LanguageType ToLang(int i)
		{
			if (i == 0) return LanguageType.CSharp;
			else if (i == 1) return LanguageType.Cpp;
			else if (i == 2) return LanguageType.Java;
			else if(i == 3) return LanguageType.JSON;
			else return LanguageType.Error;
		}
        public LanguageType ToLang(string i)
        {
            if (i == "csharp") return LanguageType.CSharp;
            else if (i == "json") return LanguageType.JSON;
            else if (i == "cpp") return LanguageType.Cpp;
            else if (i == "java") return LanguageType.Java;
            else return LanguageType.Error;
        }
    }



}
