﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Gscp.Compiler
{
	public class CompileResult
	{
		public CompileResult(Exception e)
		{
			Exception = e;
			CompiledFlag = false;
		}

		public CompileResult(bool flag, String output)
		{
			CompiledFlag = flag;
			Output = output;           
		}


		public CompileResult(bool flag, String output, Byte[] result)
			: this(flag, output)
		{ 
			CompiledProgram = result;
		}

		public Exception Exception { get; set; }
		public String Output { get; set; }
		public bool CompiledFlag { get; set; }
		public Byte[] CompiledProgram { get; set; }

	}
}
