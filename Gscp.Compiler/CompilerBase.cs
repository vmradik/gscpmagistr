﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.IO.Compression;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace Gscp.Compiler
{
    public abstract class CompilerBase
    {
        protected const Int32 COMPILER_TIMEOUT = 30 * 1000;

        string vs_build_tools_bat_path;
        string jdk_path;

        public CompilerBase()
        {
            JObject conf = JObject.Parse(File.ReadAllText(".\\server_config.json"));
            vs_build_tools_bat_path = (string)conf["vs_build_tools_bat_path"];
            jdk_path = (string)conf["jdk_path"];
        }

        #region Abstract Paths

        protected Uri _compilersFolder;
        protected Uri _batFolder;

        public abstract String CompilerFolder { get; set; }
        public abstract String SourceFileName { get; set; }
        protected abstract String CompilerFileName { get; }
        protected abstract String ResultFileName { get; }
        public abstract String CompiledFileName { get; set; }
        protected abstract Int32 EncodingCode { get; }
        protected String FullCompilerFolder
        {
            get
            {
                return Path.Combine(_compilersFolder.LocalPath, CompilerFolder);
            }
        }
        protected String FullCompilerFileName
        {
            get
            {
                return Path.Combine(_batFolder.LocalPath, CompilerFileName);
            }
        }

        #endregion

        #region CompileLogic




        private ArrayList SourceFiles = new ArrayList();
        private int SourceFilesIndex;
        protected void ClearResources()
        {
            if (File.Exists(Path.Combine(FullCompilerFolder, ResultFileName)))
            {
                File.Delete(Path.Combine(FullCompilerFolder, ResultFileName));
            }
            if (File.Exists(Path.Combine(FullCompilerFolder, CompiledFileName)))
            {
                File.Delete(Path.Combine(FullCompilerFolder, CompiledFileName));
            }
            SourceFilesIndex = SourceFiles.Count;
            while ((--SourceFilesIndex) != -1)
            {
                if (File.Exists(Path.Combine(FullCompilerFolder, (string)SourceFiles[SourceFilesIndex])))
                {
                    File.Delete(Path.Combine(FullCompilerFolder, (string)SourceFiles[SourceFilesIndex]));
                }
                SourceFiles.RemoveAt(SourceFilesIndex);

            }
        }

        protected virtual CompileResult DoCompileLogic(String sourceCode)
        {
            ClearResources();
            SaveSourceCode(sourceCode);
            RunCompiler();
            var res = GetResult();
            ClearResources();
            return res;
        }
        protected virtual CompileResult DoCompileLogic(Uri sourceCode)
        {
            ClearResources();
            SaveSourceCode(sourceCode);
            RunCompiler();
            var res = GetResult();
            ClearResources();
            return res;
        }

        protected virtual CompileResult DoCompileLogic(byte[] sourceCode)
        {
            ClearResources();
            SaveSourceCode(sourceCode);
            RunCompiler();
            var res = GetResult();
            if (res.Output == "")
                ClearResources();
            return res;
        }

        protected virtual void SaveSourceCode(String sourceCode)
        {
            File.WriteAllText(Path.Combine(FullCompilerFolder, SourceFileName), sourceCode, Encoding.GetEncoding(1251));
            SourceFiles.Add(SourceFileName);
        }
        protected virtual void SaveSourceCode(Uri sourcePath)
        {
            string sourceCodePath = sourcePath.LocalPath;
            if (Path.GetExtension(sourceCodePath) != ".zip")
            {
                File.Copy(sourceCodePath, Path.Combine(FullCompilerFolder, Path.GetFileName(sourceCodePath)), true);
                SourceFiles.Add(Path.GetFileName(sourceCodePath));
            }
            else
            {
                ZipFile.ExtractToDirectory(sourceCodePath, FullCompilerFolder);
                using (ZipArchive archive = ZipFile.OpenRead(sourceCodePath))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        SourceFiles.Add(entry.Name);
                    }
                }
            }
        }
        protected virtual void SaveSourceCode(byte[] zipArc)
        {
            File.WriteAllBytes(Path.Combine(FullCompilerFolder, "temp.zip"), zipArc);
            SourceFiles.Add("temp.zip");
            SaveSourceCode(new Uri(Path.Combine(FullCompilerFolder, "temp.zip")));

        }
        protected virtual void RunCompiler()
        {
            Boolean timeoutException = false;
            var procStartInfo = new ProcessStartInfo
            {
                FileName = "cmd",
                Arguments = $"/c {FullCompilerFileName}, {vs_build_tools_bat_path}, {jdk_path}",
                WorkingDirectory = FullCompilerFolder,
                UseShellExecute = false
            };

            // todo nomdusk log server-side output
            Process proc = Process.Start(procStartInfo);
            timeoutException = !proc.WaitForExit(COMPILER_TIMEOUT);

            if (timeoutException)
            {
                throw new TimeoutException("The compiler timeout has expired");
            }

        }

        protected virtual CompileResult GetResult()
        {
            string output = File.ReadAllText(Path.Combine(FullCompilerFolder, ResultFileName), Encoding.GetEncoding(EncodingCode));
            if (IsCompilationSuccessful(output))
            {
                byte[] file = File.ReadAllBytes(Path.Combine(FullCompilerFolder, CompiledFileName));
                return new CompileResult(true, output, file);
            }
            else
            {
                return new CompileResult(false, output);
            }
        }

        protected virtual Boolean IsCompilationSuccessful(string rez)
        {
            if (CompiledFileName == "bot.jar")
            {
                if (rez == "") return true;
                else
                {
                    var m = Regex.Match(rez, "Error|error");
                    return !m.Success;
                }
            }
            else
            {
                if (!File.Exists(Path.Combine(FullCompilerFolder, CompiledFileName)))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        #endregion


        private delegate CompileResult CompileHandler(String sourceCode);
        private CompileHandler onCompileHandler;

        public CompileResult Compile(String sourceCode)
        {
            CompileResult result;
            try
            {
                result = DoCompileLogic(sourceCode);
                if (File.Exists(Path.Combine(FullCompilerFolder, CompiledFileName)))
                {
                    File.Delete(Path.Combine(FullCompilerFolder, CompiledFileName));
                }
            }
            catch (Exception e)
            {
                return new CompileResult(e);
            }
            return result;
        }

        public CompileResult Compile(byte[] sourceCode)
        {
            CompileResult result;
            try
            {
                result = DoCompileLogic(sourceCode);
                if (File.Exists(Path.Combine(FullCompilerFolder, CompiledFileName)))
                {
                    File.Delete(Path.Combine(FullCompilerFolder, CompiledFileName));
                }
            }
            catch (Exception e)
            {
                return new CompileResult(e);
            }
            return result;
        }

        public CompileResult Compile(Uri sourceCode)
        {
            CompileResult result;
            try
            {
                result = DoCompileLogic(sourceCode);
                if (File.Exists(Path.Combine(FullCompilerFolder, CompiledFileName)))
                {
                    File.Delete(Path.Combine(FullCompilerFolder, CompiledFileName));
                }
            }
            catch (Exception e)
            {
                return new CompileResult(e);
            }
            return result;
        }

        public void CompileAsync(String sourceCode)
        {
            if (onCompileHandler == null)
            {
                onCompileHandler = new CompileHandler(Compile);
            }
            onCompileHandler.BeginInvoke(sourceCode, new AsyncCallback(OnCompilationCompleted), null);
        }

        private void OnCompilationCompleted(IAsyncResult result)
        {
            var res = onCompileHandler.EndInvoke(result);
            if (CompilationCompleted != null)
            {
                CompilationCompleted(this, new CompilationCompletedEventArgs(res));
            }
        }

        public event EventHandler<CompilationCompletedEventArgs> CompilationCompleted;


        internal CompilerBase Clone(Uri compilersFolder, Uri batFolder)
        {
            var res = (CompilerBase)this.MemberwiseClone();
            res._compilersFolder = compilersFolder;
            res._batFolder = batFolder;
            return res;
        }
    }
}
