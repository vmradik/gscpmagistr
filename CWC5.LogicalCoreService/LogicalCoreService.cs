﻿using Grpc.Core;
using GscpGrpc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using grpc = global::Grpc.Core;

namespace CWC5.LogicalCoreService
{
    public class LogicalCoreService : GscpGrpc.LogicalCoreService.LogicalCoreServiceBase
    {
        string address_path = "./settings.json";

        Server server;

        bool running;

        public LogicalCoreService() { }

        public void run()
        {
            Environment.SetEnvironmentVariable("NO_PROXY", "localhost");
            //port = 52001;
            //_process.Start_with_host(ip, port);
            JObject o = JObject.Parse(File.ReadAllText(address_path));
            if (!o.ContainsKey("host") || !o.ContainsKey("port"))
            {
                throw new Exception("invalid connection data");
            }
            string host = o["host"].ToString();
            int port = o["port"].Value<int>();
            var services = GscpGrpc.LogicalCoreService.BindService(this);
            var ports = new ServerPort(host, port, ServerCredentials.Insecure);
            server = new Grpc.Core.Server()
            {
                Services = { services },
                Ports = { ports }
            };
            server.Start();
            Console.WriteLine($"Listening on {host}:{port}...");
            running = true;
            while(running)
            {
                Thread.Sleep(1000);
            }
            Console.WriteLine($"Exiting.");
        }

        public override global::System.Threading.Tasks.Task<global::GscpGrpc.ArenaResponse> GetStartingArena(global::GscpGrpc.ArenaRequest request, grpc::ServerCallContext context)
        {
            Console.WriteLine($"Received empty arena request");
            var opts = JsonConvert.DeserializeObject(request.GameOptions, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                SerializationBinder = new CWC5.LogicalCore.Model.LogicalCoreTypesBinder()
            });
            if (opts is CWC5.LogicalCore.Model.GameOptions valid_opts)
            {
                var arena = LogicalCore.Controller.GameController.GetEmptyArena(valid_opts);
                var json = JsonConvert.SerializeObject(arena, Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects,
                    SerializationBinder = new CWC5.LogicalCore.Model.LogicalCoreTypesBinder()
                });
                Console.WriteLine($"Returning starting arena");
                return Task.FromResult(new ArenaResponse() { Arena = json});
            }
            else
            {
                throw new Exception("received invalid object. /n " + request.GameOptions);
            }
        }

        public override global::System.Threading.Tasks.Task<global::GscpGrpc.NewTickArena> GetNextTick(global::GscpGrpc.TickRequest request, grpc::ServerCallContext context)
        {
            Console.WriteLine($"Received new tick request from {context.Peer}");
            var arena = JsonConvert.DeserializeObject(request.Arena, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                SerializationBinder = new CWC5.LogicalCore.Model.LogicalCoreTypesBinder()
            }) as CWC5.LogicalCore.Model.Arena;
            var bot_responses = JsonConvert.DeserializeObject<List<CWC5.DTOModel.TurnResult?>>(request.BotAnswers, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                SerializationBinder = new CWC5.LogicalCore.Model.LogicalCoreTypesBinder()
            });
                
            var r2 = bot_responses as List<CWC5.DTOModel.TurnResult?>;
            Console.WriteLine($"Modelling tick {arena.Tick} from {context.Peer}...");
            var new_arena = LogicalCore.Controller.GameController.GetNextFrame(arena, r2);
            var arena_json = JsonConvert.SerializeObject(new_arena, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                SerializationBinder = new CWC5.LogicalCore.Model.LogicalCoreTypesBinder()
            });
            return Task.FromResult(new NewTickArena() { Arena = arena_json });
        }
    }
}
