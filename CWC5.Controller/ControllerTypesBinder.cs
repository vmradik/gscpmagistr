﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWC5.Controller
{

    public class ControllerTypesBinder : CWC5.LogicalCore.Model.LogicalCoreTypesBinder
    {
        public ControllerTypesBinder() : base()
        {
            KnownTypes.Add(typeof(CWC5.Controller.BotInfo));
            KnownTypes.Add(typeof(ExecutableType));
            KnownTypes.Add(typeof(ModelingResult));
            KnownTypes.Add(typeof(Exception));
            KnownTypes.Add(typeof(Score));
        }
    }
}
