﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    internal class FourthBot : SimpleBotBase
    {
        protected override void Turn(Arena arena)
        {

            Accelerate(new Point() { X = 10 - Random.NextDouble() * 20, Y = 10 - Random.NextDouble() * 20 });

            SetCannonDirection(arena.Tick * 0.05);

            if (arena.Me.PlasmaCooldown == 0)
                Fire(BulletType.Plasma);
            if (arena.Me.GravimineCount > 0)
                Fire(BulletType.Gravimine);
            
        }

        protected override void Init(GameOptions options)
        {
        }
    }
}
