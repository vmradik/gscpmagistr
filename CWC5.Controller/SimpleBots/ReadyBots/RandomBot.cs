﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    internal class RandomBot : SimpleBotBase
    {
        protected void Move(Arena Arena)
        {

            Accelerate(new Point() { X = 10 - Random.NextDouble() * 20, Y = 10 - Random.NextDouble() * 20 });

            if (Arena.Me.PlasmaCooldown == 0)
                Fire(BulletType.Plasma);
            if (Arena.Me.GravimineCount > 0)
                Fire(BulletType.Gravimine);

            SetCannonDirection(Random.NextDouble() * 2 * Math.PI);
            if (Arena.Me.PlasmaCooldown == 0)
                Fire(BulletType.Plasma);
            if (Arena.Me.GravimineCount > 0)
                Fire(BulletType.Gravimine);
        }


        protected override void Init(GameOptions options)
        {
            return;
        }

        protected override void Turn(Arena arena)
        {
            Move(arena);
        }
    }
}
