﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    //Воронежский кукурузник
    class Bot5 : SimpleBotBase
    {
        #region start
        public class Dot
        {
            public Dot secondDot;
            public Point ToPoint() { return new Point(x, y); }
            public Dot() { }
            public Dot(Point one) { this.x = one.X; this.y = one.Y; }
            public Dot(double x, double y) { this.x = x; this.y = y; }
            public double x, y;
            public double DistTo(Dot b)
            {
                return Math.Sqrt((x - b.x) * (x - b.x) + (y - b.y) * (y - b.y));
            }

            public double len()
            {
                return Math.Sqrt((x) * (x) + (y) * (y));
            }

            public Dot Add(Dot one)
            {
                return new Dot { x = one.x + x, y = one.y + y };
            }

            public Dot Sub(Dot one)
            {
                return new Dot { x = -one.x + x, y = -one.y + y };
            }


            public Dot Mult(double one) { return new Dot { x = x * one, y = y * one }; }

            public Dot Normalized()
            {
                var len = this.DistTo(new Dot());
                if (DoubleEqual(len, 0))
                {
                    return new Dot { x = 1, y = 0 };
                }
                else
                {
                    return new Dot { x = x / len, y = y / len };
                }
            }
        }

        public static bool DoubleEqual(double a, double b)
        {
            return (Math.Abs(a - b) < 0.0000001);
        }

        Arena arena;
        void Log(string text, int tick)
        {
            //  if (arena.Tick <= tick)
            //    AddLogMessage(text);
        }


        #endregion



        protected override void Init(GameOptions options)
        {
            settings = options;
        }


        protected override void Turn(Arena arena)
        {
            try
            {
                this.arena = arena;
                we = new Dot(arena.Me.Body.Location);
                speed = new Dot(arena.Me.Body.Speed);
                var point = WhereToGo();
                GoToPoint(point);

                //var shootDefenceOrNull = WhereToShootDefenceOrNull();
                var shootFromEnergy = ShootFromEnergy();
                var dangerousEnemy = DangerousEnemy();

                var sittingEnemy = SittingEnemy();

                if (sittingEnemy != null)
                {
                    ShootHard(sittingEnemy);

                }
                else if (shootFromEnergy != null)
                {
                    // if (arena.Me.GravimineCount > 0)
                    // {
                    //     ShootHard(shootFromEnergy);
                    // }
                    // else
                    {
                        ShootSimple(shootFromEnergy);

                    }
                }
                else if (dangerousEnemy != null)
                {


                    //if (arena.Me.GravimineCount > 0 && dangerousEnemy.secondDot.DistTo(we) < 150) //todo and 
                    //{
                    //    ShootHard(dangerousEnemy.secondDot);
                    //}
                    //else
                    {
                        ShootSimple(dangerousEnemy, true);

                    }
                }
                /*else if (shootDefenceOrNull != null)
                {
                    ShootSimple(shootDefenceOrNull);
                }
                */
                else
                {
                    var nearestWithHardBullet = arena.Enemies.Where(x => x.GravimineCount > 0)
                        .Select(x => new Dot(x.Body.Location))
                        .OrderBy(x => we.DistTo(x)).FirstOrDefault();
                    if (nearestWithHardBullet != null)
                    {
                        ShootSimple(nearestWithHardBullet, false);
                    }
                }

                if (arena.EnergySpills.Length > 0 && we.DistTo(new Dot(arena.EnergySpills[0].Body.Location)) < 150)
                {
                    this.ActivateShield();
                }
            }
            catch
            {

            }
        }

        private Dot SittingEnemy()
        {
            if (arena.Me.GravimineCount == 0)
                return null;

            var enemies = arena.Enemies.Where(x => x.IsInEnergySpill).Select(x => new Dot(x.Body.Location))
                .Where(dot => we.DistTo(dot) < 100)
                .ToList();

            if (enemies.Count > 0)
                return enemies.First();

            return null;
        }
        Dot we;
        Dot speed;
        GameOptions settings;


        #region moving


        Dot NearestShield()
        {
            if (arena.GravishieldBonuses.Length != 0)
            {
                var res = arena.GravishieldBonuses.Select(x => new Dot(x.Body.Location)).OrderBy(x => x.DistTo(we)).First();
                if (res.DistTo(we) > 500)
                    return null;
                else
                    return res;
            }
            return null;
        }

        Dot NearestGravimine()
        {
            if (arena.GravimineBonuses.Length != 0)
            {
                var res = arena.GravimineBonuses.Select(x => new Dot(x.Body.Location)).OrderBy(x => x.DistTo(we)).First();
                if (res.DistTo(we) > 500)
                    return null;
                else
                    return res;
            }
            return null;
        }

        Dot NearestBoosterBonus()
        {
            if (arena.BoosterBonuses.Length != 0)
            {
                var res = arena.BoosterBonuses.Select(x => new Dot(x.Body.Location)).OrderBy(x => x.DistTo(we)).First();
                if (res.DistTo(we) > 800)
                    return null;
                else
                    return res;
            }
            return null;
        }


        //Dot NearestBoosterBonus()
        //{
        //    if (arena.BoosterBonuses.Length != 0)
        //    {
        //        var res = arena.BoosterBonuses.Select(x => new Dot(x.Body.Location)).OrderBy(x => x.DistTo(we)).First();
        //        if (res.DistTo(we) > 800)
        //            return null;
        //        else
        //            return res;
        //    }
        //    return null;
        //}

        List<double> previousDistancesToEnergy = new List<double>();
        Dot priorityPoint = null;
        int prioritySetTick = -1000;
        Dot WhereToGo()
        {
            //bool near = arena.Me.IsInEnergySpill == false && 

            var nearestGravimine = NearestGravimine();
            var nearestShield = NearestShield();
            var nearestBooster = NearestBoosterBonus();

            int B = 10000;
            previousDistancesToEnergy.Add(B);

            if (priorityPoint != null && priorityPoint.DistTo(we) < 10)
            {
                priorityPoint = null;
            }

            if (arena.EnergySpills.Length == 0)
            {
                priorityPoint = null;
            }


            if (priorityPoint != null && arena.Tick - prioritySetTick < 30)
            {
                return priorityPoint;
            }
            else if (arena.EnergySpills.Length != 0)
            {

                var energy = arena.EnergySpills.FirstOrDefault();
                var energyPos = new Dot(energy.Body.Location);
                previousDistancesToEnergy[arena.Tick] = energyPos.DistTo(we);

                double eps = 15;
                bool stupidStay = arena.Tick > 50 && arena.Me.IsInEnergySpill == false
                   && Math.Abs(previousDistancesToEnergy[arena.Tick - 20] - previousDistancesToEnergy[arena.Tick]) < eps
                  && Math.Abs(previousDistancesToEnergy[arena.Tick - 30] - previousDistancesToEnergy[arena.Tick]) < eps &&
                  previousDistancesToEnergy[arena.Tick] < B - 0.0001;


                if (stupidStay)
                {
                    Log("stupid stay " + arena.Tick.ToString(), arena.Tick + 1);
                    //if (false)
                    {
                        prioritySetTick = arena.Tick;


                        priorityPoint = null;
                        priorityPoint = arena.BoosterBonuses.Select(x => new Dot(x.Body.Location)).OrderBy(x => x.DistTo(we)).FirstOrDefault();

                        if (priorityPoint == null || priorityPoint.DistTo(we) > 600)
                        {
                            priorityPoint = new Dot(50, 50);
                        }
                    }
                }



                return energyPos;
            }
            else if (nearestBooster != null)
            {
                return nearestBooster;// arena.BoosterBonuses.Select(x => new Dot(x.Body.Location)).OrderBy(x => x.DistTo(we)).First();

            }
            else if (nearestGravimine != null)
            {
                return nearestGravimine;// arena.GravimineBonuses.Select(x => new Dot(x.Body.Location)).OrderBy(x => x.DistTo(we)).First();
            }
            else if (nearestShield != null)
            {
                return nearestShield;// arena.GravishieldBonuses.Select(x => new Dot(x.Body.Location)).OrderBy(x => x.DistTo(we)).First();
            }
            else
            {

                //return new Dot { x = 50, y = settings.Arena_Size.Y / 2 };


                var goodPoint = GetAimPoints();

                var distToFirst = goodPoint.Item1.DistTo(we);
                var distToSecond = goodPoint.Item2.DistTo(we);

                if (goToFirstFlag)
                {
                    if (distToFirst < 10)
                    {
                        goToFirstFlag = false;
                    }
                }
                else
                {
                    if (distToSecond < 10)
                        goToFirstFlag = true;
                }

                if (goToFirstFlag)
                    return goodPoint.Item1;
                else
                    return goodPoint.Item2;
            }

        }


        bool goToFirstFlag = true;

        void GoToPoint(Dot point)
        {
            double coef = 0.5;
            Dot pos = we.Add(speed.Mult(speed.len() * coef));
            Dot acVec = point.Sub(pos);
            if (acVec.len() > 0)
            {
                acVec = acVec.Normalized().Mult(10);
                this.Accelerate(acVec.ToPoint());

            }
        }
        #endregion


        Dot ShootFromEnergy()
        {
            if (arena.Me.IsInEnergySpill)
            {

                var enemies = arena.Enemies
                    .OrderBy(x => new Dot(x.Body.Location).DistTo(we) + 5 * Math.Abs(arena.Me.Direction - GetAngle(we.ToPoint(), x.Body.Location)))
                    .Select(x => new Dot(x.Body.Location))
                    .FirstOrDefault();

                //if (enemies == null)
                //    return null;

                //var enemyItself= arena.Enemies
                //    .OrderBy(x => new Dot(x.Body.Location).DistTo(we) + 5*Math.Abs(arena.Me.Direction - GetAngle(we.ToPoint() , x.Body.Location ) ))
                //  .Select(x=>new Dot(x.Body.Location))
                //    .FirstOrDefault();
                //enemies.secondDot = enemyItself;

                if (enemies != null)
                    return enemies;
            }
            else
            {
                return null;
            }
            return null;
        }

        Dot DangerousEnemy()
        {
            if (arena.Me.IsInEnergySpill)
                return null; //use shoot from energy

            if (arena.Enemies.Length == 0)
                return null;

            var enemies = arena.Enemies
                    .OrderByDescending(x => x.Energy)
                    .FirstOrDefault();



            var position = new Dot(enemies.Body.Location);
            var speed = new Dot(enemies.Body.Speed);

            var dist = we.DistTo(position);

            var mnoz = dist / 30;


            var ans = position.Add(speed.Mult(mnoz));
            ans.secondDot = new Dot(enemies.Body.Location);
            return ans;

        }
        Dot WhereToShootDefenceOrNull()
        {
            var bulletPositions = arena.GravimineBullets.Select(x => new Dot(x.Body.Location)).ToList();
            bulletPositions.AddRange(arena.PlasmaBullets.Select(x => new Dot(x.Body.Location)));

            var bulletSpeeds = arena.GravimineBullets.Select(x => new Dot(x.Body.Speed)).ToList();
            bulletSpeeds.AddRange(arena.PlasmaBullets.Select(x => new Dot(x.Body.Speed)));

            int timeLimit = 25;
            var dangerousShells = new List<int>();




            for (int bulletIndex = 0; bulletIndex < bulletPositions.Count; bulletIndex++)
            {
                var bullet = bulletPositions[bulletIndex];
                var speed = bulletSpeeds[bulletIndex];
                for (int time = 0; time < timeLimit; time++)
                {
                    var bulletCurrent = bullet.Add(speed.Mult(time));
                    var bulletRadius = settings.PlasmaBullet_Radius;
                    bulletRadius += 30;
                    var ourRadius = settings.Spaceship_Radius;

                    var dist = we.DistTo(bulletCurrent);

                    if (dist < bulletRadius + ourRadius)
                    {
                        dangerousShells.Add(bulletIndex);
                        break;
                    }


                }
            }


            if (dangerousShells.Count > 0)
            {
                return bulletPositions[dangerousShells[0]];
            }

            return null;
            //var anglesToShells = new List<double>();
            //for (int i = 0; i < dangerousShells.Count; i++)
            //{
            //    int bulletIndex = dangerousShells[i];
            //    var angle = GetAngle(bulletPositions[bulletIndex].ToPoint());
            //}
        }

        void ShootSimple(Dot point, bool shoot = true)
        {
            this.SetCannonDirection(GetAngle(we.ToPoint(), point.ToPoint()));


            //var eps = Math.PI / 6 + 0.001;
            bool canShoot = true;// Math.Abs(arena.Me.Direction - GetAngle(we.ToPoint(), point.ToPoint())) < eps;

            // var ourAngle = GetAngle(we.ToPoint());
            // var pointAngle = GetAngle(point.ToPoint());

            if (canShoot)
            {
                if (shoot)
                    this.Fire(BulletType.Plasma);
            }
        }

        void ShootHard(Dot point, bool shoot = true)
        {
            this.SetCannonDirection(GetAngle(we.ToPoint(), point.ToPoint()));


            //var eps = Math.PI / 6 + 0.001;
            bool canShoot = true;// Math.Abs(arena.Me.Direction - GetAngle(we.ToPoint(), point.ToPoint())) < eps;

            // var ourAngle = GetAngle(we.ToPoint());
            // var pointAngle = GetAngle(point.ToPoint());

            if (canShoot)
            {
                if (shoot)
                    this.Fire(BulletType.Gravimine);
            }
        }

        Tuple<Dot, Dot> GetAimPoints()
        {
            var corners = new List<Dot>{
                new Dot(0,0),
                new Dot( settings.Arena_Size.X, 0),
                new Dot(0, settings.Arena_Size.Y),
                new Dot(settings.Arena_Size.X, settings.Arena_Size.Y)
            };

            var correspondingPoints = new List<Tuple<Dot, Dot>>
            {
                Tuple.Create(new Dot(150,50), new Dot(50,150)),
                Tuple.Create(new Dot(settings.Arena_Size.X-150, 50), new Dot(settings.Arena_Size.X - 50 , 150)),
                Tuple.Create(new Dot(150, settings.Arena_Size.Y - 50), new Dot(50, settings.Arena_Size.Y -150)),
                Tuple.Create(new Dot(settings.Arena_Size.X - 150, settings.Arena_Size.Y - 50),
                    new Dot(settings.Arena_Size.X - 50, settings.Arena_Size.Y - 150))
            };


            var enemies = arena.Enemies.Select(x => new Dot(x.Body.Location));

            var bestCorner = corners.OrderByDescending(x =>
                enemies.Min(e => e.DistTo(x))

            ).FirstOrDefault();

            return correspondingPoints[corners.IndexOf(bestCorner)];

        }
    }
}
