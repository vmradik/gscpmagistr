﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    internal class SampleBot : SimpleBotBase
    {
        private Double _angle;
        private GameOptions _options;

        protected override void Init(GameOptions options)
        {
            _angle = Math.PI / 2.0;
            _options = options;
        }

        protected override void Turn(Arena arena)
        {
            if (arena.Me.PlasmaCooldown == 0)
                Fire(BulletType.Plasma);
            else if (arena.Me.GravimineCount > 0)
                Fire(BulletType.Gravimine);

            Point c = new Point();
            c.X = _options.Arena_Size.X / 2.0;
            c.Y = _options.Arena_Size.Y / 2.0;
            Accelerate(GetVector(GetAngle(arena.Me.Body.Location, c)));

            _angle += _options.Spaceship_MaxTurnSpeed;
            SetCannonDirection(_angle);
        }
    }
}
