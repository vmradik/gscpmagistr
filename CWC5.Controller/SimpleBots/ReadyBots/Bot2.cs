﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    class Bot2 : SimpleBotBase
    {
        private GameOptions opt;
        private Arena a;
        private Point me;

        protected override void Init(GameOptions options)
        {
            opt = options;
        }

        protected override void Turn(Arena arena)
        {
            a = arena;
            me = a.Me.Body.Location;

            Firenear();


            if (arena.Me.IsInEnergySpill)
                InBubble();
            //else if (a.EnergySpills.Length != 0)
            if (a.EnergySpills.Length != 0)
                Bubble();
            else
                NoBubble();



        }

        void InBubble()
        {
            if (a.Me.GravishieldCount > 0 && a.Me.GravishieldRemainingTime <= 0)
                ActivateShield();
            //if (CountEnemies() > 0)
            //    FireMine();

        }

        void Bubble()
        {
            double r = a.EnergySpills[0].Body.Radius;
            Point bub = a.EnergySpills[0].Body.Location;

            if (CountEnemies() == 0)
            {
                Accelerate(GetVector(GetAngle(me, bub)));
            }
            else
            {
                if (a.Me.GravimineCount == 0 && a.GravimineBonuses.Length != 0)
                {
                    double max = double.MinValue;
                    GravimineBonus bestMine = a.GravimineBonuses[0];
                    foreach (GravimineBonus mine in a.GravimineBonuses)
                    {
                        double dist = GetDistance(mine.Body.Location, me);
                        if (dist > max)
                        {
                            max = dist;
                            bestMine = mine;
                        }
                    }
                    if (GetDistance(bestMine.Body.Location, me) < 200 && a.GravimineBonuses.Length > 0)
                        Accelerate(GetVector(GetAngle(me, bestMine.Body.Location)));
                    else
                    {
                        Accelerate(GetVector(GetAngle(me, bub)));
                    }

                }
                if (a.Me.GravimineCount > 0)
                {
                    FireMine();
                    Accelerate(GetVector(GetAngle(me, bub)));
                }
            }
        }

        void NoBubble()
        {
            if (a.GravimineBonuses.Length == 0 && a.BoosterBonuses.Length == 0 && a.GravishieldBonuses.Length == 0)
            {
                Mans(me);
            }
            else
            {
                double minMine = double.MaxValue;
                double minboost = double.MaxValue;
                GravimineBonus bestmine = new GravimineBonus();
                BoosterBonus bestboost = new BoosterBonus();
                GravishieldBonus bestshit = new GravishieldBonus();
                double minsHIT = double.MaxValue;

                if (a.GravimineBonuses.Length != 0)
                {
                    bestmine = a.GravimineBonuses[0];
                    foreach (GravimineBonus mine in a.GravimineBonuses)
                    {
                        double dist = GetDistance(me, mine.Body.Location);
                        if (dist < minMine)
                        {
                            minMine = dist;
                            bestmine = mine;
                        }
                    }
                }

                if (a.BoosterBonuses.Length != 0)
                {
                    bestboost = a.BoosterBonuses[0];
                    foreach (BoosterBonus mine in a.BoosterBonuses)
                    {
                        double dist = GetDistance(me, mine.Body.Location);
                        if (dist < minMine)
                        {
                            minboost = dist;
                            bestboost = mine;
                        }
                    }
                }

                if (a.GravishieldBonuses.Length != 0)
                {
                    bestshit = a.GravishieldBonuses[0];
                    foreach (GravishieldBonus mine in a.GravishieldBonuses)
                    {
                        double dist = GetDistance(me, mine.Body.Location);
                        if (dist < minMine)
                        {
                            minsHIT = dist;
                            bestshit = mine;
                        }
                    }
                }


                Point target = new Point(0, 0);

                if (minMine <= minboost && minMine <= minsHIT)
                {
                    target = bestmine.Body.Location;
                }
                if (minboost <= minMine && minboost <= minsHIT)
                {
                    target = bestboost.Body.Location;
                }
                if (minsHIT <= minboost && minsHIT <= minMine)
                {
                    target = bestshit.Body.Location;
                }

                Accelerate(GetVector(GetAngle(me, target)));
            }
        }

        void Firenear()
        {
            double max = double.MinValue;
            Spaceship enemy = a.Enemies[0];
            foreach (Spaceship boost in a.Enemies)
            {
                double dist = GetDistance(boost.Body.Location, me);
                if (dist > max && !boost.IsInEnergySpill)
                {
                    max = dist;
                    enemy = boost;
                }
            }
            SetCannonDirection(GetAngle(me, enemy.Body.Location));
            Fire(BulletType.Plasma);
        }

        void FireMine()
        {
            if (a.GravimineBonuses.Length != 0)
            {
                SetCannonDirection(GetAngle(me, a.EnergySpills[0].Body.Location));
                Fire(BulletType.Gravimine);
            }

        }

        int CountEnemies()
        {
            int c = 0;
            foreach (Spaceship en in a.Enemies)
            {
                if (en.IsInEnergySpill)
                    c++;
            }
            return c;
        }

        void Mans(Point me)
        {
            Random r = new Random();
            double dx = -50.0 + (double)r.Next(0, 100);
            double dy = -50.0 + (double)r.Next(0, 100);
            Point next = new Point(me.X + dx, me.Y + dy);
            Accelerate(GetVector(GetAngle(me, next)));
        }
    }
}
