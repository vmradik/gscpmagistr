﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    //DontLookTheCodePls
    class Bot7 : SimpleBotBase
    {
        private GameOptions _options;
        int Target = 0;

        protected override void Init(GameOptions options)
        {
            _options = options;
        }

        protected override void Turn(Arena arena)
        {

            Point c = new Point();
            Point e = new Point();
            Point b = new Point();
            Point m = new Point();


            int enId = 0;

            double minRange = 1127;

            m.X = arena.Me.Body.Location.X + arena.Me.Body.Speed.X;
            m.Y = arena.Me.Body.Location.Y + arena.Me.Body.Speed.Y;

            if (arena.Me.GravishieldCount > 0 && arena.Me.GravishieldRemainingTime <= 0 && (arena.Me.IsInEnergySpill || arena.Tick > 3300))
                ActivateShield();

            for (int i = 0; i < arena.Enemies.Length; i++)
            {
                if (GetDistance(arena.Me.Body.Location, arena.Enemies[i].Body.Location) < minRange)
                {
                    minRange = GetDistance(arena.Me.Body.Location, arena.Enemies[i].Body.Location);
                    e.X = arena.Enemies[i].Body.Location.X + arena.Enemies[i].Body.Speed.X;
                    e.Y = arena.Enemies[i].Body.Location.Y + arena.Enemies[i].Body.Speed.Y;
                    enId = i;
                }
            }

            c.X = 1000000; c.Y = 1000000;

            if (arena.EnergySpills.Length > 0)
            {
                Target = 1; // Сфера энергии
                c.X = arena.EnergySpills[0].Body.Location.X + arena.EnergySpills[0].Body.Speed.X;
                c.Y = arena.EnergySpills[0].Body.Location.Y + arena.EnergySpills[0].Body.Speed.Y;
                for (int i = 0; i < arena.Enemies.Length; i++)
                {
                    if (arena.Enemies[i].IsInEnergySpill && !arena.Me.IsInEnergySpill && arena.Me.GravimineCount == 0 && arena.GravimineBonuses.Length > 0)
                    {
                        c.X = arena.GravimineBonuses[0].Body.Location.X + arena.GravimineBonuses[0].Body.Speed.X;
                        c.Y = arena.GravimineBonuses[0].Body.Location.Y + arena.GravimineBonuses[0].Body.Speed.Y;
                        Target = 2;

                    }
                }

            }
            else
            {
                //Сбор бонусов


                for (int i = 0; i < arena.GravimineBonuses.Length; i++)
                {
                    b.X = arena.GravimineBonuses[i].Body.Location.X + arena.GravimineBonuses[i].Body.Speed.X;
                    b.Y = arena.GravimineBonuses[i].Body.Location.Y + arena.GravimineBonuses[i].Body.Speed.Y;
                    if (GetDistance(m, b) < GetDistance(m, c))
                    {
                        Target = 2; // Gravimine
                        c = b;
                    }
                }
                for (int i = 0; i < arena.GravishieldBonuses.Length; i++)
                {
                    b.X = arena.GravishieldBonuses[i].Body.Location.X + arena.GravishieldBonuses[i].Body.Speed.X;
                    b.Y = arena.GravishieldBonuses[i].Body.Location.Y + arena.GravishieldBonuses[i].Body.Speed.Y;
                    if (GetDistance(m, b) < GetDistance(m, c))
                    {
                        Target = 3; // Shield
                        c = b;
                    }
                }
                for (int i = 0; i < arena.BoosterBonuses.Length; i++)
                {
                    b.X = arena.BoosterBonuses[i].Body.Location.X + arena.BoosterBonuses[i].Body.Speed.X;
                    b.Y = arena.BoosterBonuses[i].Body.Location.Y + arena.BoosterBonuses[i].Body.Speed.Y;
                    if (GetDistance(m, b) < GetDistance(m, c))
                    {
                        Target = 4; // Booster
                        c = b;
                    }
                }

                if (c.X == 1000000)
                {
                    Target = 5; // Center

                    c.X = _options.Arena_Size.X / 2.0;
                    c.Y = _options.Arena_Size.Y / 2.0;
                }




            }

            Accelerate(GetVector(GetAngle(arena.Me.Body.Location, c)));
            SetCannonDirection(GetAngle(arena.Me.Body.Location, e));

            if (arena.Me.GravimineCount > 0 && Target == 1 && arena.Enemies[enId].IsInEnergySpill && arena.Enemies[enId].GravishieldRemainingTime == 0 && GetDistance(m, c) < 150)
                Fire(BulletType.Gravimine);
            else
                Fire(BulletType.Plasma);
        }
    }
}
