﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    internal class CeilingCrawler : SimpleBotBase
    {

        Double lastA;
        private GameOptions opts;

        protected override void Init(GameOptions options)
        {
            opts = options;
            lastA = 30;
        }
        Arena _arena;
        protected override void Turn(Arena arena)
        {
            _arena = arena;
            Move();
        }

        protected void Move()
        {
            if (_arena.Me.Body.Location.X < 100)
                lastA = 30;
            else if (_arena.Me.Body.Location.X > opts.Arena_Size.X - 100)
                lastA = -30;


            Accelerate(new Point() { X = lastA, Y = 100 });


            Point playerLocation = new Point() { X = Double.MaxValue };
            foreach (var item in _arena.Enemies)
            {
                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, playerLocation))
                {
                    playerLocation = item.Body.Location;
                }
            }

            SetCannonDirection(GetAngle(_arena.Me.Body.Location, playerLocation));

            if (_arena.Me.PlasmaCooldown == 0)
                Fire(BulletType.Plasma);
            if (_arena.Me.GravimineCount > 0)
                Fire(BulletType.Gravimine);


        }

    }
}
