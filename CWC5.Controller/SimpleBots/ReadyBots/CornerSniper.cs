﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    internal class CornerSniper : SimpleBotBase
    {
        Arena _arena;
        private GameOptions opts;
        private void TurnCannon()
        {
            Point playerLocation = new Point() { X = -opts.Arena_Size.X, Y = -opts.Arena_Size.Y };
            foreach (var item in _arena.Enemies.Where(p => Math.Abs(p.Body.Location.X - _arena.Me.Body.Location.X) <= 200 ||
    Math.Abs(p.Body.Location.Y - _arena.Me.Body.Location.Y) <= 200 &&
    Math.Abs(p.Body.Location.X - _arena.Me.Body.Location.X) <= 400).ToList())
            {
                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, playerLocation))
                {
                    playerLocation = new Point() { X = item.Body.Location.X, Y = item.Body.Location.Y + 15 };
                }
            }
            SetCannonDirection(GetAngle(_arena.Me.Body.Location, playerLocation));
        }



        protected void Move()
        {
            TurnCannon();
            if (_arena.Me.PlasmaCooldown == 0)
                Fire(BulletType.Plasma);
            if (_arena.Me.GravimineCount > 0)
                Fire(BulletType.Gravimine);
            Point bonusLocation = new Point() { X = Double.MaxValue };
            foreach (var item in _arena.EnergySpills)
            {
                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, bonusLocation))
                {
                    bonusLocation = item.Body.Location;
                }
            }
            foreach (var item in _arena.GravimineBonuses)
            {
                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, bonusLocation))
                {
                    bonusLocation = item.Body.Location;
                }
            }
            foreach (var item in _arena.GravishieldBonuses)
            {
                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, bonusLocation))
                {
                    bonusLocation = item.Body.Location;
                }
            }
            foreach (var item in _arena.BoosterBonuses)
            {
                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, bonusLocation))
                {
                    bonusLocation = item.Body.Location;
                }
            }
			if (_arena.Me.GravishieldCount > 0 && _arena.Me.GravishieldRemainingTime == 0)
			{
				ActivateShield();
			}
			if (bonusLocation.X < Double.MaxValue)
            {
                Accelerate(new Point()
                {
                    X = (bonusLocation.X - _arena.Me.Body.Location.X) * 10,
                    Y = (bonusLocation.Y - _arena.Me.Body.Location.Y) * 10
                });
                return;
            }
            if (_arena.Me.Body.Location.X != opts.Arena_Size.X / 2 && _arena.Me.Body.Location.Y != opts.Arena_Size.Y / 2)
            {
                Accelerate(new Point()
                {
                    X = opts.Arena_Size.X / 2 - _arena.Me.Body.Location.X,
                    Y = opts.Arena_Size.Y / 2 - _arena.Me.Body.Location.Y
                });
            }
            else
            {
                Accelerate(new Point() { X = 0, Y = 150 });
            }
        }

        protected override void Init(GameOptions options)
        {
            opts = options;
            return;
        }

        protected override void Turn(Arena arena)
        {
            _arena = arena;
            Move();
        }
    }
}
