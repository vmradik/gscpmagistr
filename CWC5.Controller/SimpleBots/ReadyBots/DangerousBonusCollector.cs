﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;
using CWC5.LogicalCore;

namespace CWC5.Controller.SimpleBots
{
    #region Ext

    public static class Ext {
        public static int IsCanCollision(this PhysicalObject obj, PhysicalObject pl, int iter, double speed = -1)
    {
            var spd = (speed == -1) ? DangerousBonusCollector.GetLength(obj.Speed) : speed;
        var itemForFire = new PhysicalObject()
        {
            Radius = obj.Radius,
            Location = new Point(obj.Location.X, obj.Location.Y),
            Speed = obj.Speed
        };
        for (int i = 0; i < iter; i++)
        {

                var t = DangerousBonusCollector.distance(pl.Location.X, pl.Location.Y, itemForFire.Location.X, itemForFire.Location.Y) / spd;
            itemForFire.Location = DangerousBonusCollector.Add(itemForFire.Location, DangerousBonusCollector.Multiply(itemForFire.Speed, t));
            if (DangerousBonusCollector.CollisionDegree(itemForFire, pl) >= 0)
            {
                return i;
            }

        }
        return -1;
    }
        public static Point Normalize(this Point point)
        {
            Double mod = point.GetLength();
            if (Math.Abs(mod) < Double.Epsilon)
                return new Point(0, 0);
            return new Point(point.X / mod, point.Y / mod);
        }

        public static Double GetLength(this Point p1)
        {
            return Math.Sqrt(p1.X * p1.X + p1.Y * p1.Y);
        }
    }
    #endregion


    internal class DangerousBonusCollector : SimpleBotBase
    {



        Arena _arena;
        static Arena Arena;
        private GameOptions opts;
        #region VectorMethods
        public static Point Substract(Point p1, Point p2)
        {
            return new Point(p1.X - p2.X, p1.Y - p2.Y);
        }
        public static Double GetLength(Point p1)
        {
            return Math.Sqrt(p1.X * p1.X + p1.Y * p1.Y);
        }
        public static Point Multiply(Point p1, Double k)
        {
            return new Point(p1.X * k, p1.Y * k);
        }
        public static Point Add(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static double Distance(Point o1, Point o2)
        {
            return distance(o1.X, o1.Y, o2.X, o2.Y);
        }

        public static double distance(double beginX, double beginY, double endX, double endY)
        {
            return Math.Sqrt(Math.Pow((endX - beginX), 2) + Math.Pow((endY - beginY), 2));
        }
        public static Double CollisionDegree(PhysicalObject o1, PhysicalObject o2)
        {
            return o1.Radius + o2.Radius - GetLength(Substract(o1.Location, o2.Location));
        }
        #endregion

        private void TurnCannon()
        {
            Point playerLocation = new Point() { X = -opts.Arena_Size.X, Y = -opts.Arena_Size.Y };
            PhysicalObject fireTargetPlayer = _arena.Me.Body;

            PhysicalObject itemForFire;
            PhysicalObject save_itemForFire = _arena.Me.Body;
            var meLoc = _arena.Me.Body.Location;
            var pl = _arena.Me.Body;
            double plasmaSpeed = opts.PlasmaBullet_Speed;
            bool focusPlasma = false;
            bool focusMine = false;
            bool focusMissile = false;

            foreach (var item in _arena.GravimineBullets.Where(
                            p =>

                            p.OwnerId != _arena.Me.SpaceshipID &&
                            p.Body.IsCanCollision(pl, 100, opts.GravimineBullet_StartSpeed) > -1 &&
                            (Math.Abs(p.Body.Location.X - _arena.Me.Body.Location.X) <= 200 ||
                            Math.Abs(p.Body.Location.Y - _arena.Me.Body.Location.Y) <= 200)
                        ).ToList()
            )
            {
                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, playerLocation))
                {
                    playerLocation = new Point() { X = item.Body.Location.X, Y = item.Body.Location.Y + 20 };
                    save_itemForFire = item.Body;
                    focusMine = true;
                }
            }
            if (!focusMine)
                foreach (var item in _arena.MissileBullets.Where(
                                            p =>
                                            p.Body.IsCanCollision(pl, 100, opts.MissileBullet_StartSpeed) > -1 &&
                                            p.OwnerId != _arena.Me.SpaceshipID &&
                                            (Math.Abs(p.Body.Location.X - _arena.Me.Body.Location.X) <= 200 ||
                                            Math.Abs(p.Body.Location.Y - _arena.Me.Body.Location.Y) <= 200)
                                        ).ToList()
                        )
                {
                    if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, playerLocation))
                    {
                        playerLocation = new Point() { X = item.Body.Location.X, Y = item.Body.Location.Y + 20 };
                        save_itemForFire = item.Body;
                        focusMine = true;
                    }
                }
            if (!focusMine && !focusMissile)
                foreach (var item in _arena.PlasmaBullets.Where(
                                            p =>
                                            p.Body.IsCanCollision(pl, 100, opts.PlasmaBullet_Speed) > -1 &&
                                            p.OwnerId != _arena.Me.SpaceshipID &&
                                            (Math.Abs(p.Body.Location.X - _arena.Me.Body.Location.X) <= 200 ||
                                            Math.Abs(p.Body.Location.Y - _arena.Me.Body.Location.Y) <= 200)
                                        ).ToList()
                        )
                {
                    if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, playerLocation))
                    {
                        playerLocation = new Point() { X = item.Body.Location.X, Y = item.Body.Location.Y + 20 };
                        save_itemForFire = item.Body;
                        focusPlasma = true;
                    }
                }

            bool targetIsEnemy = false;

            var enemySpaceshipId = -1;
            if (!focusMine && !focusMissile && !focusPlasma)
                //foreach (var item in _arena.Enemies.Where(
                //                            p => Math.Abs(p.Body.Location.X - _arena.Me.Body.Location.X) <= 200 ||
                //                            Math.Abs(p.Body.Location.Y - _arena.Me.Body.Location.Y) <= 200
                //                        ).ToList()
                foreach (var item in _arena.Enemies.ToList()
                        )
                {
                    if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, playerLocation))
                    {
                        playerLocation = new Point() { X = item.Body.Location.X, Y = item.Body.Location.Y + 20 };
                        fireTargetPlayer = item.Body;
                        save_itemForFire = item.Body;
                        targetIsEnemy = true;
                        enemySpaceshipId = item.SpaceshipID;
                    }
                }


            bool justDoIt = false;
            int enemyMaxIteration = 40;
            int bulletMaxIteration = 3;

            itemForFire = save_itemForFire;
            for (int i = 0; i < (targetIsEnemy ? enemyMaxIteration : bulletMaxIteration); i++)
            {
                var t = distance(pl.Location.X, pl.Location.Y, itemForFire.Location.X, itemForFire.Location.Y) / plasmaSpeed;
                itemForFire.Location = Add(itemForFire.Location, Multiply(itemForFire.Speed, t));
                if (CollisionDegree(itemForFire, pl) >= 0)
                {
                    justDoIt = true;
                    break;
                }

            }

            if (justDoIt)     //uncoment this
            {
                var fireLoc = new Point() { X = playerLocation.X, Y = playerLocation.Y }; ;
                SetCannonDirection(GetAngle(_arena.Me.Body.Location, fireLoc));

                if (targetIsEnemy && _arena.EnergySpills.Length > 0 && CollisionDegree(fireTargetPlayer, _arena.EnergySpills.FirstOrDefault().Body) >= 0)
                {
                    if (_arena.Me.GravimineCount > 0)
                        Fire(BulletType.Gravimine);
                }
                else
                {
                    Fire(BulletType.Plasma);
                }
            }
            else
            {
                SetCannonDirection(GetAngle(_arena.Me.Body.Location, playerLocation));
                if (_arena.Me.PlasmaCooldown == 0)
                    Fire(BulletType.Plasma);
                else if (targetIsEnemy && (_arena.Me.MissileCount > 0))
                    Fire(BulletType.Missile, enemySpaceshipId);
            }




            //for (int i = 0; i < (targetIsEnemy ? enemyMaxIteration : bulletMaxIteration); i++)
            //{
            //    var t = distance(pl.Location.X, pl.Location.Y, itemForFire.Location.X, itemForFire.Location.Y) / plasmaSpeed;
            //    itemForFire.Location = Add(itemForFire.Location, Multiply(itemForFire.Speed, t));
            //    if (CollisionDegree(itemForFire, pl) >= 0)
            //    {
            //        justDoIt = true;
            //        break;
            //    }

            //}

            //var fireLoc = new Point() { X = itemForFire.Location.Y, Y = itemForFire.Location.Y };
            //if (justDoIt)
            //{
            //    SetCannonDirection(GetAngle(_arena.Me.Body.Location, fireLoc));
            //    if (_arena.Me.PlasmaCooldown == 0)
            //        Fire(BulletType.Plasma);
            //    if (_arena.Me.GravimineCount > 0)
            //        Fire(BulletType.Gravimine);
            //}
            //else // ослабляем - стреляет, даже если цели нет.
            //{

            //    SetCannonDirection(GetAngle(_arena.Me.Body.Location, fireLoc));
            //}
        }

        private int _naRayoneNetPlana = 0;

        private bool IsNeedBonus(PhysicalObject p)
        {
            foreach (var e in _arena.Enemies)
                if ((p.IsCanCollision(e.Body, 100) > -1) && (Distance(e.Body.Location, p.Location) < Distance(p.Location, _arena.Me.Body.Location)))
                    return false;
            return true;

        }



        private List<Bonus> GetDangBullets(Spaceship me)
        {

            var dangBullets = new List<Bonus>();
            try
            {
                var a = _arena.Me;
                var dangMinesLocation = _arena.GravimineBullets.Where(
                                p =>

                                p.OwnerId != me.SpaceshipID &&
                                (
                                    (p.Body.IsCanCollision(me.Body, 100, opts.GravimineBullet_StartSpeed) > -1 &&
                                    (Math.Abs(p.Body.Location.X - me.Body.Location.X) <= 200 ||
                                    Math.Abs(p.Body.Location.Y - me.Body.Location.Y) <= 200))
                                    || GetDistance(p.Body.Location, me.Body.Location)< me.Body.Radius
                                )
                            ).ToList();
                //if (dangMinesLocation.Count > 0)
                //    dangMinesLocation.RemoveAt(dangMinesLocation.Count - 1);
                var dangPlasmaLocation = _arena.PlasmaBullets.Where(
                            p =>

                            p.OwnerId != me.SpaceshipID &&
                            p.Body.IsCanCollision(me.Body, 100, opts.PlasmaBullet_Speed) > -1 &&
                            (Math.Abs(p.Body.Location.X - me.Body.Location.X) <= 200 ||
                            Math.Abs(p.Body.Location.Y - me.Body.Location.Y) <= 200)
                        ).ToList();
                var dangMissileLocation = _arena.MissileBullets.Where(
                            p =>

                            p.OwnerId != me.SpaceshipID &&
                            p.Body.IsCanCollision(me.Body, 100, opts.MissileBullet_StartSpeed) > -1 &&
                            (Math.Abs(p.Body.Location.X - me.Body.Location.X) <= 200 ||
                            Math.Abs(p.Body.Location.Y - me.Body.Location.Y) <= 200)
                        ).ToList();

                //if (dangPlasmaLocation.Count > 0)
                //    dangPlasmaLocation.RemoveAt(dangPlasmaLocation.Count - 1);
                foreach (var v in dangMinesLocation)
                {
                    dangBullets.Add(new Bonus() { name = v.GetType().ToString(), obj = v.Body.Location });
                }
                foreach (var v in dangPlasmaLocation)
                {
                    dangBullets.Add(new Bonus() { name = v.GetType().ToString(), obj = v.Body.Location });
                }
                foreach (var v in dangMissileLocation)
                {
                    dangBullets.Add(new Bonus() { name = v.GetType().ToString(), obj = v.Body.Location });
                }
            }
            catch (Exception e)
            {
                var a = e;
            }

            return dangBullets;
        }

        struct Bonus
        {
            public string name;
            public Point obj;
        }
        static double EnergySpillWeight = 100000;
        static Dictionary<string, double> BonusWeight = new Dictionary<string, double>
        {
            { typeof(EnergySpill).ToString(), EnergySpillWeight },
            { typeof(GravimineBonus).ToString(), 300 },
            { typeof(MissileBonus).ToString(), 100 },

            { typeof(FireBoosterBonus).ToString(), 150 },

            { typeof(BoosterBonus).ToString(), 500 },

            { typeof(GravishieldBonus).ToString(), 20 },

            { typeof(DashBonus).ToString(), 250 },
        };

        static Dictionary<string, double> BulletsWeight = new Dictionary<string, double>
        {
            { typeof(GravimineBullet).ToString(), 100 },

            { typeof(MissileBullet).ToString(), 50 },

            { typeof(PlasmaBullet).ToString(), 20 },
        };
        int _stupidTicks = 0;
        int _stupidTicksMax {
            get
            {
                var rez =  (Arena.EnergySpills.Count() > 0)
                   ? (int)(Arena.EnergySpills.FirstOrDefault().Body.Radius / 7)
                   : 15;
                if (rez < 10)
                    rez = 10;
                return rez;
            }
        }
        int _stupidTicksMoveInversion = 0;
        int _stupidTicksMoveInversionMax
        {
            get
            {
                var rez = (Arena.EnergySpills.Count() > 0)
                    ? (int)(Arena.EnergySpills.FirstOrDefault().Body.Radius / 6)
                    : 15;
                if (rez < 20)
                    rez = 20;
                if (rez > 30)
                    rez = 30;
                if (Arena.EnergySpills.FirstOrDefault().Body.Radius > 80)
                    rez += 25;
                return rez + 15;
            }
        }

        private Point GetMovePoint()
        {
            Arena = _arena;
            //Console.WriteLine(_arena.Me.GravishieldCount + ", " + _arena.Me.GravishieldRemainingTime);
            if (_arena.Me.GravishieldCount > 0 && _arena.Me.GravishieldRemainingTime == 0)
            {
                ActivateShield();
            }
            if (_arena.Me.DashCount > 0)
            {
                ActivateDash(true);
            }


            Point distanceReferencePoint;
            var needsBonuses = new List<Bonus>();

            distanceReferencePoint = _arena.Me.Body.Location;

            Point bonusLocation = new Point() { X = Double.MaxValue };

            var dangBullets = GetDangBullets(_arena.Me);
            EnergySpill enrg;
            bool zoneCreated = false;
            if (_arena.EnergySpills.Length > 0)
            {
                foreach (var item in _arena.EnergySpills)
                {
                    //if(IsNeedBonus(item.Body))
                    if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, bonusLocation))
                    {
                        bonusLocation = item.Body.Location;
                        needsBonuses.Add(new Bonus()
                        {
                            obj = item.Body.Location,
                            name = item.GetType().ToString(),
                        });
                        enrg = item;
                        zoneCreated = true;
                    }
                    if(_arena.Enemies.Where(p => p.IsInEnergySpill).Count() == 0)
                    {
                        _stupidTicks = 0;
                        _stupidTicksMoveInversion = 0;
                    }
                    if ((!_arena.Me.IsInEnergySpill)
                        &&(_arena.Enemies.Where(p => p.IsInEnergySpill).Count() > 0)
                        && (GetDistance(_arena.Me.Body.Location, item.Body.Location) - _arena.Me.Body.Radius - item.Body.Radius < (_arena.Me.Body.Radius / 4))){ 
                        _stupidTicks++;
                    }

                    else
                    {
                        if(_stupidTicksMoveInversion == 0)
                            _stupidTicks = 0;
                    }
                }
                
            }


           
                if (_arena.Me.BonusCount < opts.Spaceship_CargoHold)
                {
                    if (_arena.Me.GravimineCount < opts.Spaceship_GravimineCapacity)
                    {
                        foreach (var item in _arena.GravimineBonuses.Where(p => IsNeedBonus(p.Body)))
                        {
                            bonusLocation = item.Body.Location;
                            needsBonuses.Add(new Bonus()
                            {
                                obj = item.Body.Location,
                                name = item.GetType().ToString(),
                            });
                        }
                    }

                    if (_arena.Me.GravishieldCount < opts.Spaceship_GravishieldCapacity)
                    {

                        //if (!havePriorityBonus)
                        foreach (var item in _arena.GravishieldBonuses.Where(p => IsNeedBonus(p.Body)))
                        {
                            if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, bonusLocation))
                            {
                                bonusLocation = item.Body.Location;
                            }
                        }
                    }
                    if (_arena.Me.MissileCount < opts.Spaceship_MissileCapacity)
                    {
                        foreach (var item in _arena.MissileBonuses.Where(p => IsNeedBonus(p.Body)))
                        {
                            bonusLocation = item.Body.Location;
                            needsBonuses.Add(new Bonus()
                            {
                                obj = item.Body.Location,
                                name = item.GetType().ToString(),
                            });
                        }
                    }

                    if (_arena.Me.DashCount < opts.Spaceship_DashCapacity)
                    {
                        foreach (var item in _arena.DashBonuses.Where(p => IsNeedBonus(p.Body)))
                        {
                            bonusLocation = item.Body.Location;
                            needsBonuses.Add(new Bonus()
                            {
                                obj = item.Body.Location,
                                name = item.GetType().ToString(),
                            });
                        }
                    }
                }

                foreach (var item in _arena.FireBoosterBonuses.Where(p => IsNeedBonus(p.Body)))
                {
                    bonusLocation = item.Body.Location;
                    needsBonuses.Add(new Bonus()
                    {
                        obj = item.Body.Location,
                        name = item.GetType().ToString(),
                    });
                }
                foreach (var item in _arena.BoosterBonuses.Where(p => IsNeedBonus(p.Body)))
                {
                    bonusLocation = item.Body.Location;
                    needsBonuses.Add(new Bonus()
                    {
                        obj = item.Body.Location,
                        name = item.GetType().ToString(),
                    });
                }
                

               
            if (dangBullets.Count != 0 || needsBonuses.Count != 0)
            {
                bonusLocation = new Point(0, 0);
                if (needsBonuses.Count > 0)
                {



                    BonusWeight[typeof(EnergySpill).ToString()] = EnergySpillWeight;
                    if (zoneCreated)
                    {
                        if (!_arena.Me.IsInEnergySpill)
                        {
                            if (_stupidTicksMoveInversion < _stupidTicksMoveInversionMax)
                            {
                                if (_stupidTicks > _stupidTicksMax)
                                {
                                    BonusWeight[typeof(EnergySpill).ToString()] = EnergySpillWeight*(-1);
                                    _stupidTicksMoveInversion++;
                                }
                            }
                            else
                            {
                                _stupidTicks = 0;
                                _stupidTicksMoveInversion = 0;
                            }
                        }
                        else
                        {
                            _stupidTicks = 0;
                            _stupidTicksMoveInversion = 0;
                        }
                    }


                    Bonus bestBonus = needsBonuses[0];
                    var bonusWeight = BonusWeight[bestBonus.name];

                    Point vb = Substract(bestBonus.obj, _arena.Me.Body.Location).Normalize();
                    foreach (var b in needsBonuses)
                    {
                        if (BonusWeight[bestBonus.name] / Distance(bestBonus.obj, _arena.Me.Body.Location) <
                            BonusWeight[b.name] / Distance(b.obj, _arena.Me.Body.Location))
                            bestBonus = b;
                    }

                    //var v = Substract(b.obj, _arena.Me.Body.Location).Normalize();
                    vb = Multiply(vb, bonusWeight);
                    vb = Multiply(vb, Distance(_arena.Me.Body.Location, bestBonus.obj));


                    bonusLocation = Add(bonusLocation, vb);

                }
                if (!zoneCreated)
                {
                    foreach (var b in dangBullets)
                    {

                        var v = Substract(_arena.Me.Body.Location, b.obj).Normalize();
                        //var v = Substract(b.obj, _arena.Me.Body.Location).Normalize();
                        v = Multiply(v, BulletsWeight[b.name]);
                        v = Multiply(v, Distance(_arena.Me.Body.Location, b.obj));


                        bonusLocation = Add(bonusLocation, v);
                    }
                }
                bonusLocation = Multiply(bonusLocation, 100);
            }

            else
            {
               if(dangBullets.Count == 0) {
                    if (_naRayoneNetPlana < MagicRandom.Next(10, 100))
                    {
                        _naRayoneNetPlana++;
                        double maxDist = Double.MinValue;
                        for (var i = 0; i < 10; i++)
                        {
                            var X = MagicRandom.Next(0, (int)opts.Arena_Size.X);
                            var Y = MagicRandom.Next(0, (int)opts.Arena_Size.Y);

                            double minDist = Double.MaxValue;
                            var point = new Point(X, Y);
                            foreach (var e in _arena.Enemies)
                            {
                                var d = Distance(e.Body.Location, _arena.Me.Body.Location);
                                if (minDist > d)
                                    minDist = d;

                            }
                            if (minDist > maxDist)
                            {
                                bonusLocation = point;
                                if (bonusLocation.X < Double.MaxValue)
                                    bonusLocation = (new Point()
                                    {   // было умножение на 10
                                        X = (bonusLocation.X - _arena.Me.Body.Location.X) * 10,
                                        Y = (bonusLocation.Y - _arena.Me.Body.Location.Y) * 10
                                    });
                            }

                        }
                    }
                    else _naRayoneNetPlana = -1;

                }
                _naRayoneNetPlana++;

            }






            return bonusLocation;
        }

        //private Point GetMovePoint()
        //{
        //    //Console.WriteLine(_arena.Me.GravishieldCount + ", " + _arena.Me.GravishieldRemainingTime);
        //    if (_arena.Me.GravishieldCount > 0 && _arena.Me.GravishieldRemainingTime == 0)
        //    {
        //        ActivateShield();
        //    }


        //    Point distanceReferencePoint;
        //    var needsBonuses = new List<Point>();

        //    distanceReferencePoint = _arena.Me.Body.Location;

        //    Point bonusLocation = new Point() { X = Double.MaxValue };
        //    bool havePriorityBonus = false;



        //    if (_arena.EnergySpills.Length > 0)
        //    {
        //        foreach (var item in _arena.EnergySpills)
        //        {
        //            //if(IsNeedBonus(item.Body))
        //            if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, bonusLocation))
        //            {
        //                bonusLocation = item.Body.Location;
        //                havePriorityBonus = true;
        //            }

        //        }

        //        return bonusLocation;
        //    }


        //    var RunVasyaMusora = false;
        //    var dangBullets = GetDangBullets(_arena.Me);
        //    var dc = dangBullets.Count;
        //    var myCoord = _arena.Me.Body.Location;

        //    var deviationRange = _arena.Me.Body.Radius;
        //    if (dangBullets.Count > 0)
        //    {
        //        for (var i = 0; i < deviationRange * 2 * deviationRange * 2; i++)
        //        {
        //            var minX = -(int)(deviationRange + _arena.Me.Body.Location.X);
        //            if (minX < 0) minX = 0;
        //            var minY = -(int)(deviationRange + _arena.Me.Body.Location.Y);
        //            if (minY < 0) minY = 0;
        //            var X = MagicRandom.Next(minX, (int)(deviationRange + _arena.Me.Body.Location.X));
        //            var Y = MagicRandom.Next(minY, (int)(deviationRange + _arena.Me.Body.Location.Y));

        //            double minDist = Double.MaxValue;
        //            var point = new Point(X, Y);
        //            _arena.Me.Body.Location = point;
        //            if (dc > GetDangBullets(_arena.Me).Count)
        //            {
        //                bonusLocation = point;
        //                dc = GetDangBullets(_arena.Me).Count;
        //            }
        //        }
        //        _arena.Me.Body.Location = myCoord;
        //        return bonusLocation;
        //    }
        //    else {

        //        if (!havePriorityBonus)
        //            foreach (var item in _arena.MissileBonuses)
        //            {
        //                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, bonusLocation))
        //                {
        //                    bonusLocation = item.Body.Location;
        //                    havePriorityBonus = true;
        //                }
        //            }



        //        //if (!havePriorityBonus)

        //        if (_arena.Me.GravimineCount < opts.Spaceship_GravimineCapacity)
        //            foreach (var item in _arena.GravimineBonuses.Where(p => IsNeedBonus(p.Body)))
        //            {
        //                if ((GetDistance(distanceReferencePoint, item.Body.Location) < GetDistance(distanceReferencePoint, bonusLocation))
        //                && (!(true)
        //                    || (GetDistance(distanceReferencePoint, item.Body.Location) < GetDistance(distanceReferencePoint, _arena.Me.Body.Location))))
        //                {
        //                    bonusLocation = item.Body.Location;
        //                    //havePriorityBonus = true;
        //                    needsBonuses.Add(item.Body.Location);
        //                }
        //            }

        //        needsBonuses.Add(bonusLocation);


        //        foreach (var item in _arena.BoosterBonuses.Where(p => IsNeedBonus(p.Body)))
        //        {
        //            if ((GetDistance(distanceReferencePoint, item.Body.Location) < GetDistance(distanceReferencePoint, bonusLocation))
        //                && (!(true)
        //                    || (GetDistance(distanceReferencePoint, item.Body.Location) < GetDistance(distanceReferencePoint, _arena.Me.Body.Location))))
        //            {
        //                bonusLocation = item.Body.Location;
        //                havePriorityBonus = true;
        //            }
        //        }
        //        needsBonuses.Add(bonusLocation);


        //        if (_arena.EnergySpills.Length <= 0)
        //        {

        //            if (_arena.Me.GravishieldCount < opts.Spaceship_GravishieldCapacity)
        //            {

        //                //if (!havePriorityBonus)
        //                foreach (var item in _arena.GravishieldBonuses.Where(p => IsNeedBonus(p.Body)))
        //                {
        //                    if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, bonusLocation))
        //                    {
        //                        bonusLocation = item.Body.Location;
        //                        havePriorityBonus = true;
        //                    }
        //                }
        //                needsBonuses.Add(bonusLocation);
        //            }
        //            if (_arena.Me.MissileCount < opts.Spaceship_MissileCapacity)
        //                foreach (var item in _arena.GravimineBonuses.Where(p => IsNeedBonus(p.Body)))
        //                {
        //                    if ((GetDistance(distanceReferencePoint, item.Body.Location) < GetDistance(distanceReferencePoint, bonusLocation))
        //                    && (!(true)
        //                        || (GetDistance(distanceReferencePoint, item.Body.Location) < GetDistance(distanceReferencePoint, _arena.Me.Body.Location))))
        //                    {
        //                        bonusLocation = item.Body.Location;
        //                        //havePriorityBonus = true;
        //                        needsBonuses.Add(item.Body.Location);
        //                    }
        //                }

        //            foreach (var item in _arena.FireBoosterBonuses.Where(p => IsNeedBonus(p.Body)))
        //            {
        //                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, bonusLocation))
        //                {
        //                    bonusLocation = item.Body.Location;
        //                    havePriorityBonus = true;
        //                    needsBonuses.Add(item.Body.Location);
        //                }
        //            }

        //            //if (!havePriorityBonus)



        //        }

        //        if (!havePriorityBonus)
        //        {
        //            if (true)
        //                if (!RunVasyaMusora && _naRayoneNetPlana < MagicRandom.Next(10, 100))
        //                {
        //                    _naRayoneNetPlana++;
        //                    double maxDist = Double.MinValue;
        //                    for (var i = 0; i < 10; i++)
        //                    {
        //                        var X = MagicRandom.Next(0, (int)opts.Arena_Size.X);
        //                        var Y = MagicRandom.Next(0, (int)opts.Arena_Size.Y);

        //                        double minDist = Double.MaxValue;
        //                        var point = new Point(X, Y);
        //                        foreach (var e in _arena.Enemies)
        //                        {
        //                            var d = Distance(e.Body.Location, _arena.Me.Body.Location);
        //                            if (minDist > d)
        //                                minDist = d;

        //                        }
        //                        if (minDist > maxDist)
        //                            bonusLocation = point;

        //                    }
        //                }
        //                else _naRayoneNetPlana = -1;

        //            _naRayoneNetPlana++;
        //        }

        //    }


        //    foreach (var b in needsBonuses)
        //        if (Distance(bonusLocation, _arena.Me.Body.Location) > Distance(b, _arena.Me.Body.Location))
        //            bonusLocation = b;
        //    return bonusLocation;
        //}
        private void MoveTo(Point bonusLocation)
        {
            Accelerate(bonusLocation);
            //if (bonusLocation.X < Double.MaxValue)
            //    Accelerate(new Point()
            //    {   // было умножение на 10
            //        X = (bonusLocation.X - _arena.Me.Body.Location.X) * 10000,
            //        Y = (bonusLocation.Y - _arena.Me.Body.Location.Y) * 10000
            //    });
            //var myNewCoord = Add(_arena.Me.Body.Speed, (new Point()
            //{
            //    X = (bonusLocation.X - _arena.Me.Body.Location.X) * 10000,
            //    Y = (bonusLocation.Y - _arena.Me.Body.Location.Y) * 10000
            //}));
        }
        protected void Move()
        {
            MoveTo(GetMovePoint());
            TurnCannon();
        }

        protected override void Init(GameOptions options)
        {
            opts = options;
            return;
        }

        protected override void Turn(Arena arena)
        {
            _arena = arena;
            Move();
        }
    }
}
