﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    internal class GoToCenter : SimpleBotBase
    {
        private GameOptions opts;

        protected void Move()
        {
            if (_arena.Me.Body.Location.X != opts.Arena_Size.X / 2 && _arena.Me.Body.Location.Y != opts.Arena_Size.Y / 2)
            {
                Accelerate(new Point() { X = opts.Arena_Size.X / 2 - _arena.Me.Body.Location.X,
                    Y = opts.Arena_Size.Y / 2 - _arena.Me.Body.Location.Y });
            }
            else
            {
                Accelerate(new Point() { X=0, Y=150});
            }
        }

        protected override void Init(GameOptions options)
        {
            opts = options;
            return;
        }
        Arena _arena;
        protected override void Turn(Arena arena)
        {
            _arena = arena;
            Move();
        }
    }
}
