﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    internal class MadRunner : SimpleBotBase
    {
        Double lastAX = 100;
        Double lastAY = 50;

        const Double BORDER = 150.0;

        private GameOptions opts;
        private void TurnCannon()
        {
            Point playerLocation = new Point() { X = -opts.Arena_Size.X, Y = -opts.Arena_Size.Y };
            foreach (var item in _arena.Enemies.Where(p => Math.Abs(p.Body.Location.X - _arena.Me.Body.Location.X) <= 200 ||
    Math.Abs(p.Body.Location.Y - _arena.Me.Body.Location.Y) <= 200 &&
    Math.Abs(p.Body.Location.X - _arena.Me.Body.Location.X) <= 400).ToList())
            {
                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, playerLocation))
                {
                    playerLocation = new Point() { X = item.Body.Location.X, Y = item.Body.Location.Y + item.Body.Radius };
                }
            }
            SetCannonDirection(GetAngle(_arena.Me.Body.Location, playerLocation));
        }



        protected void Move()
        {
            TurnCannon();
            if (_arena.Me.PlasmaCooldown == 0)
                Fire(BulletType.Plasma);
            if (_arena.Me.GravimineCount > 0)
                Fire(BulletType.Gravimine);
            
            if (_arena.Tick < 100)
            {
                Accelerate(new Point() { X = 0, Y = 100 });
                return;
            }

            Point ac = new Point();

            if (_arena.Me.Body.Location.X < 100)
            {
                lastAX = 100;
            }
            if (opts.Arena_Size.X - _arena.Me.Body.Location.X < 100)
            {
                lastAX = -100;
            }

            if (_arena.Me.Body.Location.Y < 100)
            {
                lastAY = 150;
            }
            if (opts.Arena_Size.Y - _arena.Me.Body.Location.Y < 100)
            {
                lastAY = 0;
            }
            ac.X = lastAX;
            ac.Y = lastAY;
            Accelerate(ac);
        }

        protected override void Init(GameOptions options)
        {
            opts = options;
            return;
        }

        Arena _arena;
        protected override void Turn(Arena arena)
        {
            _arena = arena;
            Move();
        }
    }
}
