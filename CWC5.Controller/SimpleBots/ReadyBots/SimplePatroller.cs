﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    internal class SimplePatroller : SimpleBotBase
    {

        const Double BORDER = 150.0;

        Point a, b, c, d;
        Int32 mode;

        private GameOptions opts;

        Point GetAim()
        {
            if (mode == 0)
            {
                return a;
            }
            if (mode == 1)
            {
                return b;
            }
            if (mode == 2)
            {
                return c;
            }
            return d;
        }



        private void TurnCannon()
        {
            Point playerLocation = new Point() { X = -opts.Arena_Size.X, Y = -opts.Arena_Size.Y };
            foreach (var item in _arena.Enemies.Where(p => Math.Abs(p.Body.Location.X - _arena.Me.Body.Location.X) <= 200 ||
    Math.Abs(p.Body.Location.Y - _arena.Me.Body.Location.Y) <= 200 &&
    Math.Abs(p.Body.Location.X - _arena.Me.Body.Location.X) <= 400).ToList())
            {
                if (GetDistance(_arena.Me.Body.Location, item.Body.Location) < GetDistance(_arena.Me.Body.Location, playerLocation))
                {
                    playerLocation = new Point() { X = item.Body.Location.X, Y = item.Body.Location.Y + item.Body.Radius };
                }
            }
            SetCannonDirection(GetAngle(_arena.Me.Body.Location, playerLocation));
        }



        protected void Move()
        {
            TurnCannon();
            if (_arena.Me.PlasmaCooldown == 0)
                Fire(BulletType.Plasma);
            if (_arena.Me.GravimineCount > 0)
                Fire(BulletType.Gravimine);
            if (_arena.Me.GravishieldCount > 0)
                ActivateShield();

            /*if (_arena.Tick < 100)
            {
                Accelerate(new Point() { X = 0, Y = 100 });
                return;
            }*/

            Point ac = GetAim();
            double dist = GetDistance(_arena.Me.Body.Location, ac);
            Point dir = GetVector(GetAngle(_arena.Me.Body.Location, ac));
            if (dist < opts.Spaceship_Radius && GetDistance(new Point(0, 0), _arena.Me.Body.Speed) < 3)
            {
                ++mode;
                if (mode > 3) { mode = 0; }
                return;
            }
            dir.X = dir.X * dist;
            dir.Y = dir.Y * dist;
            Accelerate(dir);
        }

        protected override void Init(GameOptions options)
        {
            opts = options;
            mode = 0;
            double mult = 0.25;
            double borderX = options.Arena_Size.X * mult;
            double borderY = options.Arena_Size.Y * mult;
            a = new Point() { X = borderX, Y = borderY };
            b = new Point() { X = options.Arena_Size.X - borderX, Y = borderY };
            c = new Point() { X = options.Arena_Size.X - borderX, Y = options.Arena_Size.Y - borderY };
            d = new Point() { X = borderX, Y = options.Arena_Size.Y - borderY };
            return;
        }

        Arena _arena;
        protected override void Turn(Arena arena)
        {
            _arena = arena;
            Move();
        }
    }
}
