﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    class Bot3 : SimpleBotBase
    {
        GameOptions opt;
        Point target;
        bool targetExist;
        Point moveTarget;
        protected override void Init(GameOptions options)
        {
            opt = options;
        }

        protected override void Turn(Arena arena)
        {
            bool goingToSphere = false;
            //движение
            if (arena.Me.IsInEnergySpill)
            {
                moveTarget = arena.Me.Body.Location;
                goingToSphere = true;
                //moveTarget = arena.EnergySpills[0].Body.Location;
            }
            else if (arena.EnergySpills.Length > 0)
            {
                moveTarget = arena.EnergySpills[0].Body.Location;
                goingToSphere = true;
                //движение в пузырь
            }
            else
            {
                if (arena.GravishieldBonuses.Length > 0)
                {
                    moveTarget = arena.GravishieldBonuses[0].Body.Location;
                }
                else if (arena.GravimineBonuses.Length > 0)
                {
                    moveTarget = arena.GravimineBonuses[0].Body.Location;
                }
                // else if (arena.FireBoosterBonuses.Length > 0)
                // {
                //    moveTarget = arena.FireBoosterBonuses[0].Body.Location;
                //}
                else
                {
                    moveTarget = new Point(opt.Arena_Size.X / 2, opt.Arena_Size.Y / 2);
                }
                //движение в центр, сбор бонусов
            }
            Point axel = new Point(moveTarget.X - arena.Me.Body.Location.X, moveTarget.Y - arena.Me.Body.Location.Y);

            double axelModule = Math.Sqrt(axel.X * axel.X + axel.Y * axel.Y);
            if (axelModule <= 0.01)
            {
                axel.X = 0;
                axel.Y = 0;
            }
            else
            {
                axel.X /= axelModule;
                axel.Y /= axelModule;
            }

            // double maxSpeed = 0;
            //  double module = Math.Sqrt(arena.Me.Body.Speed.X * arena.Me.Body.Speed.X + arena.Me.Body.Speed.Y * arena.Me.Body.Speed.Y);
            // if (module > maxSpeed) maxSpeed = module;
            //   double left = maxSpeed / opt.Spaceship_MaxAcceleration;
            //  if (GetDistance(arena.Me.Body.Location, moveTarget) <= left)
            //  {
            //       axel.X = -axel.X;
            //       axel.Y = -axel.Y;
            // }

            Accelerate(axel);


            Target targ = Danger(arena);
            if (targ.state)
            {
                ActivateShield();
            }


            if (goingToSphere)
            {
                Target targ2 = FindEnemyEnergy(arena);
                if (targ2.state)
                {
                    target = targ2.location;
                    targetExist = true;
                }
                else
                {
                    Target targ3 = FindEnemy(arena);
                    if (targ3.state)
                    {
                        target = targ3.location;
                        targetExist = true;
                    }
                    else targetExist = false;
                }

            }
            else if (!targetExist)
            {
                Target targ2 = FindEnemy(arena);
                if (targ2.state)
                {
                    target = targ2.location;
                    targetExist = true;
                }
            }


            //стрельба
            if (targetExist)
            {
                double direction = GetAngle(arena.Me.Body.Location, target);
                if (Math.Abs(arena.Me.Direction - direction) <= opt.Spaceship_MaxTurnSpeed)
                {
                    if (arena.Me.GravimineCount > 0 && arena.Me.GravimineCooldown <= 0 && goingToSphere)
                    {
                        Fire(BulletType.Gravimine);
                        targetExist = false;
                    }
                    else if (arena.Me.PlasmaCooldown <= 0)
                    {
                        Fire(BulletType.Plasma);
                        targetExist = false;
                    }
                }
                else
                {
                    SetCannonDirection(direction);
                }
            }
        }

        public struct Target
        {
            public Point location;
            public bool state;
        }
        public Target Danger(Arena arena)
        {
            Point location = new Point { };
            double minDist = opt.Arena_Size.X;
            double dist;
            bool state = false;
            foreach (PlasmaBullet bull in arena.PlasmaBullets)
            {
                double alph = Math.Atan2(bull.Body.Speed.Y, bull.Body.Speed.X);
                double beta = 90 - alph;
                double x = bull.Body.Radius * Math.Cos(beta);
                double y = bull.Body.Radius * Math.Sin(beta);
                Point A1 = new Point(bull.Body.Location.X + x, bull.Body.Location.Y + y);
                Point A2 = new Point(bull.Body.Location.X - x, bull.Body.Location.Y - y);
                Point B1 = new Point(A1.X + bull.Body.Speed.X * 100, A1.Y + bull.Body.Speed.Y * 100);
                Point B2 = new Point(A2.X + bull.Body.Speed.X * 100, A2.Y + bull.Body.Speed.Y * 100);
                bool bl1 = CheckCollision(A1.X, A1.Y, B1.X, B1.Y, arena.Me.Body.Location.X, arena.Me.Body.Location.Y, arena.Me.Body.Radius);
                bool bl2 = CheckCollision(A2.X, A2.Y, B2.X, B2.Y, arena.Me.Body.Location.X, arena.Me.Body.Location.Y, arena.Me.Body.Radius);
                if (bl1 || bl2)
                {
                    dist = GetDistance(arena.Me.Body.Location, bull.Body.Location);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        location = bull.Body.Location;
                        state = true;
                    }
                }
            }
            foreach (GravimineBullet bull in arena.GravimineBullets)
            {
                double alph = Math.Atan2(bull.Body.Speed.Y, bull.Body.Speed.X);
                double beta = 90 - alph;
                double x = bull.Body.Radius * Math.Cos(beta);
                double y = bull.Body.Radius * Math.Sin(beta);
                Point A1 = new Point(bull.Body.Location.X + x, bull.Body.Location.Y + y);
                Point A2 = new Point(bull.Body.Location.X - x, bull.Body.Location.Y - y);
                Point B1 = new Point(A1.X + bull.Body.Speed.X * 100, A1.Y + bull.Body.Speed.Y * 100);
                Point B2 = new Point(A2.X + bull.Body.Speed.X * 100, A2.Y + bull.Body.Speed.Y * 100);
                bool bl1 = CheckCollision(A1.X, A1.Y, B1.X, B1.Y, arena.Me.Body.Location.X, arena.Me.Body.Location.Y, arena.Me.Body.Radius);
                bool bl2 = CheckCollision(A2.X, A2.Y, B2.X, B2.Y, arena.Me.Body.Location.X, arena.Me.Body.Location.Y, arena.Me.Body.Radius);
                if (bl1 || bl2)
                {
                    dist = GetDistance(arena.Me.Body.Location, bull.Body.Location);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        location = bull.Body.Location;
                        state = true;
                    }
                }
            }
            return new Target { location = location, state = state };
        }
        static public bool CheckCollision(double x1, double y1, double x2, double y2, double xC, double yC, double R)
        {
            x1 -= xC;
            y1 -= yC;
            x2 -= xC;
            y2 -= yC;
            double dx = x2 - x1; double dy = y2 - y1;
            double a = dx * dx + dy * dy;
            double b = 2.0 * (x1 * dx + y1 * dy);
            double c = x1 * x1 + y1 * y1 - R * R;
            if (-b < 0) return (c < 0);
            if (-b < (2.0 * a)) return ((4.0 * a * c - b * b) < 0);
            return (a + b + c < 0);
        }

        static public Target FindEnemy(Arena arena)
        {
            double maxEnergy = 0;
            Point enemy = new Point { };
            bool st = false;
            foreach (Spaceship s in arena.Enemies)
            {
                if (s.Energy >= maxEnergy)
                {
                    maxEnergy = s.Energy;
                    enemy = s.Body.Location;
                    st = true;
                }
            }
            return new Target { state = st, location = enemy };
        }

        static public Target FindEnemyEnergy(Arena arena)
        {
            Point enemy = new Point { };
            bool st = false;
            foreach (Spaceship s in arena.Enemies)
            {
                if (s.IsInEnergySpill)
                {
                    enemy = s.Body.Location;
                    st = true;
                    return new Target { state = st, location = enemy };
                }
            }
            return new Target { state = st, location = enemy };
        }
    }
}
