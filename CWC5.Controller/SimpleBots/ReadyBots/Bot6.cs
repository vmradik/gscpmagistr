﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    //KubSTU#1_v34
    class Bot6 : SimpleBotBase
    {
        private GameOptions _options;

        protected override void Init(GameOptions options)
        {
            _options = options;
        }
        

        EnergySpill? spill = null;
        protected override void Turn(Arena arena)
        {
            spill = null;

            if (arena.EnergySpills.Length > 0)
            {
                spill = arena.EnergySpills[0];
            }

            Spaceship? target = ChooseTarget(arena);

            if (target.HasValue)
            {
                double angle = GetAngle(arena.Me.Body.Location, target.Value.Body.Location);
                SetCannonDirection(angle);

                //if (Math.Abs(angle) < Math.PI * 0.2)
                {
                    if (arena.Me.PlasmaCooldown == 0)
                        Fire(BulletType.Plasma);
                    //else if (arena.Me.GravimineCount > 0)
                    //    Fire(BulletType.Gravimine);
                }
                //if (arena.Me.GravimineCount > 0)
                //    Fire(BulletType.Gravimine);
            }


            PhysicalObject? targetToMove = FindBonus(arena);


            if (spill.HasValue)
            {
                if (BotInSpill(arena, spill))
                {
                    double dist = GetDistance(spill.Value.Body.Location, arena.Me.Body.Location);

                    if (arena.Me.GravimineCount > 0)
                    {
                        double angle = GetAngle(arena.Me.Body.Location, spill.Value.Body.Location);
                        SetCannonDirection(angle);



                        if (dist < arena.Me.Body.Radius * 5)
                        {
                            if (arena.Me.GravimineCooldown == 0)
                                Fire(BulletType.Gravimine);
                        }

                        if (arena.Me.IsInEnergySpill)
                        {
                            target = ChooseTargetDist(arena);
                            if (target.HasValue)
                            {
                                angle = GetAngle(arena.Me.Body.Location, target.Value.Body.Location);
                                SetCannonDirection(angle);
                                if (arena.Me.GravimineCooldown == 0)
                                    Fire(BulletType.Gravimine);
                            }
                        }


                    }

                    if (dist < arena.Me.Body.Radius * 3)
                    {
                        targetToMove = spill.Value.Body;
                    }


                }
                else
                {
                    //Not in spill -> go to spill
                    targetToMove = spill.Value.Body;
                }
            }
            else
            {
                //No spill

            }

            if (!targetToMove.HasValue)
            {
                if (target.HasValue)
                {
                    double dist = GetDistance(target.Value.Body.Location, arena.Me.Body.Location);

                    if (dist > arena.Me.Body.Radius * 4)
                    {

                        targetToMove = target.Value.Body;
                    }
                    else
                    {
                        double angle = GetAngle(target.Value.Body.Location, arena.Me.Body.Location);
                        Accelerate(GetVector(angle + Math.PI / 3));
                    }
                }
                else
                {
                    Point A = new Point(_options.Arena_Size.X / 2, _options.Arena_Size.Y / 2);
                    Accelerate(GetVector(GetAngle(arena.Me.Body.Location, A)));
                }
            }


            if (targetToMove.HasValue)
            {
                Point vector = GetVector(GetAngle(arena.Me.Body.Location, targetToMove.Value.Location));
                vector = new Point(vector.X * 3, vector.Y * 3);

                Accelerate(vector);
            }


            if (arena.Me.GravishieldCount > 0 && arena.Me.GravishieldRemainingTime <= 0)
                ActivateShield();
        }

        private PhysicalObject? FindBonus(Arena arena)
        {
            PhysicalObject? bonus = null;

            double distMin = _options.Arena_Size.X;

            foreach (var item in arena.GravimineBonuses)
            {
                double dist = GetDistance(item.Body.Location, arena.Me.Body.Location);
                if (distMin > dist)
                {
                    distMin = dist;
                    bonus = item.Body;
                }
            }
            foreach (var item in arena.GravishieldBonuses)
            {
                double dist = GetDistance(item.Body.Location, arena.Me.Body.Location);
                if (distMin > dist)
                {
                    distMin = dist;
                    bonus = item.Body;
                }
            }

            foreach (var item in arena.BoosterBonuses)
            {
                double dist = GetDistance(item.Body.Location, arena.Me.Body.Location);
                if (distMin > dist)
                {
                    distMin = dist;
                    bonus = item.Body;
                }
            }

            return bonus;
        }

        private Spaceship? ChooseTargetDist(Arena arena)
        {
            double minDist = _options.Arena_Size.X * 10000;
            Spaceship? target = null;
            foreach (var bot in arena.Enemies)
            {
                if (bot.SpaceshipID == arena.Me.SpaceshipID)
                {
                    AddLogMessage("We are in enemies");
                }

                double dist = GetDistance(arena.Me.Body.Location, bot.Body.Location);
                if (Math.Abs(minDist) > Math.Abs(dist))
                {
                    target = bot;
                    minDist = dist;
                }
            }


            return target;
        }

        private Spaceship? ChooseTarget(Arena arena)
        {
            double minTurn = _options.Arena_Size.X * 10000;
            Spaceship? target = null;
            foreach (var bot in arena.Enemies)
            {
                if (bot.SpaceshipID == arena.Me.SpaceshipID)
                {
                    AddLogMessage("We are in enemies");
                }

                double angle = GetAngle(arena.Me.Body.Location, bot.Body.Location);
                double dist = GetDistance(arena.Me.Body.Location, bot.Body.Location);
                double turn = 2 * dist * Math.Sin(angle / 2);
                if (Math.Abs(minTurn) > Math.Abs(turn))
                {
                    target = bot;
                    minTurn = turn;
                }
            }


            return target;
        }

        private bool BotInSpill(Arena arena, EnergySpill? spill)
        {
            double rSpill = spill.Value.Body.Radius;

            bool isInSpill = false;
            foreach (var bot in arena.Enemies)
            {
                double rBot = bot.Body.Radius;

                double dist = GetDistance(bot.Body.Location, spill.Value.Body.Location);
                if (dist < (rBot + rSpill) / 2)
                {
                    isInSpill = true;
                }
            }
            return isInSpill;
        }
    }
}
