﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    class Bot1 : SimpleBotBase
    {
        GameOptions _options;
        bool _throwingMine = false;
        int _prevTgtId;
        protected override void Init(GameOptions options)
        {
            _options = options;
        }

        protected override void Turn(Arena arena)
        {
            var m = arena.Me;
            // MOVE

            bool hasDest = false;
            double minDist;
            if (m.IsInEnergySpill && arena.EnergySpills.Length > 0)
            {
                minDist = arena.EnergySpills[0].Body.Radius * 1.2;
            }
            else
            {
                minDist = 999999;
            }

            if (arena.GravimineBonuses.Length > 0 && !hasDest &&
                m.GravimineCount != _options.Spaceship_GravimineCapacity)
            {

                var loc = m.Body.Location;
                foreach (var mine in arena.GravimineBonuses)
                {
                    var dist = GetDistance(mine.Body.Location, arena.Me.Body.Location);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        loc = mine.Body.Location;
                    }
                }
                if (loc.X != m.Body.Location.Y && loc.Y != m.Body.Location.Y)
                {
                    if (arena.EnergySpills.Length > 0)
                    {
                        if (m.GravimineCount > 0)
                        {
                            var distToSpill = GetDistance(m.Body.Location, arena.EnergySpills[0].Body.Location);
                            Accelerate(GetVector(GetAngle(arena.Me.Body.Location, loc)));
                        }
                    }
                    else
                    {
                        Accelerate(GetVector(GetAngle(arena.Me.Body.Location, loc)));
                        hasDest = true;
                    }
                }
            }

            if (arena.GravishieldBonuses.Length > 0 && !hasDest && !m.IsInEnergySpill && m.GravishieldCount == 0)
            {
                minDist = 999999;
                foreach (var sh in arena.GravishieldBonuses)
                {
                    var dist = GetDistance(sh.Body.Location, m.Body.Location);
                    var loc = m.Body.Location;
                    if (arena.EnergySpills.Length > 0)
                    {
                        dist += GetDistance(sh.Body.Location, arena.EnergySpills[0].Body.Location);
                    }
                    if (dist < minDist)
                    {
                        minDist = dist;
                        loc = sh.Body.Location;
                    }
                    if (loc.X != m.Body.Location.Y && loc.Y != m.Body.Location.Y)
                    {
                        Accelerate(GetVector(GetAngle(arena.Me.Body.Location, loc)));
                        hasDest = true;
                    }
                }
            }



            if (arena.EnergySpills.Length > 0 && !hasDest)
            {
                Accelerate(GetVector(GetAngle(m.Body.Location, arena.EnergySpills[0].Body.Location)));
                hasDest = true;
            }

            if (!hasDest)
            {
                var rnd = new Random();
                var X = rnd.NextDouble() * _options.Arena_Size.X;
                var Y = rnd.NextDouble() * _options.Arena_Size.Y;
                Accelerate(new Point(X, Y));
            }

            // SHIELD

            minDist = _options.Spaceship_Radius * 4;
            foreach (var mine in arena.GravimineBullets)
            {
                var dist = GetDistance(m.Body.Location, mine.Body.Location);
                if (dist < minDist)
                {
                    minDist = dist;
                }
            }
            ActivateShield();

            // FIRE
            
            minDist = 999999;
            var maxNRG = arena.Enemies[0].Energy;
            Spaceship enemyToShoot = m;
            double angleToEnemy = -1;

            enemyToShoot = m;
            minDist = 99999;
            foreach (var e in arena.Enemies)
            {
                var dist = GetDistance(m.Body.Location, e.Body.Location);
                if (dist < minDist)
                {
                    enemyToShoot = e;
                    minDist = dist;
                }
            }

            if (enemyToShoot.SpaceshipID != m.SpaceshipID)
            {
                angleToEnemy = GetAngle(m.Body.Location, enemyToShoot.Body.Location);
                SetCannonDirection(angleToEnemy);
                _throwingMine = true;
            }

            if (!_throwingMine)
            {
                minDist = _options.Spaceship_Radius * 4;
                foreach (var b in arena.PlasmaBullets)
                {
                    if (GetDistance(b.Body.Location, m.Body.Location) < minDist)
                    {
                        Accelerate(GetVector(GetAngle(m.Body.Location, b.Body.Location) + (Math.PI / 2)));
                        hasDest = true;
                        break;
                    }
                }
                minDist = _options.Spaceship_Radius * 4;
                foreach (var b in arena.GravimineBullets)
                {
                    if (GetDistance(b.Body.Location, m.Body.Location) < minDist)
                    {
                        Accelerate(GetVector(GetAngle(m.Body.Location, b.Body.Location) + (Math.PI / 2)));
                        hasDest = true;
                        break;
                    }
                }
            }

            if (!_throwingMine)
            {
                maxNRG = arena.Enemies[0].Energy;
                enemyToShoot = arena.Enemies[0];
                foreach (var enemy in arena.Enemies)
                {
                    if (maxNRG < enemy.Energy && !enemy.IsInEnergySpill)
                    {
                        enemyToShoot = enemy;
                        maxNRG = enemy.Energy;
                    }
                }

                angleToEnemy = GetAngle(m.Body.Location, enemyToShoot.Body.Location);
                SetCannonDirection(angleToEnemy);

                if (arena.Me.PlasmaCooldown == 0 &&
                    (angleToEnemy - arena.Me.Direction) < _options.Spaceship_MaxTurnSpeed)
                {
                    Fire(BulletType.Plasma);
                }
            }
            else
            {

                if (enemyToShoot.SpaceshipID != m.SpaceshipID &&
                    enemyToShoot.IsInEnergySpill &&
                    GetDistance(m.Body.Location, enemyToShoot.Body.Location) < _options.Spaceship_Radius * 2.5 &&
                    (angleToEnemy - arena.Me.Direction) < _options.Spaceship_MaxTurnSpeed &&
                    enemyToShoot.GravishieldRemainingTime == 0)
                {
                    Fire(BulletType.Gravimine);
                    _prevTgtId = enemyToShoot.SpaceshipID;
                }
                else
                {
                    _throwingMine = false;
                }

                if (enemyToShoot.GravishieldRemainingTime > 0 &&
                    (angleToEnemy - arena.Me.Direction) < _options.Spaceship_MaxTurnSpeed)
                {
                    Fire(BulletType.Plasma);
                }

                foreach (var e in arena.Enemies)
                {
                    if (_prevTgtId == e.SpaceshipID && !e.IsInEnergySpill)
                    {
                        Fire(BulletType.Plasma);
                    }
                }
            }
        }
    }
}
