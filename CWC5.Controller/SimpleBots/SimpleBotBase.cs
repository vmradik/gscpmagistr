﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;
using CWC5.LogicalCore.Controller;

namespace CWC5.Controller.SimpleBots
{
    internal abstract class SimpleBotBase : IBot
    {
        protected static Random Random = new Random();

        #region internal methods

        internal void DoInit(GameOptions options)
        {
            _maxMessagesSize = options.Bot_MessagesMaxTotalLength;
            Init(options);
        }

        internal TurnResult GetNextTurn(Arena arena)
        {
            _acceleration = new Point() { X = 0.0, Y = 0.0 };
            _bulletType = BulletType.None;
            _messages = new List<string>();
            _cannonDirection = arena.Me.Direction;
            _activateShield = false;
            _activateDash = false;
            _targetId = -1;
            _dashDirection = arena.Me.Direction;


            Turn(arena);

            return new TurnResult()
            {
                TargetID =_targetId,
                Acceleration = _acceleration,
                Bullet = _bulletType,// TODO
                CannonDirection = _cannonDirection,
                Messages = _messages.ToArray(),
                ActivateShield = _activateShield,
                ActivateDash = _activateDash,
                DashDirection = _dashDirection,
            };
        }

        #endregion
        
        #region private and intrnal properties and fields

        private Int32 _totalMessagesSize = 0;
        private Int32 _maxMessagesSize = 0;

        private Point _acceleration;
        private Double _cannonDirection;
        private BulletType _bulletType;
        private Boolean _activateDash;
        private Double _dashDirection;
        private List<String> _messages;
        private Boolean _activateShield;

        private Int32 _targetId;

        #endregion

        #region protected methods

        protected abstract void Init(GameOptions options);
        protected abstract void Turn(Arena arena);

        protected Boolean AddLogMessage(String message)
        {
            if (_totalMessagesSize + message.Length <= _maxMessagesSize)
            {
                _totalMessagesSize += message.Length;
                _messages.Add(message);
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void Accelerate(Point acceleration)
        {
            _acceleration = acceleration;
        }

        protected void SetCannonDirection(Double newAngle)
        { 
            _cannonDirection = newAngle;
        }

        protected void Fire(BulletType type)
        {
            _bulletType = type;
        }
        

        protected void Fire(BulletType type, int targetId)
        {
            _bulletType = type;
            _targetId = targetId;
        }

        protected void ActivateShield()
        {
            _activateShield = true;
        }

        protected void ActivateDash(bool activate)
        {
            _activateDash = activate;
        }

        protected void ActivateDash(bool activate, double direction)
        {
            _activateDash = activate;
            _dashDirection = direction;
        }
        #endregion

        #region Helper

        protected Double GetAngle(Point from, Point to)
        {
            return GetAngle(new Point(to.X - from.X, to.Y - from.Y));
        }

        protected Double GetAngle(Point vector)
        {
            return Math.Atan2(vector.Y, vector.X);
        }

        protected Point GetVector(Double angle)
        {
            return new Point(Math.Cos(angle), Math.Sin(angle));
        }

        protected Double GetDistance(Point p1, Point p2)
        {
            return Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) + (p1.Y - p2.Y) * (p1.Y - p2.Y));
        }

        #endregion
        
        bool IBot.Init(GameOptions options)
        {
            DoInit(options);
            return true;
        }

        TurnResult? IBot.GetTurnResult(Arena arena)
        {
            return GetNextTurn(arena);
        }
    }
}
