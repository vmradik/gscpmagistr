﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    internal class SecondBot : SimpleBotBase
    {
        protected override void Turn(Arena arena)
        {
            Point bonusLocation = new Point() { X= Double.MaxValue };
            foreach (var item in arena.EnergySpills)
            {
                if (GetDistance(arena.Me.Body.Location, item.Body.Location) < GetDistance(arena.Me.Body.Location, bonusLocation))
                {
                    bonusLocation = item.Body.Location;
                }
            }
            foreach (var item in arena.GravimineBonuses)
            {
                if (GetDistance(arena.Me.Body.Location, item.Body.Location) < GetDistance(arena.Me.Body.Location, bonusLocation))
                {
                    bonusLocation = item.Body.Location;
                }
            }
            if (bonusLocation.X < Double.MaxValue)
            Accelerate(new Point()
            {
                X = (bonusLocation.X - arena.Me.Body.Location.X) * 10,
                Y = (bonusLocation.Y - arena.Me.Body.Location.Y) * 10
            });


            Point playerLocation = new Point() { X = Double.MaxValue };
            foreach (var item in arena.Enemies)
            {
                if (GetDistance(arena.Me.Body.Location, item.Body.Location) < GetDistance(arena.Me.Body.Location, playerLocation))
                {
                    playerLocation = item.Body.Location;
                }
            }

            SetCannonDirection(GetAngle(arena.Me.Body.Location, playerLocation));

            if (arena.Me.PlasmaCooldown == 0)
                Fire(BulletType.Plasma);
            if (arena.Me.GravimineCount > 0)
                Fire(BulletType.Gravimine);


        }

        protected override void Init(GameOptions options)
        {

        }
    }
}
