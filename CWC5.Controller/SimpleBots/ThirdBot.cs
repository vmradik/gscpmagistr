﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.DTOModel;

namespace CWC5.Controller.SimpleBots
{
    internal class ThirdBot : SimpleBotBase
    {

        private GameOptions _options;

        protected override void Init(GameOptions options)
        {
            _options = options;
        }
        Double lastA = 30;

        protected override void Turn(Arena arena)
        {
            if (arena.Me.Body.Location.X < 100)
                lastA = 30;
            else if (arena.Me.Body.Location.X > _options.Arena_Size.X - 100)
                lastA = -30;


            Accelerate(new Point() { X = lastA, Y = 100 });


            Point playerLocation = new Point() { X = Double.MaxValue };
            foreach (var item in arena.Enemies)
            {
                if (GetDistance(arena.Me.Body.Location, item.Body.Location) < GetDistance(arena.Me.Body.Location, playerLocation))
                {
                    playerLocation = item.Body.Location;
                }
            }

            SetCannonDirection(GetAngle(arena.Me.Body.Location, playerLocation));

            if (arena.Me.PlasmaCooldown == 0)
            Fire(BulletType.Plasma);
            if (arena.Me.GravimineCount > 0)
                Fire(BulletType.Gravimine);


        }
    }
}
