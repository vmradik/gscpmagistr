﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CWC5.LogicalCore.Controller;

namespace CWC5.Controller.SimpleBots
{
    internal static class SimpleBotFactory
    {
        static SimpleBotFactory()
        {
        }

        public static IBot GetSimpleBot(String embeddedId)
        {
            switch (embeddedId)
            {
                case "Cleptoman":
                    {
                        return new SimpleBots.DangerousBonusCollector();
                    }
                case "Visun":
                    {
                        return new SimpleBots.DangerousCenterHolder();
                    }
                case "Crazy":
                    {
                        return new SimpleBots.MadRunner();
                    }
                case "ConrnerSniper":
                    {
                        return new SimpleBots.MadCenterRunner();
                    }
                case "SimplePatroller":
                    {
                        return new SimpleBots.SimplePatroller();
                    }
                case "CircleRunner":
                    {
                        return new SimpleBots.CircleRunner();
                    }
                case "Bot1":
                    {
                        return new SimpleBots.Bot1();
                    }
                case "Bot2":
                    {
                        return new SimpleBots.Bot2();
                    }
                case "Bot3":
                    {
                        return new SimpleBots.Bot3();
                    }
                case "Bot5":
                    {
                        return new SimpleBots.Bot5();
                    }
                case "Bot6":
                    {
                        return new SimpleBots.Bot6();
                    }
                case "Bot7":
                    {
                        return new SimpleBots.Bot7();
                    }
                default:
                    {
                        return null;
                    }
            }

        }

    }
}
