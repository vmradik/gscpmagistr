﻿using CWC5.DTOModel;
using CWC5.LogicalCore.Controller;
using Grpc.Core;
using Gscp.Grpc_Definitions;
using GscpGrpc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using grpc = global::Grpc.Core;

namespace CWC5.Controller
{
    class Grpc_Bot : GscpStrategyRunner.GscpStrategyRunnerClient, IBot, IDisposable
    {

        public Grpc_Bot(String filePath, ExecutableType type, TimeSpan receiveTimeout, Boolean saveMessages)
        {
            _process = new ExeBotRunner(filePath);
            _receiveTimeout = receiveTimeout;
            _saveMessages = saveMessages;
        }

        public void Dispose()
        {
            Stop();
        }

        GscpGrpc.Point fromDto(DTOModel.Point point)
        {
            return new GscpGrpc.Point()
            {
                X = point.X,
                Y = point.Y
            };
        }

        DTOModel.Point toDto(GscpGrpc.Point point)
        {
            return new DTOModel.Point()
            {
                X = point.X,
                Y = point.Y
            };
        }

        GscpGrpc.PhysicalObject fromDto(DTOModel.PhysicalObject obj)
        {
            return new GscpGrpc.PhysicalObject()
            {
                Radius = obj.Radius,
                Location = fromDto(obj.Location),
                Speed = fromDto(obj.Speed)
            };
        }

        GscpGrpc.Spaceship fromDto(DTOModel.Spaceship s)
        {
            return new GscpGrpc.Spaceship()
            {
                Body = fromDto(s.Body),
                SpaceshipID = s.SpaceshipID,
                Energy = s.Energy,
                Direction = s.Direction,
                IsInEnergySpill = s.IsInEnergySpill,
                PlasmaCooldown = s.PlasmaCooldown,
                GravimineCooldown = s.GravimineCooldown,
                GravimineCount = s.GravimineCount,
                GravishieldCount = s.GravishieldCount,
                GravishieldStrength = s.GravishieldStrength,
                DashCount = s.DashCount,
                MissileCooldown = s.MissileCooldown,
                MissileCount = s.MissileCount,
                GravishieldRemainingTime = s.GravishieldRemainingTime,
                BoosterRemainingTime = s.BoosterRemainingTime,
                FireBoosterRemainingTime = s.FireBoosterRemainingTime
            };
        }

        public DTOModel.TurnResult? GetTurnResult(DTOModel.Arena arena)
        {
            if (!_isAlive)
            {
                return null;
            }
            DTOModel.TurnResult result = new DTOModel.TurnResult();
            var a = new GscpGrpc.Arena()
            {
                Tick = arena.Tick,
                Me = fromDto(arena.Me),

            };
            a.EnergySpills.AddRange(arena.EnergySpills.Select(spill => new GscpGrpc.EnergySpill()
            {
                RemainingTime = spill.RemainingTime,
                Body = fromDto(spill.Body),
            }));
            a.Enemies.AddRange(arena.Enemies.Select(enemy => fromDto(enemy)));
            a.GravimineBonuses.AddRange(arena.GravimineBonuses.Select(gb => new GscpGrpc.GravimineBonus()
            {
                Body = fromDto(gb.Body),
            }));
            a.MissileBonuses.AddRange(arena.MissileBonuses.Select(gb => new GscpGrpc.MissileBonus()
            {
                Body = fromDto(gb.Body),
            }));
            a.BoosterBonuses.AddRange(arena.BoosterBonuses.Select(gb => new GscpGrpc.BoosterBonus()
            {
                Body = fromDto(gb.Body),
            }));
            a.FireBoosterBonuses.AddRange(arena.FireBoosterBonuses.Select(gb => new GscpGrpc.FireBoosterBonus()
            {
                Body = fromDto(gb.Body),
            }));
            a.GravishieldBonuses.AddRange(arena.GravishieldBonuses.Select(gb => new GscpGrpc.GravishieldBonus()
            {
                Body = fromDto(gb.Body),
            }));
            a.PlasmaBullets.AddRange(arena.PlasmaBullets.Select(gb => new GscpGrpc.PlasmaBullet()
            {
                Body = fromDto(gb.Body),
                OwnerId = gb.OwnerId
            }));
            a.GravimineBullets.AddRange(arena.GravimineBullets.Select(gb => new GscpGrpc.GravimineBullet()
            {
                Body = fromDto(gb.Body),
                OwnerId = gb.OwnerId
            }));
            a.MissileBullets.AddRange(arena.MissileBullets.Select(gb => new GscpGrpc.MissileBullet()
            {
                Body = fromDto(gb.Body),
                OwnerId = gb.OwnerId,
                TargetId = gb.TargetId
            }));

            try
            {
                var res = client.GetTurnResult(a, deadline: Grpc_tools.get_grpc_deadline());
                result = new DTOModel.TurnResult()
                {
                    Acceleration = toDto(res.Acceleration),
                    Bullet = (DTOModel.BulletType)Enum.Parse(typeof(DTOModel.BulletType), res.Bullet.ToString()),
                    CannonDirection = res.CannonDirection,
                    ActivateDash = res.ActivateDash,
                    DashDirection = res.DashDirection,
                    TargetID = res.TargetID,
                    ActivateShield = res.ActivateShield,
                    Messages = res.Messages.ToArray()
                };
                return result;
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString()); Trace.Flush();
                _isAlive = false;
                return null;
            }
        }

		public void Start(string ip, int port)
		{
            const int TicksPerMicrosecond = 10;
            _isAlive = true;
            try
            {
                Environment.SetEnvironmentVariable("NO_PROXY", "localhost");
                //port = 52001;
                _process.Start_with_host(ip, port);
                channel = new Channel(ip, port, ChannelCredentials.Insecure);
                client = new GscpStrategyRunner.GscpStrategyRunnerClient(channel);
                //_process.Start(0);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString()); Trace.Flush();
                _isAlive = false;
            }
        }
        public void Stop()
        {
            Console.WriteLine("received request to exit");
            try
            {
                client.Exit(new ExitRequest());
            }
            catch
            {
            }
            try
            {
                channel.ShutdownAsync();
            }
            catch
            {
            }

            _isAlive = false;

            if (_process != null)
            {
                _process.Dispose();
                _process = null;
            }
        }

        public bool Init(DTOModel.GameOptions options)
        {
            if (!_isAlive)
            {
                return false;
            }
            _maxMessagesLength = options.Bot_MessagesMaxTotalLength;
            //using (var call = invoker.AsyncDuplexStreamingCall(Descriptors.Method, null, new CallOptions { }))
            //{

            //}
            //WriteSignature("INIT");
            //_writer.Write(options);
            //CheckSignature();
            var opts = new GscpGrpc.GameOptions()
            {
                ArenaSize = new GscpGrpc.Point()
                {
                    X = options.Arena_Size.X,
                    Y = options.Arena_Size.Y
                },
                ArenaFightDuration = options.Arena_FightDuration,
                EnergySpillPower = options.EnergySpill_Power,
                EnergySpillRadiusMin = options.EnergySpill_RadiusMin,
                EnergySpillRadiusMax = options.EnergySpill_RadiusMax,
                EnergySpillLifetimeMin = options.EnergySpill_LifetimeMin,
                EnergySpillLifetimeMax = options.EnergySpill_LifetimeMax,
                EnergySpillPauseMin = options.EnergySpill_PauseMin,
                EnergySpillPauseMax = options.EnergySpill_PauseMax,
                EnergySpillSpaceshipResistance = options.EnergySpill_SpaceshipResistance,
                SpaceshipRadius = options.Spaceship_Radius,
                SpaceshipInitEnergy = options.Spaceship_InitEnergy,
                SpaceshipMaxAcceleration = options.Spaceship_MaxAcceleration,
                SpaceshipMaxTurnSpeed = options.Spaceship_MaxTurnSpeed,
                SpaceshipResistance = options.Spaceship_Resistance,
                SpaceshipPlasmaInitCooldown = options.Spaceship_PlasmaInitCooldown,
                SpaceshipCargoHold = options.Spaceship_CargoHold,
                SpaceshipGravimineInitCooldown = options.Spaceship_GravimineInitCooldown,
                SpaceshipGravimineCapacity = options.Spaceship_GravimineCapacity,
                SpaceshipMissileInitCooldown = options.Spaceship_MissileInitCooldown,
                SpaceshipMissileCapacity = options.Spaceship_MissileCapacity,
                SpaceshipGravishieldCapacity = options.Spaceship_GravishieldCapacity,
                SpaceshipDashCapacity = options.Spaceship_DashCapacity,
                PlasmaBulletRadius = options.PlasmaBullet_Radius,
                PlasmaBulletEnergyPower = options.PlasmaBullet_EnergyPower,
                PlasmaBulletSpeed = options.PlasmaBullet_Speed,
                PlasmaBulletReward = options.PlasmaBullet_Reward,
                GravimineBulletRadius = options.GravimineBullet_Radius,
                GravimineBulletEnergyPower = options.GravimineBullet_EnergyPower,
                GravimineBulletPushPower = options.GravimineBullet_PushPower,
                GravimineBulletStartSpeed = options.GravimineBullet_StartSpeed,
                GravimineBulletMinSpeed = options.GravimineBullet_MinSpeed,
                GravimineBulletResistance = options.GravimineBullet_Resistance,
                GravimineBulletReward = options.GravimineBullet_Reward,
                MissileBulletRadius = options.MissileBullet_Radius,
                MissileBulletEnergyPower = options.MissileBullet_EnergyPower,
                MissileBulletPushPower = options.MissileBullet_PushPower,
                MissileBulletStartSpeed = options.MissileBullet_StartSpeed,
                MissileBulletMinSpeed = options.MissileBullet_MinSpeed,
                MissileBulletResistance = options.MissileBullet_Resistance,
                MissileBulletReward = options.MissileBullet_Reward,
                GravimineBonusRadius = options.GravimineBonus_Radius,
                MissileBonusRadius = options.MissileBonus_Radius,
                BoosterBonusRadius = options.BoosterBonus_Radius,
                BoosterBonusMultiplier = options.BoosterBonus_Multiplier,
                BoosterBonusDuration = options.BoosterBonus_Duration,
                FireBoosterBonusRadius = options.FireBoosterBonus_Radius,
                FireBoosterBonusNewCooldown = options.FireBoosterBonus_NewCooldown,
                FireBoosterBonusDuration = options.FireBoosterBonus_Duration,
                GravishieldBonusRadius = options.GravishieldBonus_Radius,
                GravishieldBonusInitStrength = options.GravishieldBonus_InitStrength,
                GravishieldBonusDuration = options.GravishieldBonus_Duration,
                DashBonusRadius = options.DashBonus_Radius,
                DashBonusBoost = options.DashBonus_Boost,
                BotMessagesMaxTotalLength = options.Bot_MessagesMaxTotalLength
            };
            try
            {
                var res = client.Init(opts, deadline: Grpc_tools.get_grpc_deadline());
                if (!"ok".Equals(res.Status))
                {
                    Console.WriteLine("error");
                    _isAlive = false;
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString()); Trace.Flush();
                _isAlive = false;
                return false;
            }
        }

        protected Channel channel;
        protected GscpStrategyRunner.GscpStrategyRunnerClient client;
        protected ExeBotRunner _process;

        protected readonly TimeSpan _connectTimeout = TimeSpan.FromSeconds(10);
        protected readonly TimeSpan _receiveTimeout = TimeSpan.FromSeconds(3);
        protected readonly bool _saveMessages;
        protected int _maxMessagesLength = 0;
        protected bool _isAlive;

        static ManualResetEvent _timeoutEvent = new ManualResetEvent(false);

        protected class ExeBotRunner
        {
            private Process _process;
            private string _filename;

            protected ProcessStartInfo Start(string filename, int port)
            {
                return new ProcessStartInfo
                {
                    FileName = filename,
                    Arguments = port.ToString(),
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                };
            }
            public ExeBotRunner(String filename)
            {
                _filename = filename;
            }

            public void Start(int port)
            {
                _process = Process.Start(Start(_filename, port));
            }

            public void Start_with_host(string host, int port)
            {
                _process = Process.Start(new ProcessStartInfo
                {
                    FileName = _filename,
                    Arguments = $"{host} {port}",
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                });
            }

            public virtual void Dispose()
            {
                if (_process != null)
                {
                    try
                    {
                        if (!_process.WaitForExit(2000))
                        {
                            try
                            {
                                if (!_process.HasExited)
                                {
                                    _process.Kill();
                                    _process.WaitForExit(1000);
                                }
                            }
                            catch (InvalidOperationException)
                            {
                            }
                        }
                    }
                    catch (InvalidOperationException) // if process is already terminated
                    {
                    }

                    _process.Close();
                    _process = null;
                }
            }
        }
    }
}
