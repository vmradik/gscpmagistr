﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using CWC5.LogicalCore.Controller;

namespace CWC5.Controller
{
	class ExternalBot : IBot, IDisposable
	{
		#region Static members
				
		private static IBotProcess GetProcess(String filePath, ExecutableType type)
		{
			if (type == ExecutableType.Exe)
				return new ExeBotRunner(filePath);
			else if (type == ExecutableType.Java)
				return new JavaBotRunner(filePath);
			else
				throw new ArgumentException("Unknown bot type.", "botData");
			//return new MockBotRunner(botData);
		}
		
		static ManualResetEvent _timeoutEvent = new ManualResetEvent(false);

		/// <remarks>NOT reenterant!</remarks>
		protected static bool ExecuteWithTimeout(Action action, TimeSpan timeout)
		{
			bool ok = false;
			_timeoutEvent.Reset();
			ThreadPool.QueueUserWorkItem(x =>
			{
				try
				{
					action();
					ok = true;
				}
				catch (Exception ex)
				{
					Trace.WriteLine(ex.ToString());
					Trace.Flush();
				}
				_timeoutEvent.Set();
			}, null);

			return _timeoutEvent.WaitOne((int)timeout.TotalMilliseconds) && ok;
		}
		
		#endregion


		protected readonly TimeSpan _connectTimeout = TimeSpan.FromSeconds(10);
		protected readonly TimeSpan _receiveTimeout = TimeSpan.FromSeconds(3);
		protected readonly bool _saveMessages;
		protected int _maxMessagesLength = 0;
		protected bool _isAlive;

		protected IBotProcess _process;
		protected Socket _client;
		protected BinaryReader _reader;
		protected BinaryWriter _writer;
		

		public ExternalBot(String filePath, ExecutableType type, TimeSpan receiveTimeout, Boolean saveMessages)
		{
			_process = GetProcess(filePath, type);
			_receiveTimeout = receiveTimeout;
			_saveMessages = saveMessages;
		}

		#region Controls

		public void Start()
		{
			const int TicksPerMicrosecond = 10;
			_isAlive = true;

			try
			{

				using (var listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
				{
					listener.Bind(new IPEndPoint(IPAddress.Loopback, 0));
					listener.Listen(1);
					_process.Start(((IPEndPoint)listener.LocalEndPoint).Port);
					if (!listener.Poll((int)(_connectTimeout.Ticks / TicksPerMicrosecond), SelectMode.SelectRead))
					{
						listener.Close(0);
						StopInternal();
						throw new TimeoutException("Can't connect to process.");
					}
					_client = listener.Accept();
					var ns = new NetworkStream(_client);
					_reader = new BinaryReader(ns);
					_writer = new BinaryWriter(ns);
				}
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex.ToString()); Trace.Flush();
				_isAlive = false;
			}
		}

		public void Stop()
		{
			try
			{
				WriteSignature("EXIT");
				_client.Close((int)_connectTimeout.TotalMilliseconds);
				_client = null;
			}
			catch
			{
			}
			StopInternal();
		}

		private void StopInternal()
		{
			_isAlive = false;
			if (_client != null)
			{
				_client.Close(0);
				_client = null;
				_reader = null;
				_writer = null;
			}

			if (_process != null)
			{
				_process.Dispose();
				_process = null;
			}
		}
		
		public void Dispose()
		{
			StopInternal();
		}

		#endregion

		#region Signatures

		private void WriteSignature(string signature)
		{
			_writer.Write(Encoding.ASCII.GetBytes(signature));
		}

		private void CheckSignature()
		{
			var signature = Encoding.ASCII.GetString(_reader.ReadBytes(4));
			if (signature != "DONE")
				throw new FormatException("Invalid bot response.");
		}

		#endregion

		#region IBot Members

		public bool Init(DTOModel.GameOptions options)
		{
			if (!_isAlive)
				return false;
            bool ok = ExecuteWithTimeout(() =>
				{
					_maxMessagesLength = options.Bot_MessagesMaxTotalLength;
					WriteSignature("INIT");
					_writer.Write(options);
					CheckSignature();
				}, _receiveTimeout);
			if (!ok)
				StopInternal();
			return ok;
		}

		public DTOModel.TurnResult? GetTurnResult(DTOModel.Arena arena)
		{
			if (!_isAlive)
				return null;
			DTOModel.TurnResult result = new DTOModel.TurnResult();
			if (!ExecuteWithTimeout(
					() =>
					{
						WriteSignature("TURN");
						_writer.Write(arena);
						CheckSignature();
						result = _reader.ReadTurnResult(_maxMessagesLength);
						if (!_saveMessages)
							result.Messages = new string[0];
					},
					_receiveTimeout))
			{
				StopInternal();
				return null;
			}
			return result;
		}

		#endregion

		#region Processes

		protected interface IBotProcess : IDisposable
		{
			void Start(int port);
		}

		private abstract class BotProcessBase : IBotProcess
		{
			private Process _process;
			private string _filename;

			protected BotProcessBase(String filename)
			{
				_filename = filename;
			}
			
			public void Start(int port)
			{
				_process = Process.Start(Start(_filename, port));
			}

			protected abstract ProcessStartInfo Start(string filename, int port);

			public virtual void Dispose()
			{
				if (_process != null)
				{
					try
					{
						if (!_process.WaitForExit(1000))
						{
							try
							{
								_process.Kill();
								_process.WaitForExit(1000);
							}
							catch (InvalidOperationException)
							{
							}
						}
					}
					catch (InvalidOperationException) // if process is already terminated
					{
					}

					_process.Close();
					_process = null;
				}

				//if (_filename != null)
				//{
				//	try
				//	{
				//		File.Delete(_filename);
				//	}
				//	catch
				//	{
				//	}
				//	_filename = null;
				//}
			}
		}

		private class ExeBotRunner : BotProcessBase
		{
			public ExeBotRunner(String filename)
				: base(filename)
			{

			}

			protected override ProcessStartInfo Start(string filename, int port)
			{
				return new ProcessStartInfo
				{
					FileName = filename,
					Arguments = port.ToString(),
					UseShellExecute = false,
					WindowStyle = ProcessWindowStyle.Hidden,
				};
			}
		}

		private class JavaBotRunner : BotProcessBase
		{
			public JavaBotRunner(String filename)
				: base(filename)
			{

			}

			protected override ProcessStartInfo Start(string filename, int port)
			{
				return new ProcessStartInfo
				{
					FileName = "javaw",
					Arguments = string.Format("-jar {0} {1}", filename, port),
					UseShellExecute = false,
					WindowStyle = ProcessWindowStyle.Hidden,
				};
			}
		}

		private class MockBotRunner : BotProcessBase
		{
			public MockBotRunner(String filename)
				: base(filename)
			{
			}

			protected override ProcessStartInfo Start(string filename, int port)
			{
				return new ProcessStartInfo
				{
					FileName = "telnet",
					Arguments = "localhost " + port,
					UseShellExecute = false
				};
			}
		}

		#endregion

	}
}
