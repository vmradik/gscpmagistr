﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.Controller
{
    public class ModelingResult
    {
        public ModelingResult()
        {
            Exception = null;
        }
        public ModelingResult(Exception exception)
        {
            Exception = exception;
        }

        public ModelingResult(Byte[] fightHistory, Score score)
        {
            FightHistory = fightHistory;
            Score = score;
        }

        public Exception Exception
        {
            get;
            set;
        }

        [JsonIgnore]
        public Byte[] FightHistory
        {
            get;
            set;
        }

        public string fight_history_base64
        {
            get => Convert.ToBase64String(FightHistory);
            set => FightHistory = Convert.FromBase64String(value);
        }

        public Score Score
        {
            get;
            set;
        }

    }
}
