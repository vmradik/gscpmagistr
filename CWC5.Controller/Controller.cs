﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using CWC5.LogicalCore;
using CWC5.LogicalCore.Controller;
using System.Threading.Tasks;
using CWC5.LogicalCore.Model;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace CWC5.Controller
{
    public class Controller
    {
        #region Default Options
        public static ModelingOptions DefaultOptions
        {
            get
            {
                return _DefaultOptions.Clone();
            }
        }

        private static int arenaBaseX = 1024;
        private static int arenaBaseY = 768;
        private static double arenaMultiplier = 1.1;
        private static int arenaX = (Int32)Math.Ceiling(arenaBaseX * arenaMultiplier);
        private static int arenaY = (Int32)Math.Ceiling(arenaBaseY * arenaMultiplier);

        private static readonly ModelingOptions _DefaultOptions = new ModelingOptions()
        {

            PlayerResponsePeriod = 1,
            PlayersCount = 2,
            CloudCount = 1,
            DeformationCoefficient = 2.5,
            BonusesOnScreenLimit = 16,

            //Arena_Size = new DPoint(1024, 700),
            //Arena_Size = new DPoint(1536, 1050),
            Arena_Size = new DPoint(arenaX, arenaY),
            Arena_FightDuration = 250,

            EnergySpill_Border = 2,
            EnergySpill_ChangeDirectMin = 50,
            EnergySpill_ChangeDirectMax = 100,

            EnergySpill_RadiusMin = 60,
            EnergySpill_RadiusMax = 100,
            EnergySpill_RadiusChangeStepMin = 0.08,
            EnergySpill_RadiusChangeStepMax = 0.25,
            EnergySpill_RadiusChangeTickMin = 30,
            EnergySpill_RadiusChangeTickMax = 70,


            EnergySpill_LifetimeMin = 140,
            EnergySpill_LifetimeMax = 250,
            EnergySpill_PauseMin = 90,
            EnergySpill_PauseMax = 170,
            EnergySpill_SpaceshipResistance = 0.15,
            EnergySpill_Power = 2,


            Spaceship_Radius = 40,
            Spaceship_InitEnergy = 1500,
            Spaceship_MaxAcceleration = 1,
            Spaceship_MaxTurnSpeed = new Angle(Math.PI / 8),
            Spaceship_Resistance = 0.06,


            Spaceship_PlasmaInitCooldown = 50,
            Spaceship_GravimineInitCooldown = 30,
            //Spaceship_MissileCapacity = 30, //TODO Missile


            Spaceship_MissileInitCooldown = 30,
            Spaceship_CargoHold = 12,
            Spaceship_GravimineCapacity = 5,
            Spaceship_MissileCapacity = 5,

            Spaceship_GravishieldCapacity = 5,
            Spaceship_DashCapacity = 5,


            PlasmaBullet_Radius = 8.0,
            PlasmaBullet_EnergyPower = 40,
            PlasmaBullet_Speed = 50,
            PlasmaBullet_Price = 10,


            GravimineBullet_Radius = 13.0,
            GravimineBullet_EnergyPower = 30,
            GravimineBullet_PushPower = 60,
            GravimineBullet_StartSpeed = 50,
            GravimineBullet_MinSpeed = 1,
            GravimineBullet_Resistance = 0.018,
            GravimineBullet_Price = 10,



            //GravimineBonus_Radius = 20,
            GravimineBonus_Radius = 18,
            GravimineBonusProbability = 0.006,
            //GravimineBonusProbability = 1,



            MissileBullet_Radius = 13.0,
            MissileBullet_EnergyPower = 30,
            MissileBullet_PushPower = 60,
            MissileBullet_StartSpeed = 50,
            MissileBullet_MinSpeed = 20,
            MissileBullet_Resistance = 0.018,
            MissileBullet_Price = 5,


            //MissileBonus_Radius = 20,
            MissileBonus_Radius = 18,
            MissileBonusProbability = 0.006,
            //MissileBonusProbability = 1,



            /* now this is speed booster */
            BoosterBonus_MaxAcceleration = 3,
            BoosterBonus_RiseAcceleration = 1.5,


            //BoosterBonus_Radius = 20,
            BoosterBonus_Radius = 18,
            BoosterBonus_Multiplier = 1.2,
            BoosterBonus_Duration = 200,
            BoosterBonusProbability = 0.003,
            //BoosterBonusProbability = 0.0,


            ///* now this is speed booster */       // WTF
            //FireboosterBonus_MaxAcceleration = 3,
            //FireboosterBonus_RiseAcceleration = 1.5,


            //FireBoosterBonus_Radius = 20,
            FireboosterBonus_Radius = 18,
            //FireBoosterBonus_NewCooldown = 1.5,   // TODO
            FireboosterBonus_NewCooldown = 15,   // TODO
            FireboosterBonus_Duration = 200,
            FireboosterBonusProbability = 0.003,
            //FireboosterBonusProbability = 0.0,



            //GravishieldBonus_Radius = 20,
            GravishieldBonus_Radius = 18,
            GravishieldBonus_InitStrength = 2,
            GravishieldBonus_Duration = 150,
            GravishieldBonusProbability = 0.004,
            //GravishieldBonusProbability = 1,

            DashBonus_Radius = 18,
            DashBonus_Boost = 40,
            DashBonusProbability = 0.003,

            Bot_MessagesMaxTotalLength = 100 * 1000,




            PlayerRespawnPoints = new DPoint[]


             {
                new DPoint(arenaX / 4.0 * 1, arenaY / 4.0 * 1),
                new DPoint(arenaX / 4.0 * 3, arenaY / 4.0 * 3),
                new DPoint(arenaX / 4.0 * 1, arenaY / 4.0 * 3),
                new DPoint(arenaX / 4.0 * 3, arenaY / 4.0 * 1),

             },

            TimeToBotResponse = TimeSpan.FromMilliseconds(10000),



        };

        #endregion





        public Controller()
        {
        }

        public static Task<ModelingResult> DoModelingAsync(ModelingOptions options, IList<BotInfo> botsInfo)
        {
            return Task.Run(() => DoModeling(options, botsInfo, GameController.GetEmptyArena, GameController.GetNextFrame));
        }
        public static ModelingResult DoModeling(ModelingOptions _options, IList<BotInfo> _botsInfo)
        {
            return DoModeling(_options, _botsInfo, GameController.GetEmptyArena, GameController.GetNextFrame);
        }

        public static ModelingResult DoModeling(ModelingOptions _options, IList<BotInfo> _botsInfo,
        Func<LogicalCore.Model.GameOptions, LogicalCore.Model.Arena> get_empty_arena,
        Func<LogicalCore.Model.Arena, List<DTOModel.TurnResult?>, LogicalCore.Model.Arena> get_next_turn)
        {
            ModelingResult result;
            IBot[] bots = new IBot[_botsInfo.Count];
            try
            {
                _options.PlayersCount = _botsInfo.Count;


                //string ip = "192.168.1.13";
                string ip = "localhost";
                //int port = 60100;


                var bot_info = _botsInfo.Select(b => new LogicalCore.FightStory.BotInfo()
                {
                    Name = b.Name,
                    StrategyID = b.StrategyID,
                    UserID = 0
                }).ToList();


                string config_path = "./controller_config.json";
                int reruns = 1;
                if (File.Exists(config_path)) {
                    JObject o = JObject.Parse(File.ReadAllText(config_path));
                    if (!o.ContainsKey("round_rerun_count"))
                    {
                        throw new Exception("invalid controller config");
                    }
                    reruns = o["round_rerun_count"].Value<int>();
                }

                LinkedList<LogicalCore.FightStory.FightStoryContainer> run_logs = new LinkedList<LogicalCore.FightStory.FightStoryContainer>();

                for(int r = 0; r < reruns; ++r)
                {

                    {
                        // construct bots
                        for (int i = 0; i < bots.Length; i++)
                        {
                            var info = _botsInfo[i];
                            if (info.ExecutableType == ExecutableType.Embedded)
                            {
                                bots[i] = SimpleBots.SimpleBotFactory.GetSimpleBot(info.ExecutableFilePath);
                            }
                            else if (info.ExecutableType == ExecutableType.grpc)
                            {
                                bots[i] = new Grpc_Bot(info.ExecutableFilePath, info.ExecutableType, _options.TimeToBotResponse, info.SaveLogMessages);
                            }
                            else if (info.ExecutableType == ExecutableType.JSON)
                            {
                                bots[i] = new Remote_Bot(info.ExecutableFilePath, info.ExecutableType, _options.TimeToBotResponse, info.SaveLogMessages);
                            }
                            else
                            {
                                bots[i] = new ExternalBot(info.ExecutableFilePath, info.ExecutableType, _options.TimeToBotResponse, info.SaveLogMessages);
                            }
                        }
                        // start bots
                        foreach (var bot in bots.OfType<ExternalBot>())
                        {
                            bot.Start();
                        }
                        foreach (var bot in bots.OfType<Grpc_Bot>())
                        {
                            bot.Start(ip, Networking.get_free_port());
                            //++port;
                        }
                        foreach (var bot in bots.OfType<Remote_Bot>())
                        {
                            bot.Start();
                            //++port;
                        }
                    }



                    var watch = System.Diagnostics.Stopwatch.StartNew();

                    LogicalCore.FightStory.FightStoryContainer fightStory = new LogicalCore.FightStory.FightStoryContainer(
                        _options.Arena_FightDuration,
                        bot_info.ToList());
                    LogicalCore.Model.GameOptions game_options = _options.GetGameOptions();
                    LogicalCore.Model.Arena arena = get_empty_arena(game_options);
                    List<DTOModel.TurnResult?> answers = null;
                    fightStory.AddFrame(new LogicalCore.FightStory.Frame(arena));

                    ///инициализация всех игроков
                    for (int playerId = 0; playerId < _options.PlayersCount; playerId++)
                    {
                        bots[playerId].Init(new LogicalCore.DTO.DTOMapper().GetOptions(_options));
                    }

                    for (int tick = arena.Tick; tick < _options.Arena_FightDuration; tick++)
                    {
                        List<String> messages = new List<string>();
                        ///запрашиваем запросы тика tick
                        if ((tick % _options.PlayerResponsePeriod) == 0)
                        {
                            answers = new List<DTOModel.TurnResult?>();
                            for (int playerId = 0; playerId < _options.PlayersCount; playerId++)
                            {
                                var bot_answer = bots[playerId].GetTurnResult(BotQueryHandler.CreateBotQuery(arena, playerId));
                                answers.Add(bot_answer);

                                ///сохраняем сообщения тика tick
                                if (bot_answer.HasValue && bot_answer.Value.Messages != null)
                                {
                                    messages.AddRange(bot_answer.Value.Messages);
                                }
                            }
                        }

                        ///вополняем переход к тику (tick + 1)
                        arena = get_next_turn(arena, answers);
                        LogicalCore.FightStory.Frame current_frame = new LogicalCore.FightStory.Frame(arena) {
                            Events = arena.events,
                            Messages = messages
                        };
                        current_frame.Messages.AddRange(messages);
                        fightStory.AddFrame(current_frame);
                    }

                    ///добавление последжнего фрейма
                    //story.AddFrame(new FightStory.Frame(arena));
                    run_logs.AddLast(fightStory);


                    watch.Stop();

                    var elapsedMs = watch.ElapsedMilliseconds;
                    TimeSpan time = TimeSpan.FromMilliseconds(elapsedMs);

                    {
                        // stop boys
                        var exceptions = new List<Exception>();
                        foreach (var bot in bots.OfType<ExternalBot>())
                        {
                            try
                            {
                                //bot.Stop();
                            }
                            catch (Exception e)
                            {
                                exceptions.Add(e);
                            }
                        }
                        foreach (var bot in bots.OfType<Grpc_Bot>())
                        {
                            try
                            {
                                //bot.Stop();
                            }
                            catch (Exception e)
                            {
                                exceptions.Add(e);
                            }
                        }
                        foreach (var bot in bots.OfType<Remote_Bot>())
                        {
                            try
                            {
                                //bot.Stop();
                            }
                            catch (Exception e)
                            {
                                exceptions.Add(e);
                            }
                        }
                        if (exceptions.Any())
                        {
                            throw new AggregateException("Funny stuff happended during execution./n Cannot stop all strategies.", exceptions);
                        }
                    }
                }


                var fight = get_fair_battle(run_logs);

                MemoryStream ms = new MemoryStream();
                var fss = new LogicalCore.FightStory.FightStoryStream(ms);
                fss.WriteFightStory(fight);
                result = new ModelingResult(ms.ToArray(), new Score(fight.GetFinalPoints()));
            }
            catch (Exception e)
            {
                result = new ModelingResult(e);
            }
            finally
            {
            }

            return result;
        }

        static string get_places(LogicalCore.FightStory.FightStoryContainer log)
        {
            var points = log.GetFinalPoints();
            points = points.OrderByDescending(p => p.Value).ToList();
            StringBuilder sb = new StringBuilder();
            foreach (var p in points)
            {
                sb.Append(p.Key).Append(',');
            }
            return sb.ToString();
        }

        static LogicalCore.FightStory.FightStoryContainer get_fair_battle(LinkedList<LogicalCore.FightStory.FightStoryContainer> run_logs)
        {
            Dictionary<string, LogicalCore.FightStory.FightStoryContainer> containers = new Dictionary<string, LogicalCore.FightStory.FightStoryContainer>();
            Dictionary<string, int> score_count = new Dictionary<string, int>();

            foreach (var cont in run_logs)
            {
                string places = get_places(cont);
                if (!containers.ContainsKey(places))
                {
                    containers.Add(places, cont);
                }
                int count = 0;
                if (score_count.TryGetValue(places, out count)) {
                    score_count[places] = count + 1;
                } else
                {
                    score_count.Add(places, 1);
                }
            }
            int biggest_repetition = score_count.Max(sc => sc.Value);
            return containers[score_count.FirstOrDefault(sc => sc.Value == biggest_repetition).Key];
        }


    }
}
