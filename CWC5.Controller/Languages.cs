﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWC5.Controller
{
	public static class Languages
	{
		private static readonly Dictionary<String, CWC5.Controller.ExecutableType> _execTypes = new Dictionary<string, CWC5.Controller.ExecutableType>
		{
			{ "cpp", CWC5.Controller.ExecutableType.Exe },
			{ "csharp", CWC5.Controller.ExecutableType.grpc },
			{ "java", CWC5.Controller.ExecutableType.Java },
			{ "json", CWC5.Controller.ExecutableType.JSON },
		};

		public static CWC5.Controller.ExecutableType GetExecutableType(String languageId)
		{
			CWC5.Controller.ExecutableType result;
			if (!_execTypes.TryGetValue(languageId, out result))
			{
				result = CWC5.Controller.ExecutableType.Unknown;
			}
			return result;
		}

	}
}
