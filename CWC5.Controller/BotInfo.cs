﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace CWC5.Controller
{
	public enum ExecutableType
	{
		Unknown,
		Exe,
		Java,
		Embedded,
		grpc,
		JSON
	}

    public class BotInfo
    {
        public String Name { get; set; }

		public String ExecutableFilePath { get; set; }
		
		public ExecutableType ExecutableType { get; set; }
		
        public Int32 StrategyID { get; set; }

        public Boolean SaveLogMessages { get; set; }
    }
}
