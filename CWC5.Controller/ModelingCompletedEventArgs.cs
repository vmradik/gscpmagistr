﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CWC5.Controller
{
    public class ModelingCompletedEventArgs : EventArgs
    {
        public ModelingCompletedEventArgs(ModelingResult result)
        {
            Result = result;
        }

        public ModelingResult Result
        {
            get;
            set;
        }

    }
}
