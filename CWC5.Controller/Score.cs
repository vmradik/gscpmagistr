﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace CWC5.Controller
{
    [DataContract]
    public class Score
    {
        public Score(List<KeyValuePair<Int32, Double>> points)
        {
            Points = points;
        }
        
        [DataMember]
        public List<Int32> Strategies
        {
            get { return Points.Select(p => p.Key).ToList(); }
        }

        [DataMember]
        public List<KeyValuePair<Int32, Double>> Points
        {
            get;
            set;
        }

        public Dictionary<Int32, Double> GetDistinctPoints()
        {
            Dictionary<Int32, Double> res = new Dictionary<int, double>();
            foreach (var item in Points)
            {
                if (!res.ContainsKey(item.Key))
                {
                    res.Add(item.Key, 0);
                }
                res[item.Key] += item.Value;
            }
            return res.ToDictionary(item => item.Key, item => item.Value / Points.Count(p => p.Key == item.Key));
        }
    }
}
