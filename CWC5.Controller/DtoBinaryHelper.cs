﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace CWC5.Controller
{
    static class DtoBinaryHelper
    {
        public static void Write(this BinaryWriter bw, DTOModel.GameOptions options)
        {
            bw.WriteStruct(options);
        }

        public static void Write(this BinaryWriter bw, DTOModel.Arena arena)
        {
            bw.Write(arena.Tick);
            bw.Write(arena.EnergySpills);

			bw.WriteStruct(arena.Me);
			bw.Write(arena.Enemies);

			bw.Write(arena.GravimineBonuses);
            bw.Write(arena.MissileBonuses);

            bw.Write(arena.BoosterBonuses);
            bw.Write(arena.FireBoosterBonuses);
            
            bw.Write(arena.DashBonuses);
            bw.Write(arena.GravishieldBonuses);

            bw.Write(arena.PlasmaBullets);
            bw.Write(arena.GravimineBullets);
            bw.Write(arena.MissileBullets);
        }

        private static void WriteStruct<T>(this BinaryWriter bw, T str) where T : struct
        {
            int len = Marshal.SizeOf(str);
            var bytes = new byte[len];
            var gh = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            try
            {
                Marshal.StructureToPtr(str, gh.AddrOfPinnedObject(), false);
            }
            finally
            {
                gh.Free();
            }
            bw.Write(bytes);
        }

        private static void Write<T>(this BinaryWriter bw, T[] array) where T : struct
        {
            bw.Write(array.Length);
            for (int i = 0; i < array.Length; i++)
                bw.WriteStruct(array[i]);
        }

        private static Encoding _encoding = Encoding.GetEncoding(1251);
        public static DTOModel.TurnResult ReadTurnResult(this BinaryReader br, int maxMessageLength)
        {
            var result = new DTOModel.TurnResult();

            var point = new CWC5.DTOModel.Point();
            point.X = br.ReadDouble();
            point.Y = br.ReadDouble();

            result.Acceleration = point;
            result.Bullet = (CWC5.DTOModel.BulletType)br.ReadInt32();
            result.CannonDirection = br.ReadDouble();
            result.ActivateDash = br.ReadByte() != 0;
            result.DashDirection = br.ReadDouble();
            result.TargetID = br.ReadInt32();
            result.ActivateShield = br.ReadByte() != 0;

            result.Messages = new string[br.ReadInt32()];
            int totalMessageLength = 0;
            for (int i = 0; i < result.Messages.Length; i++)
            {
                int len = br.ReadInt32();
                totalMessageLength += len;
                if (totalMessageLength > maxMessageLength)
                    throw new FormatException("Maximum messages length exceeded.");
                result.Messages[i] = _encoding.GetString(br.ReadBytes(len));
            }

            return result;
        }
    }
}
