﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CWC5.Controller
{
    public static class Networking
    {
        public static int get_free_port()
        {
            int port;
            using (var listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                listener.Bind(new IPEndPoint(IPAddress.Loopback, 0));
                listener.Listen(1);
                port = ((IPEndPoint)listener.LocalEndPoint).Port;
                listener.Close();
            }
            return port;
        }
    }
}
